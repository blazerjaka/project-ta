-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 30 Agu 2019 pada 11.15
-- Versi Server: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `label_a`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `nama`, `username`, `password`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$a7ieySvVPE6Yyri8W6rGcO5tZ/8NLH8ERFaB/e.KqQhtC.689TZt.'),
(2, 'jack', 'admin@label.a', '$2y$10$MX/d1cTK/8KpnJwtx5/OD.OwKfj7x2SHS8Uvz1pHDxQk7G/c7XGvi'),
(4, 'admin2', 'admin@gmail.com', '$2y$10$uG2XPLQW9ceCqOzSs13UZ.PlXBvwiBCSoxJyrXt/Ouup/2EvEuJWK');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alamat`
--

CREATE TABLE `alamat` (
  `id` int(11) NOT NULL,
  `email_users` varchar(100) NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `negara` varchar(50) NOT NULL,
  `provinsi` varchar(30) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `kode_pos` varchar(5) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `tipe_alamat` enum('1','2') NOT NULL,
  `nama_tipe` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alamat`
--

INSERT INTO `alamat` (`id`, `email_users`, `nama_penerima`, `negara`, `provinsi`, `kota`, `kode_pos`, `alamat`, `no_hp`, `tipe_alamat`, `nama_tipe`) VALUES
(1, 'ilhamhermawanzero@gmail.com', 'ilham hermawan', 'Indonesia', 'Kalimantan Barat', 'sintang', '78613', 'sintang', '081649091095', '1', 'ilham Hermawan'),
(2, 'abatacreative@gmail.com', 'ABATA Creative', 'Indonesia', 'Jawa Barat', 'Kab. Bogor', '9999', 'perumahan taman cibinong asri Blok c1 no 68 rt 01/19, kode pos : 16913\r\nKel.Karadenan', '0895373684350', '1', 'My Home'),
(3, 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(4, 'blazerjaka@gmail.com', 'Ian', 'Afghanistan', '77777', 'Afghan KAroke', '12312', 'asdasdasdada', '0852345421', '2', 'Hotel'),
(5, 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', 'Kalimantan Barat', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '2', 'Rumah');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alamat_invoice`
--

CREATE TABLE `alamat_invoice` (
  `id` int(11) NOT NULL,
  `code_invoice` varchar(100) NOT NULL,
  `email_users` varchar(100) NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `negara` varchar(50) NOT NULL,
  `provinsi` varchar(30) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `kode_pos` varchar(5) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `tipe_alamat` enum('1','2') NOT NULL,
  `nama_tipe` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alamat_invoice`
--

INSERT INTO `alamat_invoice` (`id`, `code_invoice`, `email_users`, `nama_penerima`, `negara`, `provinsi`, `kota`, `kode_pos`, `alamat`, `no_hp`, `tipe_alamat`, `nama_tipe`) VALUES
(1, 'MB-0804180001', 'ilhamhermawanzero@gmail.com', 'ilham hermawan', 'Indonesia', 'Kalimantan Barat', 'sintang', '78613', 'sintang', '081649091095', '1', 'ilham Hermawan'),
(2, 'MB-2307190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(3, 'MB-2307190002', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(4, 'MB-2307190003', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(5, 'MB-2507190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(6, 'MB-2507190002', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(7, 'MB-2507190003', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(8, 'MB-2807190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(9, 'MB-3007190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(10, 'MB-3007190002', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(11, 'MB-3107190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(12, 'MB-3107190002', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(13, 'MB-3107190003', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(14, 'MB-3107190004', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(15, 'MB-3107190005', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(16, 'MB-0108190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(17, 'MB-0108190002', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(18, 'MB-0108190003', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(19, 'MB-0108190004', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(20, 'MB-0108190005', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(21, 'MB-0108190006', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(22, 'MB-0108190007', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(23, 'MB-0308190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(24, 'MB-0308190002', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(25, 'MB-0708190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(26, 'MB-0808190001', 'blazerjaka@gmail.com', 'Ian', 'Afghanistan', '77777', 'Afghan KAroke', '12312', 'asdasdasdada', '0852345421', '2', 'Hotel'),
(27, 'MB-0908190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', 'Kalimantan Barat', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '2', 'Rumah'),
(28, 'MB-0908190002', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo'),
(29, 'MB-2408190001', 'blazerjaka@gmail.com', 'Muhammad Jakaria', 'Indonesia', '78231', 'PONTIANAK', '78231', 'JLN. TANJUNG RAYA 2 KOMP. GRAND PARMA RESIDENCE NO. D15, RT. 004/RW. 005', '0852345421', '1', 'Igo');

-- --------------------------------------------------------

--
-- Struktur dari tabel `alamat_pengajuan`
--

CREATE TABLE `alamat_pengajuan` (
  `id` int(11) NOT NULL,
  `code_invoice_pengajuan` varchar(100) NOT NULL,
  `email_users` varchar(100) NOT NULL,
  `nama_penerima` varchar(50) NOT NULL,
  `negara` varchar(50) NOT NULL,
  `provinsi` varchar(30) NOT NULL,
  `kota` varchar(30) NOT NULL,
  `kode_pos` varchar(5) NOT NULL,
  `alamat` text NOT NULL,
  `no_hp` varchar(15) NOT NULL,
  `tipe_alamat` enum('1','2') NOT NULL,
  `nama_tipe` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `alamat_pengajuan`
--

INSERT INTO `alamat_pengajuan` (`id`, `code_invoice_pengajuan`, `email_users`, `nama_penerima`, `negara`, `provinsi`, `kota`, `kode_pos`, `alamat`, `no_hp`, `tipe_alamat`, `nama_tipe`) VALUES
(6, 'MB-R-1504180001', 'abatacreative@gmail.com', 'ABATA Creative', 'Indonesia', 'Jawa Barat', 'Kab. Bogor', '9999', 'perumahan taman cibinong asri Blok c1 no 68 rt 01/19, kode pos : 16913\r\nKel.Karadenan', '0895373684350', '1', 'My Home'),
(7, 'MB-R-1604180001', 'ilhamhermawanzero@gmail.com', 'ilham hermawan', 'Indonesia', 'Kalimantan Barat', 'sintang', '78613', 'sintang', '081649091095', '1', 'ilham Hermawan'),
(8, 'MB-R-1604180002', 'abatacreative@gmail.com', 'ABATA Creative', 'Indonesia', 'Jawa Barat', 'Kab. Bogor', '9999', 'perumahan taman cibinong asri Blok c1 no 68 rt 01/19, kode pos : 16913\r\nKel.Karadenan', '0895373684350', '1', 'My Home');

-- --------------------------------------------------------

--
-- Struktur dari tabel `apps_countries`
--

CREATE TABLE `apps_countries` (
  `id` int(11) NOT NULL,
  `country_code` varchar(2) NOT NULL DEFAULT '',
  `country_name` varchar(100) NOT NULL DEFAULT ''
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `apps_countries`
--

INSERT INTO `apps_countries` (`id`, `country_code`, `country_name`) VALUES
(1, 'AF', 'Afghanistan'),
(2, 'AL', 'Albania'),
(3, 'DZ', 'Algeria'),
(4, 'DS', 'American Samoa'),
(5, 'AD', 'Andorra'),
(6, 'AO', 'Angola'),
(7, 'AI', 'Anguilla'),
(8, 'AQ', 'Antarctica'),
(9, 'AG', 'Antigua and Barbuda'),
(10, 'AR', 'Argentina'),
(11, 'AM', 'Armenia'),
(12, 'AW', 'Aruba'),
(13, 'AU', 'Australia'),
(14, 'AT', 'Austria'),
(15, 'AZ', 'Azerbaijan'),
(16, 'BS', 'Bahamas'),
(17, 'BH', 'Bahrain'),
(18, 'BD', 'Bangladesh'),
(19, 'BB', 'Barbados'),
(20, 'BY', 'Belarus'),
(21, 'BE', 'Belgium'),
(22, 'BZ', 'Belize'),
(23, 'BJ', 'Benin'),
(24, 'BM', 'Bermuda'),
(25, 'BT', 'Bhutan'),
(26, 'BO', 'Bolivia'),
(27, 'BA', 'Bosnia and Herzegovina'),
(28, 'BW', 'Botswana'),
(29, 'BV', 'Bouvet Island'),
(30, 'BR', 'Brazil'),
(31, 'IO', 'British Indian Ocean Territory'),
(32, 'BN', 'Brunei Darussalam'),
(33, 'BG', 'Bulgaria'),
(34, 'BF', 'Burkina Faso'),
(35, 'BI', 'Burundi'),
(36, 'KH', 'Cambodia'),
(37, 'CM', 'Cameroon'),
(38, 'CA', 'Canada'),
(39, 'CV', 'Cape Verde'),
(40, 'KY', 'Cayman Islands'),
(41, 'CF', 'Central African Republic'),
(42, 'TD', 'Chad'),
(43, 'CL', 'Chile'),
(44, 'CN', 'China'),
(45, 'CX', 'Christmas Island'),
(46, 'CC', 'Cocos (Keeling) Islands'),
(47, 'CO', 'Colombia'),
(48, 'KM', 'Comoros'),
(49, 'CG', 'Congo'),
(50, 'CK', 'Cook Islands'),
(51, 'CR', 'Costa Rica'),
(52, 'HR', 'Croatia (Hrvatska)'),
(53, 'CU', 'Cuba'),
(54, 'CY', 'Cyprus'),
(55, 'CZ', 'Czech Republic'),
(56, 'DK', 'Denmark'),
(57, 'DJ', 'Djibouti'),
(58, 'DM', 'Dominica'),
(59, 'DO', 'Dominican Republic'),
(60, 'TP', 'East Timor'),
(61, 'EC', 'Ecuador'),
(62, 'EG', 'Egypt'),
(63, 'SV', 'El Salvador'),
(64, 'GQ', 'Equatorial Guinea'),
(65, 'ER', 'Eritrea'),
(66, 'EE', 'Estonia'),
(67, 'ET', 'Ethiopia'),
(68, 'FK', 'Falkland Islands (Malvinas)'),
(69, 'FO', 'Faroe Islands'),
(70, 'FJ', 'Fiji'),
(71, 'FI', 'Finland'),
(72, 'FR', 'France'),
(73, 'FX', 'France, Metropolitan'),
(74, 'GF', 'French Guiana'),
(75, 'PF', 'French Polynesia'),
(76, 'TF', 'French Southern Territories'),
(77, 'GA', 'Gabon'),
(78, 'GM', 'Gambia'),
(79, 'GE', 'Georgia'),
(80, 'DE', 'Germany'),
(81, 'GH', 'Ghana'),
(82, 'GI', 'Gibraltar'),
(83, 'GK', 'Guernsey'),
(84, 'GR', 'Greece'),
(85, 'GL', 'Greenland'),
(86, 'GD', 'Grenada'),
(87, 'GP', 'Guadeloupe'),
(88, 'GU', 'Guam'),
(89, 'GT', 'Guatemala'),
(90, 'GN', 'Guinea'),
(91, 'GW', 'Guinea-Bissau'),
(92, 'GY', 'Guyana'),
(93, 'HT', 'Haiti'),
(94, 'HM', 'Heard and Mc Donald Islands'),
(95, 'HN', 'Honduras'),
(96, 'HK', 'Hong Kong'),
(97, 'HU', 'Hungary'),
(98, 'IS', 'Iceland'),
(99, 'IN', 'India'),
(100, 'IM', 'Isle of Man'),
(101, 'ID', 'Indonesia'),
(102, 'IR', 'Iran (Islamic Republic of)'),
(103, 'IQ', 'Iraq'),
(104, 'IE', 'Ireland'),
(105, 'IL', 'Israel'),
(106, 'IT', 'Italy'),
(107, 'CI', 'Ivory Coast'),
(108, 'JE', 'Jersey'),
(109, 'JM', 'Jamaica'),
(110, 'JP', 'Japan'),
(111, 'JO', 'Jordan'),
(112, 'KZ', 'Kazakhstan'),
(113, 'KE', 'Kenya'),
(114, 'KI', 'Kiribati'),
(115, 'KP', 'Korea, Democratic People''s Republic of'),
(116, 'KR', 'Korea, Republic of'),
(117, 'XK', 'Kosovo'),
(118, 'KW', 'Kuwait'),
(119, 'KG', 'Kyrgyzstan'),
(120, 'LA', 'Lao People''s Democratic Republic'),
(121, 'LV', 'Latvia'),
(122, 'LB', 'Lebanon'),
(123, 'LS', 'Lesotho'),
(124, 'LR', 'Liberia'),
(125, 'LY', 'Libyan Arab Jamahiriya'),
(126, 'LI', 'Liechtenstein'),
(127, 'LT', 'Lithuania'),
(128, 'LU', 'Luxembourg'),
(129, 'MO', 'Macau'),
(130, 'MK', 'Macedonia'),
(131, 'MG', 'Madagascar'),
(132, 'MW', 'Malawi'),
(133, 'MY', 'Malaysia'),
(134, 'MV', 'Maldives'),
(135, 'ML', 'Mali'),
(136, 'MT', 'Malta'),
(137, 'MH', 'Marshall Islands'),
(138, 'MQ', 'Martinique'),
(139, 'MR', 'Mauritania'),
(140, 'MU', 'Mauritius'),
(141, 'TY', 'Mayotte'),
(142, 'MX', 'Mexico'),
(143, 'FM', 'Micronesia, Federated States of'),
(144, 'MD', 'Moldova, Republic of'),
(145, 'MC', 'Monaco'),
(146, 'MN', 'Mongolia'),
(147, 'ME', 'Montenegro'),
(148, 'MS', 'Montserrat'),
(149, 'MA', 'Morocco'),
(150, 'MZ', 'Mozambique'),
(151, 'MM', 'Myanmar'),
(152, 'NA', 'Namibia'),
(153, 'NR', 'Nauru'),
(154, 'NP', 'Nepal'),
(155, 'NL', 'Netherlands'),
(156, 'AN', 'Netherlands Antilles'),
(157, 'NC', 'New Caledonia'),
(158, 'NZ', 'New Zealand'),
(159, 'NI', 'Nicaragua'),
(160, 'NE', 'Niger'),
(161, 'NG', 'Nigeria'),
(162, 'NU', 'Niue'),
(163, 'NF', 'Norfolk Island'),
(164, 'MP', 'Northern Mariana Islands'),
(165, 'NO', 'Norway'),
(166, 'OM', 'Oman'),
(167, 'PK', 'Pakistan'),
(168, 'PW', 'Palau'),
(169, 'PS', 'Palestine'),
(170, 'PA', 'Panama'),
(171, 'PG', 'Papua New Guinea'),
(172, 'PY', 'Paraguay'),
(173, 'PE', 'Peru'),
(174, 'PH', 'Philippines'),
(175, 'PN', 'Pitcairn'),
(176, 'PL', 'Poland'),
(177, 'PT', 'Portugal'),
(178, 'PR', 'Puerto Rico'),
(179, 'QA', 'Qatar'),
(180, 'RE', 'Reunion'),
(181, 'RO', 'Romania'),
(182, 'RU', 'Russian Federation'),
(183, 'RW', 'Rwanda'),
(184, 'KN', 'Saint Kitts and Nevis'),
(185, 'LC', 'Saint Lucia'),
(186, 'VC', 'Saint Vincent and the Grenadines'),
(187, 'WS', 'Samoa'),
(188, 'SM', 'San Marino'),
(189, 'ST', 'Sao Tome and Principe'),
(190, 'SA', 'Saudi Arabia'),
(191, 'SN', 'Senegal'),
(192, 'RS', 'Serbia'),
(193, 'SC', 'Seychelles'),
(194, 'SL', 'Sierra Leone'),
(195, 'SG', 'Singapore'),
(196, 'SK', 'Slovakia'),
(197, 'SI', 'Slovenia'),
(198, 'SB', 'Solomon Islands'),
(199, 'SO', 'Somalia'),
(200, 'ZA', 'South Africa'),
(201, 'GS', 'South Georgia South Sandwich Islands'),
(202, 'ES', 'Spain'),
(203, 'LK', 'Sri Lanka'),
(204, 'SH', 'St. Helena'),
(205, 'PM', 'St. Pierre and Miquelon'),
(206, 'SD', 'Sudan'),
(207, 'SR', 'Suriname'),
(208, 'SJ', 'Svalbard and Jan Mayen Islands'),
(209, 'SZ', 'Swaziland'),
(210, 'SE', 'Sweden'),
(211, 'CH', 'Switzerland'),
(212, 'SY', 'Syrian Arab Republic'),
(213, 'TW', 'Taiwan'),
(214, 'TJ', 'Tajikistan'),
(215, 'TZ', 'Tanzania, United Republic of'),
(216, 'TH', 'Thailand'),
(217, 'TG', 'Togo'),
(218, 'TK', 'Tokelau'),
(219, 'TO', 'Tonga'),
(220, 'TT', 'Trinidad and Tobago'),
(221, 'TN', 'Tunisia'),
(222, 'TR', 'Turkey'),
(223, 'TM', 'Turkmenistan'),
(224, 'TC', 'Turks and Caicos Islands'),
(225, 'TV', 'Tuvalu'),
(226, 'UG', 'Uganda'),
(227, 'UA', 'Ukraine'),
(228, 'AE', 'United Arab Emirates'),
(229, 'GB', 'United Kingdom'),
(230, 'US', 'United States'),
(231, 'UM', 'United States minor outlying islands'),
(232, 'UY', 'Uruguay'),
(233, 'UZ', 'Uzbekistan'),
(234, 'VU', 'Vanuatu'),
(235, 'VA', 'Vatican City State'),
(236, 'VE', 'Venezuela'),
(237, 'VN', 'Vietnam'),
(238, 'VG', 'Virgin Islands (British)'),
(239, 'VI', 'Virgin Islands (U.S.)'),
(240, 'WF', 'Wallis and Futuna Islands'),
(241, 'EH', 'Western Sahara'),
(242, 'YE', 'Yemen'),
(243, 'ZR', 'Zaire'),
(244, 'ZM', 'Zambia'),
(245, 'ZW', 'Zimbabwe');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ci_sessions`
--

CREATE TABLE `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `data` blob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ci_sessions`
--

INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
('odn1deeru2qq9o41vfbn0g0ff3kqs1jc', '::1', 1566718941, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536363731383831343b576562736974654d6173746572426f74616e6963616c7c733a31333a2253756461684d6173756b4d6173223b6e616d617c733a343a226a61636b223b757365726e616d657c733a31333a2261646d696e406c6162656c2e61223b757365725f6c6f676765645f696e7c733a31323a2253756461685f4c6f6767696e223b757365725f70726f66696c657c613a383a7b733a323a226964223b733a32313a22313134333935363831313733353839323232373037223b733a353a22656d61696c223b733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b733a31343a2276657269666965645f656d61696c223b623a313b733a343a226e616d65223b733a31363a224d7568616d6d6164204a616b61726961223b733a31303a22676976656e5f6e616d65223b733a383a224d7568616d6d6164223b733a31313a2266616d696c795f6e616d65223b733a373a224a616b61726961223b733a373a2270696374757265223b733a38333a2268747470733a2f2f6c68332e676f6f676c6575736572636f6e74656e742e636f6d2f612d2f41417545376d416269755331715833384f3970706b5a5043394a46394d5a4452432d68475867717073554b434277223b733a363a226c6f63616c65223b733a323a22656e223b7d6e616d615f757365727c733a31363a224d7568616d6d6164204a616b61726961223b656d61696c7c733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b),
('bl2po9odio2smlfm1pj7tv7jt8pfssd6', '::1', 1566719910, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536363731393730313b576562736974654d6173746572426f74616e6963616c7c733a31333a2253756461684d6173756b4d6173223b6e616d617c733a343a226a61636b223b757365726e616d657c733a31333a2261646d696e406c6162656c2e61223b757365725f70726f66696c657c613a383a7b733a323a226964223b733a32313a22313134333935363831313733353839323232373037223b733a353a22656d61696c223b733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b733a31343a2276657269666965645f656d61696c223b623a313b733a343a226e616d65223b733a31363a224d7568616d6d6164204a616b61726961223b733a31303a22676976656e5f6e616d65223b733a383a224d7568616d6d6164223b733a31313a2266616d696c795f6e616d65223b733a373a224a616b61726961223b733a373a2270696374757265223b733a38333a2268747470733a2f2f6c68332e676f6f676c6575736572636f6e74656e742e636f6d2f612d2f41417545376d416269755331715833384f3970706b5a5043394a46394d5a4452432d68475867717073554b434277223b733a363a226c6f63616c65223b733a323a22656e223b7d6e616d615f757365727c733a31363a224d7568616d6d6164204a616b61726961223b656d61696c7c733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b61646d696e5f6c6f676765645f696e7c733a31323a2253756461685f4c6f6767696e223b),
('4dthj0pb0uhm8uc0qbrsqgq3h0kmvbpk', '::1', 1566720190, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536363732303038383b576562736974654d6173746572426f74616e6963616c7c733a31333a2253756461684d6173756b4d6173223b6e616d617c733a343a226a61636b223b757365726e616d657c733a31333a2261646d696e406c6162656c2e61223b757365725f70726f66696c657c613a383a7b733a323a226964223b733a32313a22313134333935363831313733353839323232373037223b733a353a22656d61696c223b733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b733a31343a2276657269666965645f656d61696c223b623a313b733a343a226e616d65223b733a31363a224d7568616d6d6164204a616b61726961223b733a31303a22676976656e5f6e616d65223b733a383a224d7568616d6d6164223b733a31313a2266616d696c795f6e616d65223b733a373a224a616b61726961223b733a373a2270696374757265223b733a38333a2268747470733a2f2f6c68332e676f6f676c6575736572636f6e74656e742e636f6d2f612d2f41417545376d416269755331715833384f3970706b5a5043394a46394d5a4452432d68475867717073554b434277223b733a363a226c6f63616c65223b733a323a22656e223b7d6e616d615f757365727c733a31363a224d7568616d6d6164204a616b61726961223b656d61696c7c733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b61646d696e5f6c6f676765645f696e7c733a31323a2253756461685f4c6f6767696e223b),
('9dgpsq329pjiimvqudt8uvog8evctfcq', '::1', 1566721378, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536363732313331333b576562736974654d6173746572426f74616e6963616c7c733a31333a2253756461684d6173756b4d6173223b6e616d617c733a343a226a61636b223b757365726e616d657c733a31333a2261646d696e406c6162656c2e61223b757365725f70726f66696c657c613a383a7b733a323a226964223b733a32313a22313134333935363831313733353839323232373037223b733a353a22656d61696c223b733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b733a31343a2276657269666965645f656d61696c223b623a313b733a343a226e616d65223b733a31363a224d7568616d6d6164204a616b61726961223b733a31303a22676976656e5f6e616d65223b733a383a224d7568616d6d6164223b733a31313a2266616d696c795f6e616d65223b733a373a224a616b61726961223b733a373a2270696374757265223b733a38333a2268747470733a2f2f6c68332e676f6f676c6575736572636f6e74656e742e636f6d2f612d2f41417545376d416269755331715833384f3970706b5a5043394a46394d5a4452432d68475867717073554b434277223b733a363a226c6f63616c65223b733a323a22656e223b7d6e616d615f757365727c733a31363a224d7568616d6d6164204a616b61726961223b656d61696c7c733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b757365725f6c6f676765645f696e7c733a31323a2253756461685f4c6f6767696e223b636172745f636f6e74656e74737c613a333a7b733a31303a22636172745f746f74616c223b643a3534303030303b733a31313a22746f74616c5f6974656d73223b643a31323b733a33323a226433643934343638303261343432353937353564333865366431363365383230223b613a31303a7b733a323a226964223b733a323a223130223b733a333a22717479223b643a31323b733a353a227072696365223b643a34353030303b733a363a22776569676874223b733a323a223132223b733a31343a226b617465676f72795f6265726174223b733a333a22506373223b733a343a226e616d65223b733a31333a2242494f4151554120536572756d223b733a353a22696d616765223b733a33363a2261303165623438303838643831636332643734356361373963353365353166392e6a7067223b733a343a22736c7567223b733a31333a2262696f617175612d736572756d223b733a353a22726f776964223b733a33323a226433643934343638303261343432353937353564333865366431363365383230223b733a383a22737562746f74616c223b643a3534303030303b7d7d),
('767g52a1dqdjshi03i36emcs53la20kq', '::1', 1566813210, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536363831333230333b576562736974654d6173746572426f74616e6963616c7c733a31333a2253756461684d6173756b4d6173223b),
('99sssqps6iae8nehdlqk7bkcru5cq9dd', '::1', 1566813210, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536363831333230333b576562736974654d6173746572426f74616e6963616c7c733a31333a2253756461684d6173756b4d6173223b),
('c1e41q978jrfs3shoejp13esp3esi7lg', '::1', 1566813418, 0x5f5f63695f6c6173745f726567656e65726174657c693a313536363831333230393b576562736974654d6173746572426f74616e6963616c7c733a31333a2253756461684d6173756b4d6173223b757365725f70726f66696c657c613a383a7b733a323a226964223b733a32313a22313134333935363831313733353839323232373037223b733a353a22656d61696c223b733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b733a31343a2276657269666965645f656d61696c223b623a313b733a343a226e616d65223b733a31363a224d7568616d6d6164204a616b61726961223b733a31303a22676976656e5f6e616d65223b733a383a224d7568616d6d6164223b733a31313a2266616d696c795f6e616d65223b733a373a224a616b61726961223b733a373a2270696374757265223b733a38333a2268747470733a2f2f6c68332e676f6f676c6575736572636f6e74656e742e636f6d2f612d2f41417545376d416269755331715833384f3970706b5a5043394a46394d5a4452432d68475867717073554b434277223b733a363a226c6f63616c65223b733a323a22656e223b7d6e616d615f757365727c733a31363a224d7568616d6d6164204a616b61726961223b656d61696c7c733a32303a22626c617a65726a616b6140676d61696c2e636f6d223b636172745f636f6e74656e74737c613a333a7b733a31303a22636172745f746f74616c223b643a313035303030303b733a31313a22746f74616c5f6974656d73223b643a31343b733a33323a223435633438636365326532643766626465613161666335316337633661643236223b613a31303a7b733a323a226964223b733a313a2239223b733a333a22717479223b643a31343b733a353a227072696365223b643a37353030303b733a363a22776569676874223b733a323a223134223b733a31343a226b617465676f72795f6265726174223b733a333a22506373223b733a343a226e616d65223b733a31313a2242494f4151554120422e42223b733a353a22696d616765223b733a33363a2232623639653531653331313936653638623232323535313539323130376666352e6a7067223b733a343a22736c7567223b733a31303a2262696f617175612d6262223b733a353a22726f776964223b733a33323a223435633438636365326532643766626465613161666335316337633661643236223b733a383a22737562746f74616c223b643a313035303030303b7d7d61646d696e5f6c6f676765645f696e7c733a31323a2253756461685f4c6f6767696e223b6e616d617c733a343a226a61636b223b757365726e616d657c733a31333a2261646d696e406c6162656c2e61223b);

-- --------------------------------------------------------

--
-- Struktur dari tabel `gambar_produk`
--

CREATE TABLE `gambar_produk` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `gambar` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoice`
--

CREATE TABLE `invoice` (
  `id` int(11) NOT NULL,
  `email_users` varchar(100) NOT NULL,
  `invoice` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `status` enum('Proceed','Shipped','Confirmed','Paid','Cancelled') NOT NULL,
  `delivery` tinytext NOT NULL,
  `payment` tinytext NOT NULL,
  `total` int(11) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `no_tracking` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `invoice`
--

INSERT INTO `invoice` (`id`, `email_users`, `invoice`, `tanggal`, `status`, `delivery`, `payment`, `total`, `gambar`, `no_tracking`) VALUES
(1, 'ilhamhermawanzero@gmail.com', 'MB-0804180001', '2018-04-08 00:31:29', 'Proceed', 'Fedex', 'BankWire', 20, '', ''),
(2, 'blazerjaka@gmail.com', 'MB-2307190001', '2019-07-23 12:02:39', 'Proceed', 'Pos Indonesia', 'Paypal', 20, '', ''),
(3, 'blazerjaka@gmail.com', 'MB-2307190002', '2019-07-23 12:31:19', 'Cancelled', 'Pos Indonesia', 'Paypal', 10, '', ''),
(4, 'blazerjaka@gmail.com', 'MB-2307190003', '2019-07-23 12:36:31', 'Cancelled', 'Pos Indonesia', 'Paypal', 10, '', ''),
(5, 'blazerjaka@gmail.com', 'MB-2507190001', '2019-07-25 11:32:07', 'Paid', 'Pos Indonesia', 'Paypal', 10, '', ''),
(6, 'blazerjaka@gmail.com', 'MB-2507190002', '2019-07-25 11:46:48', 'Proceed', 'Gojek', 'Paypal', 10, '', ''),
(7, 'blazerjaka@gmail.com', 'MB-2507190003', '2019-07-25 12:34:05', 'Paid', 'Gojek', 'BNI', 10, '753010a6f0d506068e6cfe220f202cf8.png', ''),
(8, 'blazerjaka@gmail.com', 'MB-2807190001', '2019-07-28 17:03:06', 'Paid', 'Gojek', 'MANDIRI', 40000, '', ''),
(9, 'blazerjaka@gmail.com', 'MB-3007190001', '2019-07-30 11:35:38', 'Shipped', 'Grab', 'BNI', 40000, '', ''),
(10, 'blazerjaka@gmail.com', 'MB-3007190002', '2019-07-30 20:14:08', 'Paid', 'Grab', 'MANDIRI', 80000, '', ''),
(11, 'blazerjaka@gmail.com', 'MB-3107190001', '2019-07-31 14:34:31', 'Proceed', 'Gojek', 'MANDIRI', 155000, '', ''),
(12, 'blazerjaka@gmail.com', 'MB-3107190002', '2019-07-31 14:34:57', 'Proceed', 'Gojek', 'MANDIRI', 155000, '', ''),
(13, 'blazerjaka@gmail.com', 'MB-3107190003', '2019-07-31 14:35:17', 'Proceed', 'Gojek', 'MANDIRI', 155000, '', ''),
(14, 'blazerjaka@gmail.com', 'MB-3107190004', '2019-07-31 14:36:23', 'Proceed', 'Gojek', 'MANDIRI', 155000, '', ''),
(15, 'blazerjaka@gmail.com', 'MB-3107190005', '2019-07-31 14:38:11', 'Proceed', 'TakeAway', 'BNI', 120000, '', ''),
(16, 'blazerjaka@gmail.com', 'MB-0108190001', '2019-08-01 12:17:30', 'Proceed', 'TakeAway', 'BNI', 15000, '', ''),
(17, 'blazerjaka@gmail.com', 'MB-0108190002', '2019-08-01 13:50:49', 'Proceed', 'Grab', 'BNI', 40000, '', ''),
(18, 'blazerjaka@gmail.com', 'MB-0108190003', '2019-08-01 16:38:37', 'Proceed', 'TakeAway', 'BNI', 40000, '', ''),
(19, 'blazerjaka@gmail.com', 'MB-0108190004', '2019-08-01 16:49:58', 'Proceed', 'TakeAway', 'MANDIRI', 40000, '', ''),
(20, 'blazerjaka@gmail.com', 'MB-0108190005', '2019-08-01 17:11:55', 'Proceed', 'Pos Indonesia', 'BCA', 25000, '', ''),
(21, 'blazerjaka@gmail.com', 'MB-0108190006', '2019-08-01 23:04:19', 'Proceed', 'TakeAway', 'MANDIRI', 90000, '', ''),
(22, 'blazerjaka@gmail.com', 'MB-0108190007', '2019-08-01 23:06:19', 'Proceed', 'Grab', 'BCA', 35000, '', ''),
(23, 'blazerjaka@gmail.com', 'MB-0308190001', '2019-08-03 14:00:28', 'Proceed', 'Gojek', 'BCA', 25000, '', ''),
(24, 'blazerjaka@gmail.com', 'MB-0308190002', '2019-08-03 14:37:55', 'Proceed', 'Grab', 'BRI', 15000, '', ''),
(25, 'blazerjaka@gmail.com', 'MB-0708190001', '2019-08-07 15:22:42', 'Cancelled', 'Grab', 'BCA', 40000, '', ''),
(26, 'blazerjaka@gmail.com', 'MB-0808190001', '2019-08-08 11:55:27', 'Paid', 'Gojek', 'BCA', 70000, '', ''),
(27, 'blazerjaka@gmail.com', 'MB-0908190001', '2019-08-09 09:42:44', 'Paid', 'Grab', 'BNI', 70000, '', ''),
(28, 'blazerjaka@gmail.com', 'MB-0908190002', '2019-08-09 10:03:16', 'Proceed', 'TakeAway', 'BCA', 75000, '', ''),
(29, 'blazerjaka@gmail.com', 'MB-2408190001', '2019-08-24 16:34:30', 'Proceed', 'TakeAway', 'BNI', 810000, '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `invoice_pengajuan`
--

CREATE TABLE `invoice_pengajuan` (
  `id` int(11) NOT NULL,
  `email_users` varchar(100) NOT NULL,
  `invoice_pengajuan` varchar(100) NOT NULL,
  `tanggal` datetime NOT NULL,
  `status` enum('Proceed','Shipped','Confirmed','Paid','Cancelled','Submitted') NOT NULL,
  `delivery` tinytext NOT NULL,
  `payment` tinytext NOT NULL,
  `total` int(11) NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `no_tracking` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `invoice_pengajuan`
--

INSERT INTO `invoice_pengajuan` (`id`, `email_users`, `invoice_pengajuan`, `tanggal`, `status`, `delivery`, `payment`, `total`, `gambar`, `no_tracking`) VALUES
(7, 'abatacreative@gmail.com', 'MB-R-1504180001', '2018-04-15 07:28:38', 'Paid', 'Pos Indonesia', 'Moneygram', 0, 'a8e602e8e4852ec9c00286eb80f84e8d.png', ''),
(8, 'ilhamhermawanzero@gmail.com', 'MB-R-1604180001', '2018-04-16 10:29:57', 'Submitted', 'Pos Indonesia', 'BankWire', 0, '', ''),
(9, 'abatacreative@gmail.com', 'MB-R-1604180002', '2018-04-16 10:58:41', 'Cancelled', 'Pos Indonesia', 'Paypal', 90, '', '');

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategory_produk`
--

CREATE TABLE `kategory_produk` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `slug` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `kategory_produk`
--

INSERT INTO `kategory_produk` (`id`, `nama`, `gambar`, `slug`) VALUES
(15, 'Liptint', 'ef0e22e6cfb11585703282aa7dc779ad.jpg', 'liptint'),
(16, 'Concealer', 'f3c28e59e20fa0be6f1e2a637bcb7790.png', 'concealer'),
(17, 'Eyeliner', '4dc46d0321b7f0604e4f0ec7aba6afa1.jpg', 'eyeliner'),
(19, 'Eyeshadow', '973e8ab678333ef27e7361e85bc04e04.jpg', 'eyeshadow'),
(20, 'Masker', '170a4f00996e9c1d428a80e2e1a328ae.jpg', 'masker'),
(22, 'Lipstik', '20f84f8c5724ebe803064d57cfc61da0.jpg', 'lipstik'),
(23, 'Lotion', '2383a2b4483204e5007a245329f10e24.jpg', 'lotion'),
(24, 'Serum', 'f07811a240f0aad4bf4281b47180520b.jpg', 'serum'),
(25, 'Body Gel', '94a0bad9816b609d2b6a3e6fa1cd7b96.jpg', 'body-gel'),
(26, 'Blush On', '8d95e60d05d366885c2fcc335be55608.jpg', 'blush-on'),
(27, 'Sabun Wajah', 'b1123be6a0a9c1c1061f568ef8fcf832.jpg', 'sabun-wajah'),
(28, 'Bedak Wajah', '28a9c2d0c337632760cbb79a02c99076.jpg', 'bedak-wajah'),
(29, 'Hand Body', '5769688c0dd72d723f826583b5ed4075.jpg', 'hand-body'),
(30, 'Highlighter', 'cc51ed3248820dfbbd58d0aeed7cd2ce.jpg', 'highlighter'),
(31, 'Eyebrow', '62e6b4ec82fa5a89085e1e163f0f5fae.jpg', 'eyebrow'),
(32, 'Lip Balm', '0cd8ef762edd3e2755cb268a23148525.jpg', 'lip-balm'),
(33, 'Lip Cream', 'aef5a5051f93e0a9973ebd5f8c71cea4.jpg', 'lip-cream'),
(34, 'Kutek', '3bb4108d391d75858225f92f3c446d8c.jpg', 'kutek'),
(35, 'Hutmun', '699f044dffda42c989c3c75ce13cf5dd.jpg', 'hutmun'),
(36, 'Mascara', '70e4fd48a18089c6c947101129fb17c4.jpg', 'mascara');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `code_invoice` varchar(100) NOT NULL,
  `qty` int(11) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `harga_produk` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `kategory_berat` enum('Gr','Kg') NOT NULL,
  `gambar_produk` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orders`
--

INSERT INTO `orders` (`id`, `code_invoice`, `qty`, `nama_produk`, `harga_produk`, `berat`, `kategory_berat`, `gambar_produk`) VALUES
(1, 'MB-0804180001', 2, 'aasds', 10, 1, 'Kg', '54ae63cc31090486d0c7775720c5f192.jpg'),
(2, 'MB-2307190001', 2, 'Hanamasui', 10, 10, '', 'b46ac592e8cc3c02b98ac848ef1258ed.jpg'),
(3, 'MB-2307190002', 1, 'Hanamasui', 10, 10, '', 'b46ac592e8cc3c02b98ac848ef1258ed.jpg'),
(4, 'MB-2307190003', 1, 'MIYAZHI', 10, 10, '', '7baa29be7587048672193a99858c3261.jpg'),
(5, 'MB-2507190001', 1, 'MIYAZHI', 10, 10, '', '7baa29be7587048672193a99858c3261.jpg'),
(6, 'MB-2507190002', 1, 'Hanamasui', 10, 10, '', 'b46ac592e8cc3c02b98ac848ef1258ed.jpg'),
(7, 'MB-2507190003', 1, 'MIYAZHI', 10, 10, '', '7baa29be7587048672193a99858c3261.jpg'),
(8, 'MB-2807190001', 1, 'H N', 40000, 20, '', '8fd3048e71d410c1d12847ae256ae843.jpg'),
(9, 'MB-3007190001', 1, 'BIOAQUA', 40000, 20, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(10, 'MB-3007190002', 2, 'BIOAQUA', 40000, 20, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(11, 'MB-3107190001', 3, 'BIOAQUA', 40000, 20, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(12, 'MB-3107190002', 3, 'BIOAQUA', 40000, 20, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(13, 'MB-3107190003', 3, 'BIOAQUA', 40000, 20, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(14, 'MB-3107190004', 3, 'BIOAQUA', 40000, 20, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(15, 'MB-3107190004', 1, 'Lumicer', 35000, 25, '', '744ca3f6174f9d1ef54b7995d8975d40.jpg'),
(16, 'MB-3107190005', 3, 'BIOAQUA', 40000, 17, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(17, 'MB-0108190001', 1, 'Implora', 15000, 45, '', 'c6d58ffccdfb28240fb66490f3ae7412.jpg'),
(18, 'MB-0108190002', 1, 'BIOAQUA', 40000, 14, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(19, 'MB-0108190003', 1, 'H N', 40000, 20, '', '8fd3048e71d410c1d12847ae256ae843.jpg'),
(20, 'MB-0108190004', 1, 'H N', 40000, 19, '', '8fd3048e71d410c1d12847ae256ae843.jpg'),
(21, 'MB-0108190005', 1, 'Focallure', 25000, 25, '', '93c2483e204f5ebb5d2cccb0d1f5fbee.jpg'),
(22, 'MB-0108190006', 1, 'BIOAQUA', 40000, 13, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(23, 'MB-0108190006', 2, 'Hanasui', 25000, 100, '', 'ca220c1a015bf8685cb53dc99da169f7.jpg'),
(24, 'MB-0108190007', 1, 'MIYAZHI', 35000, 0, '', '7baa29be7587048672193a99858c3261.jpg'),
(25, 'MB-0308190001', 1, 'QICIY', 25000, 30, '', 'b39ff15b3779c9f944150528647b3c1b.jpg'),
(26, 'MB-0308190002', 1, 'Implora', 15000, 44, '', 'c6d58ffccdfb28240fb66490f3ae7412.jpg'),
(27, 'MB-0708190001', 1, 'BIOAQUA', 40000, 12, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(28, 'MB-0808190001', 1, 'BIOAQUA', 40000, 11, '', 'a6098c3e566f8677524422476e344ca4.jpg'),
(29, 'MB-0808190001', 1, 'Collagen', 5000, 30, '', '321e6cdb2115998065bd9f82070c548f.jpg'),
(30, 'MB-0808190001', 1, 'Esenses', 25000, 30, '', '616e7d6356f5bb051083e644cc6e2af3.jpg'),
(31, 'MB-0908190001', 2, 'Lumicer', 35000, 24, '', '744ca3f6174f9d1ef54b7995d8975d40.jpg'),
(32, 'MB-0908190002', 1, 'BIOAQUA B.B', 75000, 15, '', '2b69e51e31196e68b222551592107ff5.jpg'),
(33, 'MB-2408190001', 22, 'Lumicer', 35000, 22, '', '744ca3f6174f9d1ef54b7995d8975d40.jpg'),
(34, 'MB-2408190001', 1, 'BIOAQUA', 40000, 1, '', 'a6098c3e566f8677524422476e344ca4.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders_pengajuan`
--

CREATE TABLE `orders_pengajuan` (
  `id` int(11) NOT NULL,
  `code_invoice_pengajuan` varchar(100) NOT NULL,
  `nama_produk` varchar(50) NOT NULL,
  `berat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `orders_pengajuan`
--

INSERT INTO `orders_pengajuan` (`id`, `code_invoice_pengajuan`, `nama_produk`, `berat`) VALUES
(1, 'MB-R-1504180001', 'coba', 1),
(2, 'MB-R-1504180001', 'aaa', 1),
(3, 'MB-R-1604180001', 'aaa', 1),
(4, 'MB-R-1604180001', 'coba', 1),
(5, 'MB-R-1604180002', 'aaa', 10),
(6, 'MB-R-1604180002', 'coba', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id` int(11) NOT NULL,
  `id_kategory` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `harga` int(11) NOT NULL,
  `berat` int(11) NOT NULL,
  `kategory_berat` enum('Gr','Kg','Pcs') NOT NULL,
  `tanggal` date NOT NULL,
  `gambar` varchar(50) NOT NULL,
  `slug` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id`, `id_kategory`, `nama`, `deskripsi`, `harga`, `berat`, `kategory_berat`, `tanggal`, `gambar`, `slug`) VALUES
(2, 19, 'MIYAZHI', 'Eyeshadow & Eyebrow', 35000, 12, 'Pcs', '2019-08-03', '7baa29be7587048672193a99858c3261.jpg', 'miyazhi'),
(3, 20, 'Naturgo', 'Masker', 20000, 20, 'Pcs', '2019-07-27', '4919d693fa8d591f9e7f5f9439de51c6.jpg', 'naturgo'),
(4, 23, 'Shining Whitening Lotion', 'Lotion', 20000, 10, 'Pcs', '2019-07-27', '59f83c3c62b8c18b6dee002873fed305.jpg', 'shining-whitening-lotion'),
(5, 24, 'Hanamasui', 'Serum', 25000, 10, 'Pcs', '2019-07-27', 'b46ac592e8cc3c02b98ac848ef1258ed.jpg', 'hanamasui'),
(6, 25, 'Hanasui', 'Peeling badan', 25000, 98, 'Pcs', '2019-07-27', 'ca220c1a015bf8685cb53dc99da169f7.jpg', 'hanasui'),
(7, 26, 'BIOAQUA', 'Perona Pipi', 40000, 0, 'Pcs', '2019-08-24', 'a6098c3e566f8677524422476e344ca4.jpg', 'bioaqua'),
(8, 27, 'Yu Chun Mei Gold', 'Facial Wash', 40000, 20, 'Pcs', '2019-07-27', '46885624b12ee8ae573eed7415855eca.jpg', 'yu-chun-mei-gold'),
(9, 28, 'BIOAQUA B.B', 'Bedak Wajah', 75000, 14, 'Pcs', '2019-07-27', '2b69e51e31196e68b222551592107ff5.jpg', 'bioaqua-bb'),
(10, 24, 'BIOAQUA Serum', 'Melembabkan dan melembutkan kulit wajah', 45000, 12, 'Pcs', '2019-07-27', 'a01eb48088d81cc2d745ca79c53e51f9.jpg', 'bioaqua-serum'),
(11, 29, 'H N', 'Memutihkan badan', 40000, 18, 'Pcs', '2019-07-27', '8fd3048e71d410c1d12847ae256ae843.jpg', 'h-n'),
(12, 19, 'Hasaya ', 'Pewarna Mata', 35000, 15, 'Pcs', '2019-07-27', '7e676a425507ca68f2525e21ca3fa471.jpg', 'hasaya'),
(13, 19, 'N S F', 'Pewarna Mata', 30000, 15, 'Pcs', '2019-07-27', 'aaf0766b351e6f5f054d2176e85f1a6c.jpg', 'n-s-f'),
(14, 19, 'Hasaya Eyeshadow', 'Pewarna Mata', 35000, 20, 'Pcs', '2019-07-27', 'b855e087c61bd315be9edd6aac619f39.jpg', 'hasaya-eyeshadow'),
(15, 26, 'Naked', 'Perona Pipi', 35000, 30, 'Pcs', '2019-07-27', 'bec18a1b0d1465ef1a073caab6838645.jpg', 'naked'),
(16, 26, 'Lumicer', 'Perona Pipi', 35000, 0, 'Pcs', '2019-07-27', '744ca3f6174f9d1ef54b7995d8975d40.jpg', 'lumicer'),
(17, 30, 'Focallure', 'Best Pipi', 25000, 24, 'Pcs', '2019-07-27', '93c2483e204f5ebb5d2cccb0d1f5fbee.jpg', 'focallure'),
(18, 31, 'Me Now', 'Pewarna Alis', 28000, 23, 'Pcs', '2019-07-27', 'f55136502826d72c77bb2f36217253d3.jpg', 'me-now'),
(19, 17, 'QICIY', 'Penegas Warna Mata', 25000, 29, 'Pcs', '2019-07-27', 'b39ff15b3779c9f944150528647b3c1b.jpg', 'qiciy'),
(20, 31, 'Etude', 'Pensil Alis', 25000, 23, 'Pcs', '2019-07-27', '83cfd5e314b41ef1aab3e948205fb42c.jpg', 'etude'),
(21, 32, 'Vaseline', 'Pelembab Bibir', 12000, 50, 'Pcs', '2019-07-27', '76a592cb7a0d4aa5d68337be00d8310b.jpg', 'vaseline'),
(22, 16, 'Me Now Concealer', 'Penyamar Warna Kulit', 40000, 7, 'Pcs', '2019-07-27', 'f00717c9caf1418a130672703556b311.jpg', 'me-now-concealer'),
(23, 33, 'Romantic Beauty', 'Lipstik Cair', 25000, 30, 'Pcs', '2019-07-27', 'c06fba388715d8cd58c9b62baf1cc58c.jpg', 'romantic-beauty'),
(24, 34, 'Implora', 'Pewarna Kuku', 15000, 43, 'Pcs', '2019-07-27', 'c6d58ffccdfb28240fb66490f3ae7412.jpg', 'implora'),
(25, 22, 'N Y X', 'Pewarna Bibir', 15000, 50, 'Pcs', '2019-07-27', 'f587cf6856532d47e4cb3310f5d61355.jpg', 'n-y-x'),
(26, 22, 'Etude lipstick', 'Pewarna Bibir', 15000, 30, 'Pcs', '2019-07-27', 'bc8437c5d08b642b00ed538e65e28db5.jpg', 'etude-lipstick'),
(27, 35, 'Hutmun', 'Masker penghilang komedo', 28000, 26, 'Pcs', '2019-07-27', 'fd8490b5c16fcadcdab7d48cbeb9a914.jpg', 'hutmun'),
(28, 36, 'BIOAQUA Mascara', 'Penebal Bulu Mata', 40000, 30, 'Pcs', '2019-07-27', 'ec8a77e3d87ef8e8ab3f9093bb249c15.jpg', 'bioaqua-mascara'),
(29, 17, 'Esenses', 'Penegas garis Mata', 25000, 29, 'Pcs', '2019-07-27', '616e7d6356f5bb051083e644cc6e2af3.jpg', 'esenses'),
(30, 31, 'V  I - V A', 'Pencil Alis', 10000, 35, 'Pcs', '2019-07-27', '9e5a54be0e34121cb417ecc479bfd89c.jpg', 'v-i-v-a'),
(31, 31, 'Popfeel', 'Penegas garis mata', 20000, 20, 'Pcs', '2019-07-27', '298b83c45a37c2f984f606e78d31799e.jpg', 'popfeel'),
(32, 33, 'MAYBELLINE', 'Pewarna bibir cair', 25000, 30, 'Pcs', '2019-07-27', 'ca29773bfc3944de6e05e1ed15364b99.jpg', 'maybelline'),
(33, 15, 'Kiss Beauty', 'Lipstik Cair', 18000, 30, 'Pcs', '2019-07-27', '042da1065c941e22e0078987915b3893.jpg', 'kiss-beauty'),
(34, 20, 'Collagen', 'Masker Bibir dan Masker Mata', 5000, 29, 'Pcs', '2019-07-27', '321e6cdb2115998065bd9f82070c548f.jpg', 'collagen'),
(35, 20, 'rorec blueberry', 'Masker Wajah', 10000, 30, 'Pcs', '2019-07-27', '09d68e83ea915ed66a645fd5e08c9eb0.jpg', 'rorec-blueberry'),
(36, 20, 'rorec olive', 'Masker Wajah', 10000, 20, 'Pcs', '2019-07-27', '9c8b5503a6bf3a15fb096dc35d689e4c.jpg', 'rorec-olive');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk_pengajuan`
--

CREATE TABLE `produk_pengajuan` (
  `id` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `produk_pengajuan`
--

INSERT INTO `produk_pengajuan` (`id`, `nama`) VALUES
(1, 'coba'),
(2, 'aaa');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ulasan`
--

CREATE TABLE `ulasan` (
  `id` int(11) NOT NULL,
  `id_produk` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `tgl` date NOT NULL,
  `rating_1` int(11) NOT NULL,
  `rating_2` int(11) NOT NULL,
  `rating_3` int(11) NOT NULL,
  `rating_4` int(11) NOT NULL,
  `rating_5` int(11) NOT NULL,
  `ulasan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `ulasan`
--

INSERT INTO `ulasan` (`id`, `id_produk`, `nama`, `email`, `tgl`, `rating_1`, `rating_2`, `rating_3`, `rating_4`, `rating_5`, `ulasan`) VALUES
(1, 2, 'Mamang Garok', 'samlekom@mamang.com', '2019-07-16', 0, 0, 1, 0, 0, 'blalavlalba'),
(7, 3, 'mamang garok', 'mamang@garok.com', '2019-07-03', 0, 0, 1, 0, 0, 'lalallala'),
(8, 5, 'Muhammad Jakaria', 'blazerjaka@gmail.com', '2019-07-25', 0, 0, 0, 1, 0, 'lalalal'),
(9, 7, 'Muhammad Jakaria', 'blazerjaka@gmail.com', '2019-07-28', 0, 0, 0, 1, 0, 'lalalal'),
(10, 9, 'Muhammad Jakaria', 'blazerjaka@gmail.com', '2019-07-28', 0, 0, 0, 1, 0, 'lalalal');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `fullname` varchar(100) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `google_id` varchar(50) DEFAULT NULL,
  `gender` varchar(10) DEFAULT NULL,
  `profile_image` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `email`, `fullname`, `password`, `firstname`, `lastname`, `google_id`, `gender`, `profile_image`) VALUES
(1, 'ilhamhermawanzero@gmail.com', 'ilham hermawan', NULL, 'ilham', 'hermawanzero', '114371165514474979531', NULL, 'https://lh6.googleusercontent.com/-tyhR9qryvlI/AAAAAAAAAAI/AAAAAAAAACs/tFUa6NK-NXg/photo.jpg'),
(2, 'abatacreative@gmail.com', 'ABATA Creative', NULL, 'ABATA', 'Creative', '110835049360059050682', NULL, 'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg'),
(3, 'blazerjaka@gmail.com', 'Muhammad Jakaria', '$2y$10$mm824t7fYN.2dF92RvnhjeowFZGUK6hNt5G416wy5Sgn8v8H5ilDq', 'Muhammad', 'Jakaria', '114395681173589222707', NULL, 'https://lh3.googleusercontent.com/-0uKLRI4kRm0/AAAAAAAAAAI/AAAAAAAAAWE/KAgXGqaMsXA/photo.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `alamat`
--
ALTER TABLE `alamat`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_users` (`email_users`),
  ADD KEY `email_users` (`email_users`);

--
-- Indexes for table `alamat_invoice`
--
ALTER TABLE `alamat_invoice`
  ADD PRIMARY KEY (`id`),
  ADD KEY `kode_invoice` (`code_invoice`),
  ADD KEY `email_users` (`email_users`);

--
-- Indexes for table `alamat_pengajuan`
--
ALTER TABLE `alamat_pengajuan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code_invoice_pengajuan` (`code_invoice_pengajuan`),
  ADD KEY `email_users` (`email_users`);

--
-- Indexes for table `apps_countries`
--
ALTER TABLE `apps_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ci_sessions`
--
ALTER TABLE `ci_sessions`
  ADD KEY `ci_sessions_timestamp` (`timestamp`);

--
-- Indexes for table `gambar_produk`
--
ALTER TABLE `gambar_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `invoice`
--
ALTER TABLE `invoice`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice` (`invoice`),
  ADD KEY `id_users` (`email_users`),
  ADD KEY `email_users` (`email_users`);

--
-- Indexes for table `invoice_pengajuan`
--
ALTER TABLE `invoice_pengajuan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `email_users` (`email_users`),
  ADD KEY `invoice_pengajuan` (`invoice_pengajuan`);

--
-- Indexes for table `kategory_produk`
--
ALTER TABLE `kategory_produk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code_invoice` (`code_invoice`);

--
-- Indexes for table `orders_pengajuan`
--
ALTER TABLE `orders_pengajuan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `code_invoice_pengajuan` (`code_invoice_pengajuan`);

--
-- Indexes for table `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nama` (`nama`),
  ADD KEY `id_kategory` (`id_kategory`),
  ADD KEY `slug` (`slug`);

--
-- Indexes for table `produk_pengajuan`
--
ALTER TABLE `produk_pengajuan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ulasan`
--
ALTER TABLE `ulasan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_produk` (`id_produk`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `google_id` (`google_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `alamat`
--
ALTER TABLE `alamat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `alamat_invoice`
--
ALTER TABLE `alamat_invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `alamat_pengajuan`
--
ALTER TABLE `alamat_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `apps_countries`
--
ALTER TABLE `apps_countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=246;
--
-- AUTO_INCREMENT for table `gambar_produk`
--
ALTER TABLE `gambar_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `invoice`
--
ALTER TABLE `invoice`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `invoice_pengajuan`
--
ALTER TABLE `invoice_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `kategory_produk`
--
ALTER TABLE `kategory_produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `orders_pengajuan`
--
ALTER TABLE `orders_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `produk`
--
ALTER TABLE `produk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `produk_pengajuan`
--
ALTER TABLE `produk_pengajuan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `ulasan`
--
ALTER TABLE `ulasan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `alamat`
--
ALTER TABLE `alamat`
  ADD CONSTRAINT `alamat_ibfk_1` FOREIGN KEY (`email_users`) REFERENCES `users` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `alamat_invoice`
--
ALTER TABLE `alamat_invoice`
  ADD CONSTRAINT `alamat_invoice_ibfk_1` FOREIGN KEY (`code_invoice`) REFERENCES `invoice` (`invoice`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `alamat_pengajuan`
--
ALTER TABLE `alamat_pengajuan`
  ADD CONSTRAINT `alamat_pengajuan_ibfk_1` FOREIGN KEY (`code_invoice_pengajuan`) REFERENCES `invoice_pengajuan` (`invoice_pengajuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `gambar_produk`
--
ALTER TABLE `gambar_produk`
  ADD CONSTRAINT `gambar_produk_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `invoice`
--
ALTER TABLE `invoice`
  ADD CONSTRAINT `invoice_ibfk_1` FOREIGN KEY (`email_users`) REFERENCES `users` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `invoice_pengajuan`
--
ALTER TABLE `invoice_pengajuan`
  ADD CONSTRAINT `invoice_pengajuan_ibfk_1` FOREIGN KEY (`email_users`) REFERENCES `users` (`email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`code_invoice`) REFERENCES `invoice` (`invoice`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `orders_pengajuan`
--
ALTER TABLE `orders_pengajuan`
  ADD CONSTRAINT `orders_pengajuan_ibfk_1` FOREIGN KEY (`code_invoice_pengajuan`) REFERENCES `invoice_pengajuan` (`invoice_pengajuan`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD CONSTRAINT `produk_ibfk_1` FOREIGN KEY (`id_kategory`) REFERENCES `kategory_produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `ulasan`
--
ALTER TABLE `ulasan`
  ADD CONSTRAINT `ulasan_ibfk_1` FOREIGN KEY (`id_produk`) REFERENCES `produk` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
