<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_alamat extends CI_Model {

	var $table = 'alamat';

	public function json() {
    $this->datatables->select("id, nama_penerima,IF(tipe_alamat='1','<span class=\"label label-success\">Main Address</span>','<span class=\"label label-info\">Other</span>') as tipe_alamat,nama_tipe ");
    $where = array('email_users' => $this->session->userdata('email'), );
    $this->datatables->where($where);
    $this->datatables->from($this->table);
    $this->datatables->add_column('view', '
    	<div align="center">
    	<a href="'.site_url("address/select/$1").'" class="btn btn-default btn-sm" title="View"><i class="fa fa-eye"></i></a>
    	<a href="'.site_url("address/edit/$1").'" class="btn btn-warning btn-sm" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
    	<a href="'.site_url("address/delete/$1").'" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash-o"></i></a>
      </div>', 'id');
    return $this->datatables->generate();
  }	

}

/* End of file M_alamat.php */
/* Location: ./application/models/M_alamat.php */