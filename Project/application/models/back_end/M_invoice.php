<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_invoice extends CI_Model {

	var $table = 'invoice';

  public function json() {
    $this->datatables->select("invoice.id,invoice.invoice,invoice.tanggal,
      IF(invoice.status = 'Proceed', '<span class=\"label label-warning\">Proceed</span>',IF(invoice.status = 'Shipped','<span class=\"label label-primary\">Shipped</span>',IF(invoice.status ='Confirmed','<span class=\"label label-success\">Confirmed</span>',IF(invoice.status ='Paid','<span class=\"label label-info\">Paid</span>',IF(invoice.status ='Cancelled','<span class=\"label label-default\">Cancelled</span>','Error' ))))) as status
      ,CONCAT('Rp ',invoice.total) as total,  delivery, payment,email_users");
    $this->datatables->from($this->table);
    $this->datatables->add_column('view', '
      <div align="center">
      <a class="btn btn-default btn-rounded btn-sm" href="'.site_url("back_end/invoice_detail/index/$1").'"><i class="fa fa-eye"></i></a>
      <a class="btn btn-default btn-rounded btn-sm" href="javascript:void(0)" title="Edit" onclick="edit($2)"><i class="fa fa-edit"></i></a>
      </div>', 'invoice,id');
    $this->datatables->join('orders','orders.code_invoice = invoice.invoice');
    $this->datatables->group_by('invoice.invoice');
    return $this->datatables->generate();
  }


}

/* End of file M_invoice.php */
/* Location: ./application/models/back_end/M_invoice.php */