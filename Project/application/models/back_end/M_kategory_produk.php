<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kategory_produk extends CI_Model {

	var $table = 'kategory_produk';

	public function json() {
		$this->datatables->select('id,nama,gambar,slug');
		$this->datatables->from($this->table);
		$this->datatables->add_column('view_gambar','<div align="center"> <img src="'.base_url('assets/front_end/img/').'$1" class="img-thumbnail img-circle" width="100" height="100"></div>', 'gambar');
		$this->datatables->add_column('view', '<div align="center">
			<a href="'.site_url('produk/index/$2').'" target="_blank" class="btn btn-success btn-rounded btn-sm"><i class="fa fa-eye"></i> </a>
			<a class="btn btn-default btn-rounded btn-sm" href="javascript:void(0)" title="Edit" onclick="edit($1)"><i class="fa fa-edit"></i></a>
			<a class="btn btn-default btn-rounded btn-sm" href="javascript:void(0)" title="Hapus" onclick="hapus($1)" ><i class="fa fa-trash"></i></a>
			</div>', 'id,slug');
		return $this->datatables->generate();
	}

}

/* End of file M_kategory_produk.php */
/* Location: ./application/models/back_end/M_kategory_produk.php */