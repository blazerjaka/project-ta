<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_tambah_gambar extends CI_Model {

	var $table = 'gambar_produk';

	public function json($where='') {
		$this->datatables->select('id,gambar');
		$this->datatables->where($where);
		$this->datatables->from($this->table);
		$this->datatables->add_column('gambar','<div align="center"> <img src="'.base_url('assets/front_end/images/produk/').'$1" class="img-thumbnail img-circle" width="100" height="100"></div>', 'gambar');
		$this->datatables->add_column('view', '<div align="center">
			<a class="btn btn-default btn-rounded btn-sm" href="javascript:void(0)" onclick="hapus($1)" ><i class="fa fa-trash"></i></a>
			</div>', 'id');
		return $this->datatables->generate();
	}	

}

/* End of file M_tambah_gambar.php */
/* Location: ./application/models/back_end/M_tambah_gambar.php */