<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_produk extends CI_Model {

	var $table = 'produk';

	public function json() {
		$this->datatables->select('id,nama,tanggal,
			CONCAT("Rp ", harga," / ",berat, " ", kategory_berat) as harga_berat,
			gambar,
			slug');
		$this->datatables->from($this->table);
		$this->datatables->add_column('gambar','<div align="center"> <img src="'.base_url('assets/front_end/images/produk/').'$1" class="img-thumbnail img-circle" width="100" height="100"></div>', 'gambar');
		$this->datatables->add_column('view', '<div align="center">
			<a class="btn btn-success btn-rounded btn-sm" href="'.site_url('produk/detail/$2').'" target="_blank"><i class="fa fa-eye"></i></a>
			<a class="btn btn-default btn-rounded btn-sm" href="'.site_url('back_end/produk/edit/$1').'" ><i class="fa fa-edit"></i></a>
			<a class="btn btn-default btn-rounded btn-sm" href="javascript:void(0)" onclick="hapus($1)" ><i class="fa fa-trash"></i></a>
			<a class="btn btn-primary btn-rounded btn-sm" href="'.site_url('back_end/tambah_gambar/index/$1').'" target="_blank">Tambah Gambar</a>
			</div>', 'id,slug');
		return $this->datatables->generate();
	}

}

/* End of file M_produk.php */
/* Location: ./application/models/back_end/M_produk.php */