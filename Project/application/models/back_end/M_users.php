<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_users extends CI_Model {

	var $table = 'users';

	public function json() {
		$this->datatables->select('id,fullname,email,profile_image');
		$this->datatables->from($this->table);
		$this->datatables->add_column('view_gambar','<div align="center"> <img src="$1" class="img-thumbnail img-circle" width="100" height="100"></div>', 'profile_image');
		// $this->datatables->add_column('view', '<div align="center">
		// 	<a class="btn btn-default btn-rounded btn-sm" href="javascript:void(0)" title="Hapus" onclick="hapus($1)" ><i class="fa fa-trash"></i></a>
		// 	</div>', 'id');
		return $this->datatables->generate();
	}

}

/* End of file M_users.php */
/* Location: ./application/models/back_end/M_users.php */