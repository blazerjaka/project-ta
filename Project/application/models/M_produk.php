<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_produk extends CI_Model {



    function ulasan($data)
    {
        $this->db->where($data);
        $this->db->order_by('tgl', 'DESC');
        return $this->db->get('ulasan');
    }

    function gambar_produk($data)
    {
        $this->db->where($data);
        return $this->db->get('gambar_produk');
    }


    function simpan_ulasan($data)
    {
        $this->db->insert('ulasan', $data);

    }

}

/* End of file M_produk.php */
/* Location: ./application/models/M_produk.php */