<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_history extends CI_Model {

  var $table = 'invoice';

  public function json() {
    $this->datatables->select("invoice.id,invoice.invoice,invoice.tanggal,
      IF(invoice.status = 'Proceed', '<span class=\"label label-warning\">Proceed</span>',IF(invoice.status = 'Shipped','<span class=\"label label-primary\">Shipped</span>',IF(invoice.status ='Confirmed','<span class=\"label label-success\">Confirmed</span>',IF(invoice.status ='Paid','<span class=\"label label-info\">Paid</span>',IF(invoice.status ='Cancelled','<span class=\"label label-default\">Cancelled</span>','Error' ))))) as status
      ,CONCAT('Rp ',invoice.total) as total");
    $where = array('email_users' => $this->session->userdata('email'), );
    $this->datatables->where($where);
    $this->datatables->from($this->table);
    $this->datatables->add_column('view', '
      <div align="center">
      <a href="'.site_url("history/select/$1").'" class="btn btn-template-main btn-sm" title="View"><i class="fa fa-eye"></i>View</a>
      </div>', 'invoice');
    $this->datatables->join('orders','orders.code_invoice = invoice.invoice');
    $this->datatables->group_by('invoice.invoice');
    return $this->datatables->generate();
  }

}

/* End of file M_history.php */
/* Location: ./application/models/M_history.php */