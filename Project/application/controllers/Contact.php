<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_history','history');
        $this->load->library('email');
    }

    public function index()
    {
        $title['title']='Contact Us';
        $this->load->view('front_end/temp_header',$title);
        $this->load->view('front_end/v_kontak');
        $this->load->view('front_end/temp_footer');
    }

    function sendmail()
    {
        // $nama_user=$_POST['nama_user'];
        // $email=$_POST['email'];
        // $subject=$_POST['judul'];
        // $message=$_POST['message'];

        // $this->email->to('adetiyaburhasanp45@gmail.com');
        // $this->email->from($email,$nama_user);
        // $this->email->subject($subject);
        // $this->email->message($message);
        // $this->email->send();
        echo "<script>
            alert('Pesan Berhasil Dikirim');";
            echo 'window.location.assign("'.site_url("").'")
            </script>';
    }
}

/* End of file Kontak.php */
/* Location: ./application/controllers/Kontak.php */