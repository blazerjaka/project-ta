<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	var $table = 'produk';

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('You Must Login!');";
			echo 'window.location.assign("'.site_url("back_end").'")
			</script>';
			// redirect('admin/welcome');
		}
		$this->load->model('back_end/M_produk','Model');
	}

	public function index()
	{
		$title = array('title' => ' Produk', );
		$this->load->view('back_end/temp_header',$title);
		$this->load->view('back_end/v_produk');
		$this->load->view('back_end/temp_footer');
	}

	public function json() {
		if ($this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo $this->Model->json();
		}
	}

	//hapus
	public function hapus($id)
	{
		if ($this->input->is_ajax_request()) {
			$where = array('id' => $id);
			$this->DButama->GetDBWhere($this->table,$where)->row();
			$this->DButama->DeleteDB($this->table,$where);
			echo json_encode(array("status" => TRUE));
		}

	}

	public function tambah()
	{
		$title = array('title' => 'Tambah Produk', );
		$data['kategory_produk'] =$this->db->order_by('nama', 'asc');
		$data['kategory_produk'] = $this->DButama->GetDB('kategory_produk');
		$this->load->view('back_end/temp_header',$title);
		$this->load->view('back_end/v_produk_tambah',$data);
		$this->load->view('back_end/temp_footer');
	}

	public function proses_tambah()
	{
		$this->load->library('form_validation');

		$config = array(
			array('field' => 'id_kategory','label' => "Kategory Produk",'rules' => 'required' ),
			array('field' => 'nama','label' => 'Nama Produk','rules' => 'required',),
			array('field' => 'harga','label' => 'Harga Produk','rules' => 'required|numeric'),
			array('field' => 'berat','label' => 'Berat Produk','rules' => 'required|numeric'),
			array('field' => 'kategory_berat','label' => 'Kategory Berat','rules' => 'required'),
			array('field' => 'deskripsi','label' => 'Deskripsi','rules' => 'required'),
		);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('error', validation_errors());
			$this->_Values();
			redirect('back_end/produk/tambah','refresh');
		}else{
			$DataUser  = array('nama' => $this->input->post('nama'));
			if ($this->DButama->GetDBWhere($this->table,$DataUser)->num_rows() == 1) {
				$this->_Values();
				$this->session->set_flashdata('error', 'Nama Produk Sama / Tidak Boleh Duplikat');
				redirect('back_end/produk/tambah','refresh');
			}else{
				$slug = url_title($this->input->post('nama'), 'dash', TRUE);
				$data = array(
					'id_kategory' => $this->input->post('id_kategory'),
					'nama' => $this->input->post('nama'),
					'tanggal' => date("Y-m-d"),
					'harga' => $this->input->post('harga'),
					'berat' => $this->input->post('berat'),
					'kategory_berat' => $this->input->post('kategory_berat'),
					'deskripsi' => $this->input->post('deskripsi'),
					'slug' => $slug,
				);
				
				$gambar = $_FILES['gambar']['name'];
				if(!empty($gambar))
				{
					$upload = $this->_do_upload();
					$data['gambar'] = $upload;
				}

				$this->DButama->AddDB($this->table,$data);
				redirect('back_end/produk','refresh');
			}
		}
	}

    //edit
	public function edit($id)
	{
		$cek = $this->DButama->GetDBWhere($this->table,array('id'=> $id));
		if ($cek->num_rows() == 1) {
			$title = array('title' => 'Edit Produk', );
			$data['produk'] = $cek->row();
			$data['kategory_produk'] =$this->db->order_by('nama', 'asc');
			$data['kategory_produk'] = $this->DButama->GetDB('kategory_produk');
			$this->load->view('back_end/temp_header',$title);
			$this->load->view('back_end/v_produk_edit',$data);
			$this->load->view('back_end/temp_footer');
		}else{
			redirect('error404','refresh');
		}
	}

	public function proses_edit()
	{
		$this->load->library('form_validation');
		$config = array(
			array('field' => 'id_kategory','label' => "Kategory Produk",'rules' => 'required' ),
			array('field' => 'nama','label' => 'Nama Produk','rules' => 'required',),
			array('field' => 'harga','label' => 'Harga Produk','rules' => 'required|numeric'),
			array('field' => 'berat','label' => 'Berat Produk','rules' => 'required|numeric'),
			array('field' => 'kategory_berat','label' => 'Kategory Berat','rules' => 'required'),
			array('field' => 'deskripsi','label' => 'Deskripsi','rules' => 'required'),
			array('field' => 'id','label' => 'Deskripsi','rules' => 'required|numeric'),
		);
		$this->form_validation->set_rules($config);
		if ($this->form_validation->run() == FALSE)
		{
			$this->session->set_flashdata('error', validation_errors());
			redirect('back_end/produk/edit/'.$this->input->post('id'),'refresh');
		}else{

			$where  = array('id' => $this->input->post('id'));
			$query = $this->DButama->GetDBWhere($this->table,$where);
			$row = $query->row();
			$where_username = array('nama' => $this->input->post('nama'));
			$cari_username = $this->DButama->GetDBWhere($this->table,$where_username);

			 // jika username tidak di ganti
			if ($row->nama == $this->input->post('nama')) {

				$slug = url_title($this->input->post('nama'), 'dash', TRUE);
				$data = array(
					'id_kategory' => $this->input->post('id_kategory'),
					'nama' => $this->input->post('nama'),
					'tanggal' => date("Y-m-d"),
					'harga' => $this->input->post('harga'),
					'berat' => $this->input->post('berat'),
					'kategory_berat' => $this->input->post('kategory_berat'),
					'deskripsi' => $this->input->post('deskripsi'),
					'slug' => $slug,
				);

				if(!empty($_FILES['gambar']['name']))
				{
					$upload = $this->_do_upload();
					$data['gambar'] = $upload;
				}

				$this->DButama->UpdateDB($this->table,$where,$data);
				redirect('back_end/produk','refresh');
        // jika username di ganti ternyata duplikat
			}elseif ($cari_username->num_rows() == 1) {
            # code...
				$this->session->set_flashdata('error', 'Nama Produk Sama / Tidak Boleh Duplikat');
				redirect('back_end/produk/edit/'.$this->input->post('id'),'refresh');
        // jika username di ganti
			}else{
				$slug = url_title($this->input->post('nama'), 'dash', TRUE);
				$data = array(
					'id_kategory' => $this->input->post('id_kategory'),
					'nama' => $this->input->post('nama'),
					'tanggal' => date("Y-m-d"),
					'harga' => $this->input->post('harga'),
					'berat' => $this->input->post('berat'),
					'kategory_berat' => $this->input->post('kategory_berat'),
					'deskripsi' => $this->input->post('deskripsi'),
					'slug' => $slug,
				);
				
				if(!empty($_FILES['gambar']['name']))
				{
					$upload = $this->_do_upload();
					$data['gambar'] = $upload;
				}

				$this->DButama->UpdateDB($this->table,$where,$data);
				redirect('back_end/produk','refresh');
			}
		}
	}

	private function _Values()
	{
		$this->session->set_flashdata('nama', set_value('nama') );
		$this->session->set_flashdata('id_kategory', set_value('id_kategory') );
		$this->session->set_flashdata('harga', set_value('harga') );
		$this->session->set_flashdata('berat', set_value('berat') );
		$this->session->set_flashdata('kategory_berat', set_value('kategory_berat') );
		$this->session->set_flashdata('deskripsi', set_value('deskripsi') );
	}

	private function _do_upload()
	{
		$config['upload_path']   = 'assets/front_end/images/produk/';
		$config['allowed_types'] = 'jpg|png';
		$config['remove_spaces'] = TRUE;
		$config['encrypt_name']  = TRUE;
        $config['file_name']     = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('gambar')) //upload and validate
        {
        	$this->session->set_flashdata('upload_error', 'Upload error: '.$this->upload->display_errors('',''));
        	$this->_Values();
        	redirect('back_end/produk/tambah','refresh');
        }
        return $this->upload->data('file_name');
    }

}

/* End of file Produk.php */
/* Location: ./application/controllers/back_end/Produk.php */