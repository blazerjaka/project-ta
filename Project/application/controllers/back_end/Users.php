<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

	var $table = 'users';

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('You Must Login!');";
			echo 'window.location.assign("'.site_url("back_end").'")
			</script>';
			// redirect('admin/welcome');
		}
		$this->load->model('back_end/M_users','Model');
	}

	public function index()
	{
		$title = array('title' => 'Users', );
		$this->load->view('back_end/temp_header',$title);
		$this->load->view('back_end/v_users');
		$this->load->view('back_end/temp_footer');
	}

	public function json() {
		if ($this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo $this->Model->json();
		}
	}

}

/* End of file Users.php */
/* Location: ./application/controllers/back_end/Users.php */