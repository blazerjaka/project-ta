<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('You Must Login!');";
			echo 'window.location.assign("'.site_url("back_end").'")
			</script>';
			// redirect('admin/welcome');
		}
	}

	public function index()
	{
		$title = array('title' => 'Home', );
		$data['proceed'] = $this->DButama->GetDBWhere('invoice', array('status' => 'proceed', ))->num_rows();
		$data['paid'] = $this->DButama->GetDBWhere('invoice', array('status' => 'paid', ))->num_rows();
		$data['shipped'] = $this->DButama->GetDBWhere('invoice', array('status' => 'shipped', ))->num_rows();
		$data['confirmed'] = $this->DButama->GetDBWhere('invoice', array('status' => 'confirmed', ))->num_rows();
		$data['cancelled'] = $this->DButama->GetDBWhere('invoice', array('status' => 'cancelled', ))->num_rows();
		$data['all'] = $this->DButama->GetDB('invoice')->num_rows();
		$this->load->view('back_end/temp_header',$title);
		$this->load->view('back_end/v_home',$data);
		$this->load->view('back_end/temp_footer');
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/back_end/Home.php */