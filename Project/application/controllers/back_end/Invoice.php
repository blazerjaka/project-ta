<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice extends CI_Controller {

	var $table = 'invoice';

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('You Must Login!');";
			echo 'window.location.assign("'.site_url("back_end").'")
			</script>';
			// redirect('admin/welcome');
		}
		$this->load->model('back_end/M_invoice','Model');
	}

	public function index()
	{
		$title = array('title' => ' Produk', );
		$this->load->view('back_end/temp_header',$title);
		$this->load->view('back_end/v_invoice');
		$this->load->view('back_end/temp_footer');
	}

	public function json() {
		if ($this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo $this->Model->json();
		}
	}

	 //edit
	public function edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$where = array('id' => $id);
			$data = $this->DButama->GetDBWhere($this->table,$where)->row();
			echo json_encode($data);
		}
	}
	public function update()
	{
		if ($this->input->is_ajax_request()) {
			$where  = array('id' => $this->input->post('id'));
			$data = array(
				'status' => $this->input->post('status'),
				'no_tracking' => $this->input->post('no_tracking'),
			);
			$this->DButama->UpdateDB($this->table,$where,$data);

			// $where  = array('id' => $this->input->post('id'));
			// $data = array(
			// 	'status' => $this->input->post('status'),
			// 	'no_tracking' => $this->input->post('no_tracking'),
			// );
			$this->DButama->UpdateDB($this->table,$where,$data);
			echo json_encode(array("status" => TRUE));
		}
	}

}


/* End of file Invoice.php */
/* Location: ./application/controllers/back_end/Invoice.php */