<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	var $table = 'admin';

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('admin_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('You Must Login!');";
			echo 'window.location.assign("'.site_url("back_end").'")
			</script>';
			// redirect('admin/welcome');
		}
		$this->load->model('back_end/M_admin','Model');
	}

	public function index()
	{
		$title = array('title' => 'Admin', );
		$this->load->view('back_end/temp_header',$title);
		$this->load->view('back_end/v_admin');
		$this->load->view('back_end/temp_footer');
	}

	public function json() {
		if ($this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo $this->Model->json();
		}
	}

	//hapus
	public function hapus($id)
	{
		if ($this->input->is_ajax_request()) {
			$where = array('id' => $id);
			$this->DButama->GetDBWhere($this->table,$where)->row();
			$this->DButama->DeleteDB($this->table,$where);
			echo json_encode(array("status" => TRUE));
		}

	}

    //input
	public function tambah()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate();
			$DataUser  = array('username' => $this->input->post('username'));
			if ($this->DButama->GetDBWhere($this->table,$DataUser)->num_rows() == 1) {
				$data = array();
				$data['inputerror'][] = 'username';
				$data['error_string'][] = 'Username sudah ada / tidak boleh duplikat';
				$data['status'] = FALSE;
				echo json_encode($data);
				exit();
			}else{
				$this->_validate();
				$pass=$this->input->post('password');
				$hash=password_hash($pass, PASSWORD_DEFAULT);
				$data = array(
					'username' => $this->input->post('username'),
					'nama' => $this->input->post('nama'),
					'password' => $hash,
				);
				$this->DButama->AddDB($this->table,$data);
				echo json_encode(array("status" => TRUE));
			}
		}
	}

    //edit
	public function edit($id)
	{
		if ($this->input->is_ajax_request()) {
			$where = array('id' => $id);
			$data = $this->DButama->GetDBWhere($this->table,$where)->row();
			echo json_encode($data);
		}
	}
	public function update()
	{
		if ($this->input->is_ajax_request()) {
			$this->_validate();
			$where  = array('id' => $this->input->post('id'));
			$query = $this->DButama->GetDBWhere($this->table,$where);
			$row = $query->row();
			$where_username = array('username' => $this->input->post('username'));
			$cari_username = $this->DButama->GetDBWhere($this->table,$where_username);
        // jika username tidak di ganti
			if ($row->username == $this->input->post('username')) {
            # code...
				if($this->input->post('password') == '') {
					$hash=$row->password;
				}else{
					$pass=$this->input->post('password');
					$hash=password_hash($pass, PASSWORD_DEFAULT);    
				}
				$data = array(
					'nama' => $this->input->post('nama'),
					'password' => $hash
				);
				$this->DButama->UpdateDB($this->table,$where,$data);
				echo json_encode(array("status" => TRUE));
        // jika username di ganti ternyata duplikat
			}elseif ($cari_username->num_rows() == 1) {
            # code...
				$data = array();
				$data['inputerror'][] = 'username';
				$data['error_string'][] = 'Username sudah ada / tidak boleh duplikat';
				$data['status'] = FALSE;
				echo json_encode($data);
				exit();
        // jika username di ganti
			}else{
				if($this->input->post('password') == '') {
					$hash=$row->password;
				}else{
					$pass=$this->input->post('password');
					$hash=password_hash($pass, PASSWORD_DEFAULT);    
				}

				$data = array(
					'username' => $this->input->post('username'),
					'nama' => $this->input->post('nama'),
					'password' => $hash,
				);
				$this->DButama->UpdateDB($this->table,$where,$data);
				echo json_encode(array("status" => TRUE));
			}
		}

	}

	private function _validate()
	{
		$data = array();
		$data['error_string'] = array();
		$data['inputerror'] = array();
		$data['status'] = TRUE;

		if($this->input->post('username') == '')
		{
			$data['inputerror'][] = 'username';
			$data['error_string'][] = 'First name is required';
			$data['status'] = FALSE;
		} 

		if($this->input->post('nama') == '')
		{
			$data['inputerror'][] = 'nama';
			$data['error_string'][] = 'First name is required';
			$data['status'] = FALSE;
		} 

		if($data['status'] === FALSE)
		{
			echo json_encode($data);
			exit();
		}
	}
}

/* End of file Admin.php */
/* Location: ./application/controllers/back_end/Admin.php */