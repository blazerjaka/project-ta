<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Invoice_detail extends CI_Controller {

	public function index($id)
	{
		$this->load->helper('rupiah');
		$cek = $this->DButama->GetDBWhere('invoice',array('invoice'=> $id));
		if ($cek->num_rows() == 1) {
			$title = array('title' => 'Detail Invoice', );
			$data['invoice'] = $cek->row();
			$data['orders'] 	= $this->DButama->GetDBWhere('orders', array('code_invoice' => $id, ));
			$data['alamat']		= $this->DButama->GetDBWhere('alamat_invoice', array('code_invoice' => $id));
			$this->load->view('back_end/temp_header',$title);
			$this->load->view('back_end/v_invoice_detail',$data);
			$this->load->view('back_end/temp_footer');
		}else{
			redirect('error404','refresh');
		}
	}

}

/* End of file Invoice_detail.php */
/* Location: ./application/controllers/back_end/Invoice_detail.php */