<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		//Do your magic here
		if ($this->session->userdata('user_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('You Must Login!');";
			echo 'window.location.assign("'.site_url("welcome/cart").'")
			</script>';
			// redirect('admin/welcome');
		}
		if ($this->cart->total_items() == 0) {
			# code...
			echo "<script>
			alert('Your cart is empty');";
			echo 'window.location.assign("'.site_url("welcome/cart").'")
			</script>';
		}
		

		$this->load->model('M_checkout','checkout');
		$this->load->helper('rupiah');
	}

	public function index()
	{

		$title['title']='Checkout';
		$this->load->view('front_end/temp_header',$title);
		$data['alamat']   = $this->DButama->GetDBWhere('alamat', array('email_users' => $this->session->userdata('email'), ));
		$this->load->view('front_end/v_checkout',$data);
		$this->load->view('front_end/temp_footer');
	}

	public function delivery()
	{
		# code...
		$title['title']='Checkout';
		$this->load->view('front_end/temp_header',$title);
		$this->load->view('front_end/v_delivery');
		$this->load->view('front_end/temp_footer');
	}

	public function payment()
	{
		# code...
		$title['title']='Checkout';
		$this->load->view('front_end/temp_header',$title);
		$this->load->view('front_end/v_payment');
		$this->load->view('front_end/temp_footer');
	}

	public function order_review()
	{
		# code...
		$title['title']='Checkout';
		$this->load->view('front_end/temp_header',$title);
		$this->load->view('front_end/v_order_review');
		$this->load->view('front_end/temp_footer');
	}

	public function proses_checkout1()
	{
		# code...
		$query = $this->DButama->GetDBWhere('alamat', array('email_users' => $this->session->userdata('email'), 'id' => $this->input->post('id_alamat') ));
		if ($query->num_rows() >= 1) {
			$sess_data['id_alamat'] = $this->input->post('id_alamat');
			$this->session->set_userdata($sess_data);
			redirect('checkout/delivery','refresh');
		}else{
			echo "<script>
			alert('You Have to Choose a Destination Address');";
			echo 'window.location.assign("'.site_url("checkout").'")
			</script>';
		}

	}

	public function proses_checkout2()
	{
		# code...
		if ($this->session->userdata('id_alamat') != '') {
			$this->load->library('form_validation');
			$this->form_validation->set_rules('delivery', 'Delivery', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				echo "<script>
				alert('You Must Choose a Shipping Method');";
				echo 'window.location.assign("'.site_url("checkout/delivery").'")
				</script>';
			}else{
				$sess_data['delivery'] = $this->input->post('delivery');
				$this->session->set_userdata($sess_data);	
				redirect('checkout/payment','refresh');
			}
		}else{
			redirect('checkout','refresh');
		}

	}

	// public function proses_checkout3()
	// {
	// 	# code...
	// 	if ($this->session->userdata('id_alamat') != '' and $this->session->userdata('delivery') !='' ) {
	// 		$this->load->library('form_validation');

	// 		$this->form_validation->set_rules('payment', 'Payment', 'required');
	// 		if ($this->form_validation->run() == FALSE)
	// 		{
	// 			echo "<script>
	// 			alert('You Must Choose Method of Purchase');";
	// 			echo 'window.location.assign("'.site_url("checkout/payment").'")
	// 			</script>';
	// 		}else{
	// 			$sess_data['payment'] = $this->input->post('payment');
	// 			$this->session->set_userdata($sess_data);
	// 			redirect('checkout/order_review','refresh');
	// 		}
	// 	}else{
	// 		redirect('checkout','refresh');
	// 	}

	// }

	public function proses_checkout3()
	{
		# code...
		if ($this->session->userdata('id_alamat') != '') {
			$this->load->library('form_validation');

			$this->form_validation->set_rules('payment', 'Payment', 'required');
			if ($this->form_validation->run() == FALSE)
			{
				echo "<script>
				alert('You Must Choose Method of Purchase');";
				echo 'window.location.assign("'.site_url("checkout/payment").'")
				</script>';
			}else{
				$sess_data['delivery'] = $this->input->post('delivery');
				$sess_data['payment'] = $this->input->post('payment');
				$this->session->set_userdata($sess_data);
				redirect('checkout/order_review','refresh');
			}
		}else{
			redirect('checkout','refresh');
		}

	}

	public function proses_checkout4()
	{
		# code...
		if ($this->session->userdata('id_alamat') != '' and $this->session->userdata('delivery') !='' and $this->session->userdata('payment') !='') {
			$cek_alamat = $this->DButama->GetDBWhere('alamat', array('id' => $this->session->userdata('id_alamat'), 'email_users' => $this->session->userdata('email')));
			if ($cek_alamat->num_rows() == 0) {
				redirect('checkout','refresh');
			}else{
				$invoice_id =  $this->checkout->find_invoice();
				$invoice 	= array(
					'invoice' => $invoice_id,
					'tanggal' => date('Y-m-d H:i:s'),
					'email_users' => $this->session->userdata('email'),
					'status'  => 'Proceed',
					'delivery' => $this->session->userdata('delivery'),
					'payment' => $this->session->userdata('payment'),
					'total' => $this->cart->total(),
				);
				$add1 = $this->DButama->AddDB('invoice',$invoice);
				$query = $this->DButama->GetDBWhere('alamat', array('id' => $this->session->userdata('id_alamat'), ));
				$row = $query->row();
				$alamat_invoice = array(
					'code_invoice' => $invoice_id,
					'email_users' => $this->session->userdata('email'),
					'nama_penerima' => $row->nama_penerima,
					'negara' => $row->negara,
					'provinsi' => $row->provinsi,
					'kota' => $row->kota,
					'kode_pos' => $row->kode_pos,
					'alamat' => $row->alamat,
					'no_hp' => $row->no_hp,
					'tipe_alamat' => $row->tipe_alamat,
					'nama_tipe' => $row->nama_tipe,
				);
				$add2 = $this->DButama->AddDB('alamat_invoice',$alamat_invoice);
				foreach ($this->cart->contents() as $items) {
				# code...
					$orders = array(
						'code_invoice' => $invoice_id,
						'nama_produk' => $items['name'],
						'qty' => $items['qty'],
						'berat' => $items['weight'],
						'kategory_berat' => $items['kategory_berat'],
						'harga_produk' => $items['price'],
						'gambar_produk' => $items['image'],
					);
					$add3 = $this->DButama->AddDB('orders',$orders);

					$DataWhere = array('id' => $items['id'], );
					$rowProduk = $this->db->get_where('produk',$DataWhere)->row();
					$hasil = $rowProduk->berat - $items['qty'];
					$DataObject = array('berat' => $hasil, );
					$this->db->update('produk', $DataObject, $DataWhere);
				}
				$this->cart->destroy();
				$this->session->unset_userdata(array('id_alamat','delivery','payment'));
				redirect('history','refresh');
			}
		}else{
			redirect('checkout','refresh');
		}

	}

}

/* End of file Checkout.php */
/* Location: ./application/controllers/Checkout.php */