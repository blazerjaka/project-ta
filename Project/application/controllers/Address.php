<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Address extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('user_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('Anda Harus Login!');";
			echo 'window.location.assign("'.site_url("welcome/cart").'")
			</script>';
		}
    $this->load->model('M_alamat','Model');
  }

  public function index()
  {
    $title['title']='My Address';
    $this->load->view('front_end/temp_header',$title);
    $this->load->view('front_end/v_alamat');
    $this->load->view('front_end/temp_footer');
  }

  public function tambah()
  {
    $title['title']='Add Address';
    $this->load->view('front_end/temp_header',$title);
    $query = $this->db->select('apps_countries.country_name');
    $query = $this->db->from('apps_countries');
    $query = $this->db->order_by('apps_countries.country_name', 'asc');
    $query = $this->db->get();
    $data['negara']	= $query;
    $this->load->view('front_end/v_alamat_tambah',$data);
    $this->load->view('front_end/temp_footer');
  }

  public function json() {
    if ($this->input->is_ajax_request()) {
      header('Content-Type: application/json');
      echo $this->Model->json();
    }
  }
   //hapus
  public function hapus($id)
  {
    if ($this->input->is_ajax_request()) {
     $where = array('id' => $id);
     $this->DButama->GetDBWhere($this->table,$where)->row();
     $this->DButama->DeleteDB($this->table,$where);
     echo json_encode(array("status" => TRUE));
   }
 }

 public function proses_tambah()
 {
  $this->load->library('form_validation');

  $config = array(
    array('field' => 'nama_penerima','label' => "Recipient's Name",'rules' => 'required' ),
    array('field' => 'tipe_alamat','label' => 'Address Type','rules' => 'required',),
    array('field' => 'nama_tipe','label' => 'Name Type','rules' => 'required'),
    array('field' => 'negara','label' => 'Country','rules' => 'required'),
    array('field' => 'provinsi','label' => 'State','rules' => 'required'),
    array('field' => 'kota','label' => 'City','rules' => 'required'),
    array('field' => 'kode_pos','label' => 'Postal Code','rules' => 'required|numeric'),
    array('field' => 'no_hp','label' => 'Phone','rules' => 'required|numeric'),
    array('field' => 'alamat','label' => 'Address','rules' => 'required')
  );
  $this->form_validation->set_rules($config);
  if ($this->form_validation->run() == FALSE)
  {
    $this->session->set_flashdata('error', validation_errors());
    $this->session->set_flashdata('nama_penerima', set_value('nama_penerima') );
    $this->session->set_flashdata('nama_tipe', set_value('nama_tipe') );
    $this->session->set_flashdata('negara', set_value('negara') );
    $this->session->set_flashdata('provinsi', set_value('provinsi') );
    $this->session->set_flashdata('kota', set_value('kota') );
    $this->session->set_flashdata('kode_pos', set_value('kode_pos') );
    $this->session->set_flashdata('no_hp', set_value('no_hp') );
    redirect('Address/tambah','refresh');
  }else{
    $data = array(
      'email_users' => $this->session->userdata('email'),
      'nama_penerima' => $this->input->post('nama_penerima'),
      'tipe_alamat' => $this->input->post('tipe_alamat'),
      'nama_tipe' => $this->input->post('nama_tipe'),
      'negara' => $this->input->post('negara'),
      'provinsi' => $this->input->post('provinsi'),
      'kota' => $this->input->post('kota'),
      'kode_pos' => $this->input->post('kode_pos'),
      'no_hp' => $this->input->post('no_hp'),
      'alamat' => $this->input->post('alamat'),
    );
    $this->DButama->AddDB('alamat',$data);
    redirect('Address','refresh');
  }

}


public function select($uri='')
{
  $query = $this->DButama->GetDBWhere('alamat', array('id' => $uri, 'email_users' => $this->session->userdata('email')));
  if ($query->num_rows() == 1) {
    $title['title']='Select Address';
    $this->load->view('front_end/temp_header',$title);
    $data['alamat']   = $query->row();
    $this->load->view('front_end/v_alamat_select',$data);
    $this->load->view('front_end/temp_footer');
  }else{
    redirect('address','refresh');
  }
}

public function edit($uri='')
{
  $query = $this->DButama->GetDBWhere('alamat', array('id' => $uri, 'email_users' => $this->session->userdata('email')));
  if ($query->num_rows() == 1) {
            # code...
    $title['title']='Edit Address';
    $this->load->view('front_end/temp_header',$title);
    $data['alamat']   = $query->row();
    $query = $this->db->select('apps_countries.country_name');
    $query = $this->db->from('apps_countries');
    $query = $this->db->order_by('apps_countries.country_name', 'asc');
    $query = $this->db->get();
    $data['negara']   = $query;
    $this->load->view('front_end/v_alamat_edit',$data);
    $this->load->view('front_end/temp_footer');
  }else{
    redirect('Address','refresh');
  }
}

public function proses_edit()
{

  $query = $this->DButama->GetDBWhere('alamat', array('id' => $this->security->xss_clean($this->input->post('id')) , 'email_users' => $this->session->userdata('email')));
  if ($query->num_rows() == 1) {
    $this->load->library('form_validation');

    $config = array(
      array('field' => 'nama_penerima','label' => "Recipient's Name",'rules' => 'required' ),
      array('field' => 'tipe_alamat','label' => 'Address Type','rules' => 'required',),
      array('field' => 'nama_tipe','label' => 'Name Type','rules' => 'required'),
      array('field' => 'negara','label' => 'Country','rules' => 'required'),
      array('field' => 'provinsi','label' => 'State','rules' => 'required'),
      array('field' => 'kota','label' => 'City','rules' => 'required'),
      array('field' => 'kode_pos','label' => 'Postal Code','rules' => 'required|numeric'),
      array('field' => 'no_hp','label' => 'Phone','rules' => 'required|numeric'),
      array('field' => 'alamat','label' => 'Address','rules' => 'required')
    );
    $this->form_validation->set_rules($config);
    if ($this->form_validation->run() == FALSE)
    {
      $this->session->set_flashdata('error', validation_errors());
      redirect('Address/edit/'.$this->input->post('id'),'refresh');
    }else{
      $data = array(
        'email_users' => $this->session->userdata('email'),
        'nama_penerima' => $this->input->post('nama_penerima'),
        'tipe_alamat' => $this->input->post('tipe_alamat'),
        'nama_tipe' => $this->input->post('nama_tipe'),
        'negara' => $this->input->post('negara'),
        'provinsi' => $this->input->post('provinsi'),
        'kota' => $this->input->post('kota'),
        'kode_pos' => $this->input->post('kode_pos'),
        'no_hp' => $this->input->post('no_hp'),
        'alamat' => $this->input->post('alamat'),
      );
      $where = array('id' => $this->input->post('id') );
      $this->DButama->UpdateDB('alamat',$where,$data);
      redirect('Address','refresh');
    }
  }else{
    echo "<script>
    alert(Not Your Right');";
    echo 'window.location.assign("'.site_url("Address").'")
    </script>';
  }
}

public function delete($uri='')
{
        # code...
  $query = $this->DButama->GetDBWhere('alamat', array('id' => $uri, 'email_users' => $this->session->userdata('email') ));
  if ($query->num_rows() == 1) {
            # code...
    $this->DButama->DeleteDB('alamat', array('id' => $uri, 'email_users' => $this->session->userdata('email') ));
    redirect('Address','refresh');
  }else{
    echo "<script>
    alert(Not Your Right');";
    echo 'window.location.assign("'.site_url("Address").'")
    </script>';
  }
}




}

/* End of file Alamat.php */
/* Location: ./application/controllers/Alamat.php */