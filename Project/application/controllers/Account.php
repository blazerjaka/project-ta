<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('user_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('You Must Login!');";
			echo 'window.location.assign("'.site_url("welcome/cart").'")
			</script>';
			// redirect('admin/welcome');
		}
	}

	public function index()
	{

		$title['title'] = 'MY Account';
		$this->load->view('front_end/temp_header',$title);
		$data['users'] = $this->DButama->GetDBWhere('users', array('email' => $this->session->userdata('email'), ))->row();
		$this->load->view('front_end/v_account',$data);
		$this->load->view('front_end/temp_footer');
	}

	

	public function proses_ganti_password()
	{
		if ($this->input->method() == "post") {
		# code...
			$old_password = $this->input->post('old_password');
			$pass=$this->input->post('password');
			$where = array('email' => $this->session->userdata('email') );
			$query = $this->DButama->GetDBWhere('users',$where);
			$row = $query->row();
			if (password_verify($old_password, $row->password)) {
				$hash=password_hash($pass, PASSWORD_DEFAULT);	
				$data = array(
					'password' => $hash,
				);
				$this->DButama->UpdateDB('users',$where,$data);
				echo "<script>
				alert('Change Password Success');";
				echo 'window.location.assign("'.site_url("account").'")
				</script>';	
			} else {
				echo "<script>
				alert('Change Password Failed');";
				echo 'window.location.assign("'.site_url("account").'")
				</script>';	
			}
		}

	}

	public function proses_edit_nama($value='')
	{
		$this->load->library('form_validation');
		if ($this->input->method() == "post") {
			$config = array(
				array('field' => 'fullname','label' => "Name",'rules' => 'required' ),
			);
			$this->form_validation->set_rules($config);
			if ($this->form_validation->run() == FALSE)
			{
				$this->session->set_flashdata('error', validation_errors());
				redirect('Address/tambah','refresh');
			}else{
				$data = array(
					'fullname' => $this->input->post('fullname'),
				);

				$where = array('email' => $this->session->userdata('email') );
				$this->DButama->UpdateDB('users',$where,$data);

				$sess_data['nama_user'] = $this->input->post('fullname');
				$this->session->set_userdata($sess_data);
				redirect('account','refresh');
			}
		}
	}

	public function cek_password()
	{
		# code...
		if ($this->input->method() == "post") {
			if (!empty($this->input->post('old_password'))) {
				$old_password = $this->input->post('old_password');
				$query = $this->DButama->GetDBWhere('users',array('email' => $this->session->userdata('email'), ));
				$row = $query->row();
				if (password_verify($old_password, $row->password)) {
					echo 'true'; 
				} else {
					echo 'false'; 
				}
			}
		}
	}

	public function add_password()
	{
		if ($this->input->method() == "post") {
			$where = array('email' => $this->session->userdata('email'), );
			$password =  $this->DButama->GetDBWhere('users', $where)->row()->password;
			if ($password == null) {
				if ($this->input->post('password') == $this->input->post('cpassword')) {
					$hash = $hash=password_hash($this->input->post('password'), PASSWORD_DEFAULT);	
					$data = array('password' => $hash, );
					$this->DButama->UpdateDB('users',$where,$data);
					echo "<script>alert('Add Your Password Success');history.go(-1);</script>";
				}else{
					echo "<script>alert('Your password and Re Enter Password not valid');history.go(-1);</script>";
				}
			}else{
				redirect('account','refresh');
			}
		}
	}

}

/* End of file Account.php */
/* Location: ./application/controllers/Account.php */