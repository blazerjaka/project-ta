<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->helper('rupiah');
    }
    public function index($url='',$page=0)
    {
        $cek = $this->DButama->GetDBWhere('kategory_produk', array('slug' => $url, ));
        if ($cek->num_rows() == 1) {
            $row = $cek->row();
            $title['title']= $row->nama;
            $this->load->view('front_end/temp_header',$title);

            $where = array('id_kategory' => $row->id, );
            $query =  $this->db->order_by('nama', 'asc');
            $query = $this->DButama->GetDBWhere('produk', array('id_kategory' => $row->id, ));
            $jml = $query;

            $config['base_url'] = base_url('').'produk/index/'.$url.'/';
            $config['total_rows'] = $jml->num_rows();;
            $config['per_page'] = 6;
            $config['uri_segment'] = 4;

            /*Class bootstrap pagination yang digunakan*/
            $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
            $config['full_tag_close'] ="</ul>";
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
            $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
            $config['next_tag_open'] = "<li>";
            $config['next_tagl_close'] = "</li>";
            $config['prev_tag_open'] = "<li>";
            $config['prev_tagl_close'] = "</li>";
            $config['first_tag_open'] = "<li>";
            $config['first_tagl_close'] = "</li>";
            $config['last_tag_open'] = "<li>";
            $config['last_tagl_close'] = "</li>";

            $this->pagination->initialize($config);

            $data['halaman']    = $this->pagination->create_links();
            $query = $this->db->order_by('nama', 'asc');
            $query = $this->db->where(array('id_kategory' => $row->id, ));
            $query = $this->db->get('produk', $config['per_page'], abs($page));
            $data['produk'] = $query;
            $this->load->view('front_end/v_produk',$data);
            $this->load->view('front_end/temp_footer');
        }else{
            redirect('error404','refresh');
        }
    }

    function viewall($page=0)
    {
        $title['title']='View All';
        $query =  $this->db->order_by('nama', 'asc');
        $query = $this->DButama->GetDB('produk');
        $jml = $query;

        $config['base_url'] = base_url('').'produk/viewall/';
        $config['total_rows'] = $jml->num_rows();;
        $config['per_page'] = 6;
        $config['uri_segment'] = 3;

        /*Class bootstrap pagination yang digunakan*/
        $config['full_tag_open'] = "<ul class='pagination pagination-sm' style='position:relative; top:-25px;'>";
        $config['full_tag_close'] ="</ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config);

        $data['halaman']    = $this->pagination->create_links();
        $query = $this->db->order_by('nama', 'asc');
        $query = $this->db->get('produk', $config['per_page'], $page);
        $data['produk'] = $query;
        
        $this->load->view('front_end/temp_header',$title);
        $this->load->view('front_end/v_produk',$data);
        $this->load->view('front_end/temp_footer');
    }


    function detail($url)
    {
        $cek = $this->DButama->GetDBWhere('produk', array('slug' => $url, ));
        if ($cek->num_rows() == 1) {
            $data_id = $cek->row()->id;
            $nama_data = $cek->row()->nama;
            $title['title']='Detail Product | '.$nama_data;
            $where  = array('produk.id' => $data_id, );
            $query = $this->db->select('produk.id,produk.nama,produk.deskripsi,produk.harga,produk.berat,produk.kategory_berat,produk.tanggal,produk.gambar,kategory_produk.nama as nama_kategory');
            $query = $this->db->where($where);
            $query = $this->db->from('produk');
            $query = $this->db->join('kategory_produk', 'produk.id_kategory = kategory_produk.id', 'left');
            $query = $this->db->get();
            $data['produk'] = $query;

            $query = $this->db->where(array('id_produk' => $data_id, ));
            $query = $this->db->order_by('tgl', 'DESC');
            $query = $this->db->get('ulasan');
            $data['ulasan'] = $query;
            $data['id_produk'] = $data_id;

            $query = $this->DButama->GetDBWhere('gambar_produk', array('id_produk' => $data_id, ));
            $data['gambar_produk'] = $query;

            $ul=0;$ul1=0;$ul2=0;$ul3=0;$ul4=0;
            $jm=1;$jml=0;

            $ulas=$this->db->from('ulasan');
            $ulas=$this->db->where('id_produk',$data_id );
            $ulas=$this->db->get();
            foreach ($ulas->result() as $key0) {
                $ul=$ul+$key0->rating_1;
                $ul1=$ul1+$key0->rating_2;
                $ul2=$ul2+$key0->rating_3;
                $ul3=$ul3+$key0->rating_4;
                $ul4=$ul4+$key0->rating_5;
            }
            $j5=5*$ul4;
            $j4=4*$ul3;
            $j3=3*$ul2;
            $j2=2*$ul1;
            $j1=1*$ul;
            $jml=$j5+$j4+$j3+$j2+$j1;
            $jm=$ul+$ul1+$ul2+$ul3+$ul4;
            error_reporting(0);
            $jumlah=$jml/$jm;
            $data['r1']=$ul;
            $data['r2']=$ul1;
            $data['r3']=$ul2;
            $data['r4']=$ul3;
            $data['r5']=$ul4;
            $data['jumlah']=$jumlah;
            $this->load->view('front_end/temp_header',$title);
            $this->load->view('front_end/v_detail',$data);
            $this->load->view('front_end/temp_footer');
        }else{
            redirect('error404','refresh');
        }
    }


    

    function tambah_ulasan()
    {
        if ($this->input->method() == "post") {
            $data_star = $this->input->post('star');
            if ($data_star== "1") {
                $rating_1=1;
                $rating_2=0;
                $rating_3=0;
                $rating_4=0;
                $rating_5=0;
            }elseif ($data_star== "2") {
                $rating_1=0;
                $rating_2=1;
                $rating_3=0;
                $rating_4=0;
                $rating_5=0;
            }elseif ($data_star== "3") {
                $rating_1=0;
                $rating_2=0;
                $rating_3=1;
                $rating_4=0;
                $rating_5=0;
            }elseif ($data_star== "4") {
                $rating_1=0;
                $rating_2=0;
                $rating_3=0;
                $rating_4=1;
                $rating_5=0;
            }elseif ($data_star== "5") {
                $rating_1=0;
                $rating_2=0;
                $rating_3=0;
                $rating_4=0;
                $rating_5=1;
            }
            $data = array(
                'id_produk' => $this->input->post('id_produk'),
                'nama' => $this->input->post('nama_user'),
                'email' => $this->input->post('email'),
                'tgl' => date('Y-m-d'),
                'rating_1' => $rating_1,
                'rating_2' => $rating_2,
                'rating_3' => $rating_3,
                'rating_4' => $rating_4,
                'rating_5' => $rating_5,
                'ulasan' => $this->input->post('ulasan'),
            );
            $insert = $this->DButama->AddDB('ulasan',$data);
            echo "<script>
            alert('Thanks for Sending a Review');";
            echo 'history.go(-1)
            </script>';
        }
    }

    function add_cart()
    {
        if ($this->input->is_ajax_request()) {
            $where  = array('id' => $this->input->post('id'), );
            $produk = $this->DButama->GetDBWhere('produk', $where)->row();
            $data = array(
                'id'      => $produk->id,
                'qty'           => 1,
                'price'   => $produk->harga,
                'weight'    => $produk->berat,
                'kategory_berat'    => $produk->kategory_berat,
                'name'    => $produk->nama,
                'image'  => $produk->gambar,
                'slug' => $produk->slug,
            );
            $this->cart->insert($data);
        $total_items = $this->cart->total_items(); //count total items
        die(json_encode(array('items'=>$total_items))); //output json
    }
}
}

/* End of file Produk.php */
/* Location: ./application/controllers/Produk.php */