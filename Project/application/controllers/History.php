<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('user_logged_in') !=  "Sudah_Loggin") {
			echo "<script>
			alert('Anda Harus Login!');";
			echo 'window.location.assign("'.site_url("welcome/cart").'")
			</script>';
			// redirect('admin/welcome');
		}
		$this->load->model('M_history','Model');
		$this->load->helper('rupiah');
	}

	public function index()
	{
		$title['title']='History';
		$this->load->view('front_end/temp_header',$title);
		$this->load->view('front_end/v_history');
		$this->load->view('front_end/temp_footer');
	}

	public function json() {
		if ($this->input->is_ajax_request()) {
			header('Content-Type: application/json');
			echo $this->Model->json();
		}
	}

	public function select($uri='')
	{
		# code...
		$query = $this->DButama->GetDBWhere('invoice', array('invoice' => $uri, 'email_users' => $this->session->userdata('email') ));
		if ($query->num_rows() == 1) {
			# code...
			$title['title']='History';
			$this->load->view('front_end/temp_header',$title);
			$row = $query->row();
			$data['invoice'] 	= $row;
			$data['orders'] 	= $this->DButama->GetDBWhere('orders', array('code_invoice' => $uri, ));
			$data['alamat']		= $this->DButama->GetDBWhere('alamat_invoice', array('code_invoice' => $uri));
			$this->load->view('front_end/v_history_select',$data);
			$this->load->view('front_end/temp_footer');
		}else{
			redirect('history','refresh');
		}
	}

	public function addgbrinv($value='')
	{
		if ($this->input->method() == "post") {
			$query = $this->DButama->GetDBWhere('invoice', array('invoice' => $this->input->post('invoice'), 'email_users' => $this->session->userdata('email'), 'status' => 'Proceed',));
			if ($query->num_rows() == 1) {
				if ($this->input->post('submit_cencel')) { 
					$data = array('status' => 'Cancelled',);
					$where = array('invoice' => $this->input->post('invoice') , 'email_users' => $this->session->userdata('email')  );
	      				$this->DButama->UpdateDB('invoice',$where,$data);
			        	redirect('history/select/'.$this->input->post('invoice'),'refresh');
				}else{
					$config['upload_path']   = 'assets/front_end/inv/';
					$config['allowed_types'] = 'jpg|png';
					$config['remove_spaces'] = TRUE;
					$config['encrypt_name']  = TRUE;
		        	$config['file_name']     = round(microtime(true) * 1000); //just milisecond timestamp fot unique name
			        $this->load->library('upload', $config);
			        if(!$this->upload->do_upload('gambar')) //upload and validate
			        {
			        	$this->session->set_flashdata('upload_error', 'Upload error: '.$this->upload->display_errors('',''));
			        	redirect('history/select/'.$this->input->post('invoice'),'refresh');
			        }else{

			        	$gambar = $this->upload->data('file_name');
			        	$data = array(
			        		'gambar' => $gambar,
			        		'status' => 'Paid',
			        	);
			        	$where = array('invoice' => $this->input->post('invoice') , 'email_users' => $this->session->userdata('email')  );
	      				$this->DButama->UpdateDB('invoice',$where,$data);
			        	redirect('history/select/'.$this->input->post('invoice'),'refresh');
			        }
		    	}
		    }else{
		    	redirect('history','refresh');
		    }
		}
	}



}

/* End of file History.php */
/* Location: ./application/controllers/History.php */