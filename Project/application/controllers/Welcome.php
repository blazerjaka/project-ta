<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('rupiah');
	}

	function index()
	{
		$title['title']='Welcome';
		$this->load->view('front_end/temp_header',$title);
		$data['kategory_produk'] =  $this->db->order_by('nama', 'asc');
		$data['kategory_produk'] = $this->DButama->GetDB('kategory_produk');
		$this->load->view('front_end/v_index',$data);
		$this->load->view('front_end/temp_footer');
	}

	public function get_tokens($value="") {
		if ($this->session->userdata('WebsiteMasterBotanical') == "SudahMasukMas") {
			echo $this->security->get_csrf_hash();
		}
	}

	public function recaptcha()
	{
		$recaptcha = $this->input->post('g-recaptcha-response');
		$response = $this->recaptcha->verifyResponse($recaptcha);
		if (!isset($response['success']) || $response['success'] <> true) {
			redirect('','refresh');
		} else {
			$google = $this->googleplus->loginURL();
			redirect($google,'refresh');
		}
	}

	public function login_google()
	{
		if ($this->session->userdata('user_logged_in') ==  "Sudah_Loggin") {
			redirect('account');
		}
		if (isset($_GET['code'])) {
			$this->googleplus->getAuthenticate();
			$this->session->set_userdata('user_logged_in',"Sudah_Loggin");
			$this->session->set_userdata('user_profile',$this->googleplus->getUserInfo());
			$profile_google = $this->session->userdata('user_profile');

			$where  = array('email' => $profile_google['email'], );
			$query = $this->DButama->GetDBWhere('users',$where);
			if ($query->num_rows() == 0) {
				$object = array(
					'google_id' => $profile_google['id'],
					'email' => $profile_google['email'],
					'fullname' => $profile_google['name'],
					'firstname' => $profile_google['given_name'],
					'lastname' => $profile_google['family_name'],
					'gender' => $profile_google['gender'],
					'profile_image' => $profile_google['picture']
				);
				$this->DButama->AddDB('users',$object);
				$fullname = $profile_google['name'];
				$email = $profile_google['email'];
			}else{
				$row = $query->row();
				$fullname = $row->fullname;
				$email = $row->email;
			}

			$sess_data['nama_user'] = $fullname;
			$sess_data['email'] = $email;
			$this->session->set_userdata($sess_data);
			$this->session->unset_userdata('admin_logged_in');
			redirect('account');
		}
	}

	function add_cart()
	{
		if ($this->input->is_ajax_request()) {
		$where  = array('id' => $this->input->post('id'), );
		$produk = $this->DButama->GetDBWhere('produk', $where)->row();
		$data = array(
			'id'      => $produk->id,
			'qty'     		=> 1,
			'price'   => $produk->harga,
			'weight'	=> $produk->berat,
			'kategory_berat'	=> $produk->kategory_berat,
			'name'    => $produk->nama,
			'image'  => $produk->gambar,
			'slug' => $produk->slug,
		);
		$this->cart->insert($data);
		$total_items = $this->cart->total_items(); //count total items
		die(json_encode(array('items'=>$total_items))); //output json
	}
	}

	function cart()
	{
		$title['title']='Cart';
		$this->load->view('front_end/temp_header',$title);
		$this->load->view('front_end/v_cart');
		$this->load->view('front_end/temp_footer');
	}

	function update_cart()
	{
		$qty = intval($this->input->post('qty'));
		if ($qty >= 0 ) {
			# code...
			$rowid = $this->input->post('rowid');
			$data = array(
				'rowid' => $rowid,
				'qty'   => $qty
			);
			$this->cart->update($data);
			redirect('welcome/cart','refresh');
		}else{
			echo "<script>
			alert('Inputan Anda Salah!');";
			echo 'window.location.assign("'.site_url("welcome/cart").'")
			</script>';
		}

	}

	function delete_cart($rowid)
	{
		$data = array(
			'rowid'   => $rowid,
			'qty'     => 0
		);
		$this->cart->update($data);
		redirect('welcome/cart','refresh');
	}


	function logout()
	{
		$user_data = $this->session->all_userdata();

		foreach ($user_data as $key => $value) {
			if ($key != 'session_id' && $key != 'ip_address' && $key != 'user_agent' && $key != 'last_activity') {
				$this->session->unset_userdata($key);
			}
		}
		$this->googleplus->revokeToken();
		redirect('welcome','refresh');
	}

	function about()
	{
		$title['title']='About ';
		$this->load->view('front_end/temp_header',$title);
		$this->load->view('front_end/v_tentangkami');
		$this->load->view('front_end/temp_footer');
	}

	function shopping_guide()
	{
		$title['title']='Shopping Guide Label-a';
		$this->load->view('front_end/temp_header', $title);
		$this->load->view('front_end/v_shopping_guide');
		$this->load->view('front_end/temp_footer');
	}
}
