<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<?php 
  if ($this->session->userdata('admin_logged_in') == 'Sudah_Loggin'){
    redirect('back_end/home','refresh');
  }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Login Admin</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Favicon and apple touch icons-->
  <link rel="shortcut icon" href="<?php echo base_url() ?>assets/front_end/images/icon/favicon.ico" type="image/x-icon" />
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?= base_url('assets/back_end') ?>/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url('assets/back_end') ?>/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?= base_url('assets/back_end') ?>/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url('assets/back_end') ?>/dist/css/AdminLTE.min.css">
  
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<!-- Recaptcha -->
<?=  $this->recaptcha->getScriptTag(); ?>
</head>
<body class="hold-transition login-page">
  <div class="login-box">
    <div class="login-logo">
      <a href="<?= site_url('') ?>"><b>Login</b>Admin</a>
  </div>
  <?= $this->session->flashdata('pesan'); ?>
  <?= $this->session->flashdata('error'); ?>
  <!-- /.login-logo -->
  <div class="login-box-body">
      <p class="login-box-msg">Sign in to start your session</p>
      <?= form_open('back_end/welcome/recaptcha'); ?>
      <div class="form-group has-feedback">
          <input type="email" name="username" class="form-control" placeholder="Email" required="" value="<?= $this->session->flashdata('email'); ?>">
          <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
          <input type="password" name="password" class="form-control" placeholder="Password" required="" value="<?= $this->session->flashdata('password'); ?>">
          <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">        
          <!-- /.col -->
          <div class="col-xs-12">
          <?= $this->recaptcha->getWidget() ?>
            </div>
          <div class="col-xs-12">
            <button type="submit" class="btn btn-success btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
    </div>
    <?= form_close(); ?>
    <hr>
    <!-- x -->
</div>
<!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="<?= base_url('assets/back_end') ?>/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url('assets/back_end') ?>/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
</body>
</html>
