<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/back_end/') ?>datatables/css/dataTables.bootstrap.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Invoice
    </h1>
    <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Invoice</a></li>
      <li class="active">Daftar Invoice</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Invoice</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div>
            <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
            <!-- <button class="btn btn-warning " onclick="backupdatabase()"><span class="fa fa-file-excel-o"></span> BackUP Database</button> -->
          </div><br>
          <div class="box-body table-responsive no-padding">
            <table id="mytable" class="table table-striped table-bordered table-hover" width="100%">
              <thead>
                <tr><th width="1%">No</th>
                  <th>Order</th>
                  <th>Email</th>
                  <th width="10%">Tanggal</th>
                  <th width="8%">Pengiriman</th>
                  <th width="8%">Pembayaran</th>
                  <th>Status</th>
                  <th>Total</th>
                  <th width="15%">Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- jQuery 3 -->
  <script src="<?= base_url('assets/back_end/') ?>bower_components/jquery/dist/jquery.min.js"></script>
  <!-- DataTables -->
  <script src="<?= base_url('assets/back_end/') ?>datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?= base_url('assets/back_end/') ?>datatables/js/dataTables.bootstrap.js"></script>
  <!-- page script -->
  <script type="text/javascript">

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };

    var table = $('#mytable').DataTable({
      oLanguage: {
        sProcessing: "loading..."
      },
      processing: true,
      serverSide: true,
      ajax: {"url": "<?= base_url() ?>back_end/invoice/json", "type": "POST"},
      columns: [
      {
        "data": "id",
        "orderable": false
      },
      {"data": "invoice"},
      {"data": "email_users"},
      {"data": "tanggal"},
      {"data": "delivery"},
      {"data": "payment"},
      {"data": "status"},
      {"data": "total"},
      {"data": "view","orderable": false}
      ],
      order: [[3, 'desc']],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });

    //fun reload
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax
      }

        //fun edit
        function edit(id)
        {
          save_method = 'update';
    $('#form')[0].reset(); // reset form on modals
    $('.form-group').removeClass('has-error'); // clear error class
    $('.help-block').empty(); // clear error string
    $.ajax({
      url : '<?php echo site_url("back_end/invoice/edit/'+id+'") ?>',
      type: "GET",
      dataType: "JSON",
      success: function(data)
      {
        $('[name="id"]').val(data.id);
        $('[name="invoice"]').val(data.invoice);
        $('[name="email_users"]').val(data.email_users);
        $('[name="status"]').val(data.status);
        $('[name="no_tracking"]').val(data.no_tracking);
            $('#modal_form').modal('show'); // show bootstrap modal when complete loaded
            $('.modal-title').text('Edit Status Invoice'); // Set title to Bootstrap modal title
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error get data from ajax');
          }
        });
  }

    //fun simpan
    function save()
    {
      refreshTokens();
    $('#btnSave').text('saving...'); //change button text
    $('#btnSave').attr('disabled',true); //set button disable
    var url;    
    url = "<?php echo site_url('back_end/invoice/update')?>";
    // ajax adding data to database
    var formData = new FormData($('#form')[0]);
    $.ajax({
      url : url,
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      dataType: "JSON",

      success: function(data)
      {
            if(data.status) //if success close modal and reload ajax table
            {
              $('#modal_form').modal('hide');
              reload_table();
            } else
            {
              for (var i = 0; i < data.inputerror.length; i++)
              {
                    $('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
                    $('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
                  }
                }
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
          },
          error: function (jqXHR, textStatus, errorThrown)
          {
            alert('Error adding / update data');
            $('#btnSave').text('save'); //change button text
            $('#btnSave').attr('disabled',false); //set button enable
          }
        });
  }

    // Aktif Navigasi
    $(document).ready(function() {
      $('#invoice').addClass('active treeview');
    });

    function refreshTokens() {
      var url = "<?= base_url()."welcome/get_tokens" ?>";
      $.get(url, function(theResponse) {
        /* you should do some validation of theResponse here too */
        $('#csrfHash').val(theResponse);;
      });
    }
  </script>
  <!--modal tambah dan edit -->
  <div class="modal fade" id="modal_form" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h3 class="modal-title"></h3>
        </div>
        <div class="modal-body form">
          <form action="#" id="form" class="form-horizontal">
            <input type="hidden" id="csrfHash" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
            <div class="modal-body">
              <input type="hidden" value="" name="id"/>
              <div class="form-group">
                <label >Order</label>
                <input type="text" class="form-control" name="invoice" disabled="" />
                <span class="help-block"></span>
              </div>
              <div class="form-group">
                <label >Email Users</label>
                <input type="text" class="form-control" name="email_users" disabled="" />
                <span class="help-block"></span>
              </div>
              <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                  <option value="Proceed">Proceed</option>
                  <option value="Paid">Paid</option>
                  <option value="Shipped">Shipped</option>
                  <option value="Confirmed">Confirmed</option>
                  <option value="Cancelled">Cancelled</option>
                </select>
              </div>
              <div class="form-group">
                <label >No Tracking</label>
                <input type="text" class="form-control" name="no_tracking" />
                <span class="help-block"></span>
              </div>
            </div>
          </form>
        </div>
        <div class="modal-footer">
          <div class="pull-right">
            <button type="button" id="btnSave" onclick="save()" class="btn btn-primary"><i class="fa fa-mail-forward "></i> Save</button>
          </div>
          <div class="pull-left">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-mail-reply"></i> Cancel</button>
          </div>
        </div>
      </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->