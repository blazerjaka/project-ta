<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Invoice Detail
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Invoice Detail</a></li>
      <li class="active"><?= $invoice->invoice ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Invoice Detail <?= $invoice->invoice?></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <th colspan="2">Product Name</th>
                  <th>Quatity</th>
                  <th>Price</th>
                  <th>Discout</th>
                  <th>Total</th>
                </tr>
              </thead>
              <tbody>
                <?php
                $hasil = 0;
                foreach ($orders->result() as $key):
                  ?>
                  <tr>
                    <td>
                      <a href="#">
                        <img width="100px" height="100px" src="<?php echo base_url(); ?>assets/front_end/images/produk/<?= $key->gambar_produk ?>">
                      </a>
                    </td>
                    <td><?= $key->nama_produk ?></td>
                    <td><?= $key->qty ?></td>
                    <td>Rp <?php echo rupiah($key->harga_produk); ?>,00</td>
                    <td>Rp 0.00</td>
                    <td>Rp <?php echo rupiah($key->harga_produk*$key->qty); ?>,00</td>
                  </tr>
                  <?php
                  $hasil = ($key->harga_produk*$key->qty)+$hasil;
                  endforeach ?>
                </tbody>
                <tfoot>
                  <tr>
                    <th colspan="5" class="text-right"><b>Total Order</b></th>
                    <th><b>Rp <?php echo rupiah($hasil); ?>,00</b></th>
                  </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.table-responsive -->
            <hr>
            <h3 class="text-uppercase">Your Shipping Data</h3>
            <?php foreach ($alamat->result() as $key => $row_alamat): ?>
              <div class="table-responsive">
                <table class="table table-bordered">
                  <tbody>
                    <tr>
                      <td width="200px">Invoce</td><td><?= $invoice->invoice ?></td>
                    </tr>
                    <tr>
                      <td>Recipient's name</td><td><?= $row_alamat->nama_penerima ?></td>
                    </tr>
                    <tr>
                      <td>Email</td><td><?= $row_alamat->email_users ?></td>
                    </tr>
                    <tr>
                      <td>Country</td><td><?= $row_alamat->negara ?></td>
                    </tr>
                    <tr>
                      <td>State</td><td><?= $row_alamat->provinsi ?></td>
                    </tr>
                    <tr>
                      <td>City</td><td><?= $row_alamat->kota ?></td>
                    </tr>
                    <tr>
                      <td>Postal Code</td><td><?= $row_alamat->kode_pos ?></td>
                    </tr>
                    <tr>
                      <td>No HP</td><td><?= $row_alamat->no_hp ?></td>
                    </tr>
                    <tr>
                      <td>Address </td><td><?php if ($row_alamat->tipe_alamat == '1'): ?>
                        Utama
                      <?php endif ?>
                      <?php if ($row_alamat->tipe_alamat == '2'): ?>
                        Other
                      <?php endif ?> : <?= $row_alamat->nama_tipe ?><br><hr><?= $row_alamat->alamat ?></td>
                    </tr>
                    <tr>
                      <td>Payment Method</td><td><?= $invoice->payment ?></td>
                    </tr>
                    <tr>
                      <td>Shipping Method</td><td><?= $invoice->delivery ?></td>
                    </tr>
                    <tr>
                      <td>Total</td><td>Rp <?php echo rupiah($hasil); ?>,00</td>
                    </tr>
                    <tr>
                      <td>No Tracking</td><td><b><?= $invoice->no_tracking ?></b></td>
                    </tr>
                    <tr>
                      <td>proof of payment</td><td><a target="_blank" href='<?php echo base_url(); ?>assets/front_end/inv/<?= $invoice->gambar ?>'><img width="100px" height="100px" src="<?php echo base_url(); ?>assets/front_end/inv/<?= $invoice->gambar ?>"></a></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            <?php endforeach ?>
            <!-- /.box-body -->
            <div class="box-footer">
              Footer
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->

        </section>
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <script src="<?= base_url('assets/back_end') ?>/bower_components/jquery/dist/jquery.min.js"></script>
      <!-- date-range-picker -->
      <script src="<?= base_url('assets/back_end') ?>/bower_components/moment/min/moment.min.js"></script>
      <script>    
       $(document).ready(function() {
        $('#invoice').addClass('active');
      });
    </script>
