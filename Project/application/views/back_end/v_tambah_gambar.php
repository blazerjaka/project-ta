<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
  function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

    oFReader.onload = function (oFREvent) {
      document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
  };
</script>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/back_end/') ?>datatables/css/dataTables.bootstrap.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Tambah Gambar
    </h1>
    <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Tambah Gambar</a></li>
      <li class="active">Daftar Gambar | <?= $produk->nama ?></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Daftar Gambar | <?= $produk->nama ?></h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div >
            <table class="table table-striped">
              <tbody>
                <tr>
                  <th width="15%">Nama Produk</th>
                  <td width="1%">:</td>
                  <td><?= $produk->nama ?></td>
                </tr>
                <tr>
                  <th>Harga Produk</th>
                  <td>:</td>
                  <td><?= '$'.$produk->harga ?></td>
                </tr>
                <tr>
                  <th>Berat</th>
                  <td>:</td>
                  <td><?= $produk->berat.'/'.$produk->kategory_berat ?></td>
                </tr>
                <tr>
                  <th>Kategory Produk</th>
                  <td>:</td>
                  <td><?= $kategory_produk ?></td>
                </tr>
              </tr>
              <tr>
                <th>Link Produk</th>
                <td>:</td>
                <td>
                  <a class="btn btn-success btn-rounded btn-sm" href="<?= site_url('produk/detail/'.$produk->slug) ?>" target="_blank"><i class="fa fa-eye"></i></a>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
        <br>
        
        <div class="box-body">
          <div class="form-group">
            <p><?= $this->session->flashdata('error'); ?></p>
          </div>
          <form action="<?= site_url('back_end/tambah_gambar/proses') ?>" enctype="multipart/form-data" method="post" accept-charset="utf-8">            
            <input type="hidden" name="id_produk" value="<?= $produk->id ?>" style="display: none">
            <input type="hidden" id="csrfHash" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
            <div class="form-group">
              <label for="exampleInputEmail1">Foto</label><br>
              <?= $this->session->flashdata('upload_error') ?>
              <div class="btn btn-default btn-file">
                <input id="uploadImage" type="file" name="gambar" onchange="PreviewImage();" /><i class="fa fa-paperclip"></i> Upload Gambar
              </div>
              <p class="help-block">Max. 2MB</p>
              <img id="uploadPreview" style="width:200px; height:200px;" />
            </div>
            <div class="form-group">
              <button class="btn btn-success form-control" onclick="refreshTokens()" type="submit"> Simpan</button>
              <hr>
              <a href="<?= site_url('back_end/produk') ?>" class="btn btn-default form-control "> Batalkan</a>
            </div>
            <? form_close(); ?>
          </div>
          <div class="box-body table-responsive no-padding">
            <table id="mytable" class="table table-striped table-bordered table-hover" width="100%">
              <thead>
                <tr><th width="1%">No</th>
                  <th>Gambar</th>
                  <th width="15%">Action</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <!-- jQuery 3 -->
  <script src="<?= base_url('assets/back_end/') ?>bower_components/jquery/dist/jquery.min.js"></script>
  <!-- DataTables -->
  <script src="<?= base_url('assets/back_end/') ?>datatables/js/jquery.dataTables.min.js"></script>
  <script src="<?= base_url('assets/back_end/') ?>datatables/js/dataTables.bootstrap.js"></script>
  <!-- page script -->
  <script type="text/javascript">

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };

    var table = $('#mytable').DataTable({
      oLanguage: {
        sProcessing: "loading..."
      },
      processing: true,
      serverSide: true,
      ajax: {"url": "<?= base_url() ?>back_end/tambah_gambar/json/<?= $produk->id ?>", "type": "POST"},
      columns: [
      {
        "data": "id",
        "orderable": false
      },
      {"data": "gambar","orderable": false},
      {"data": "view","orderable": false}
      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });

    //fun reload
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax
      }

    //Fun Hapus
    function hapus(id)
    {
      if(confirm('Anda yakin ingin menghapus data?'))
      {
            // ajax delete data to database
            $.ajax({
              url : '<?php echo site_url("back_end/tambah_gambar/hapus/'+id+'") ?>',
              type: "POST",
              dataType: "JSON",
              data: { <?= $this->security->get_csrf_token_name(); ?> : function () {
                refreshTokens();
                return $( "#csrfHash" ).val();
              }},
              success: function(data)
              {
                    //if success reload ajax table
                    $('#modal_form').modal('hide');
                    reload_table();
                  },
                  error: function (jqXHR, textStatus, errorThrown)
                  {
                    alert('Error deleting data');
                  }
                });
          }
        }

    // Aktif Navigasi
    $(document).ready(function() {
      $('#produk').addClass('active treeview');
    });

    function refreshTokens() {
      var url = "<?= base_url()."welcome/get_tokens" ?>";
      $.get(url, function(theResponse) {
        /* you should do some validation of theResponse here too */
        $('#csrfHash').val(theResponse);;
      });
    }
  </script>
  