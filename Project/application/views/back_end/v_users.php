<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url('assets/back_end/') ?>datatables/css/dataTables.bootstrap.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Users
    </h1>
    <ol class="breadcrumb">
        <li><a href=""><i class="fa fa-dashboard"></i> Users</a></li>
        <li class="active">Daftar Users</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

  <!-- Default box -->
  <div class="box">
    <div class="box-header with-border">
      <h3 class="box-title">Daftar Users</h3>

      <div class="box-tools pull-right">
        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
        title="Collapse">
        <i class="fa fa-minus"></i></button>
        <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
          <i class="fa fa-times"></i></button>
      </div>
  </div>
  <div class="box-body">
      <div>
          <button class="btn btn-primary" onclick="tambah()"><span class="fa fa-edit"></span> Tambah</button>
          <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
          <!-- <button class="btn btn-warning " onclick="backupdatabase()"><span class="fa fa-file-excel-o"></span> BackUP Database</button> -->
      </div><br>
      <div class="box-body table-responsive no-padding">
          <table id="mytable" class="table table-striped table-bordered table-hover" width="100%">
              <thead>
                  <tr><th width="1%">No</th>
                      <th width="10%">Gambar</th>
                      <th>Nama</th>
                      <th>Email</th>
                      <!-- <th width="1%">Action</th> -->
                  </tr>
              </thead>
              <tbody>
              </tbody>
          </table>
      </div>
  </div>
  <!-- /.box-body -->
  <div class="box-footer">
      Footer
  </div>
  <!-- /.box-footer-->
</div>
<!-- /.box -->

</section>
<!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- jQuery 3 -->
<script src="<?= base_url('assets/back_end/') ?>bower_components/jquery/dist/jquery.min.js"></script>
<!-- DataTables -->
<script src="<?= base_url('assets/back_end/') ?>datatables/js/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/back_end/') ?>datatables/js/dataTables.bootstrap.js"></script>
<!-- page script -->
<script type="text/javascript">

    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
    {
        return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $('#mytable').DataTable({
        oLanguage: {
            sProcessing: "loading..."
        },
        processing: true,
        serverSide: true,
        ajax: {"url": "<?= base_url() ?>back_end/users/json", "type": "POST"},
        columns: [
        {
            "data": "id",
            "orderable": false
        },
        {"data": "view_gambar","orderable": false},
        {"data": "fullname"},
        {"data": "email"},
        // {"data": "view","orderable": false}
        ],
        order: [[2, 'asc']],
        rowCallback: function(row, data, iDisplayIndex) {
            var info = this.fnPagingInfo();
            var page = info.iPage;
            var length = info.iLength;
            var index = page * length + (iDisplayIndex + 1);
            $('td:eq(0)', row).html(index);
        }
    });

    //fun reload
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax
    }

    // Aktif Navigasi
    $(document).ready(function() {
        $('#users').addClass('active treeview');
    });
</script>