<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Home
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Home</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="row">
            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-primary">
                <div class="inner">
                  <h3><?= $proceed ?></h3>
                  <p>Order Proceed</p>
                </div>
                <div class="icon">
                  <i class="fa  fa-spinner"></i>
                  <!-- <i class="ion ion-bag"></i> -->
                </div>
                <a href="<?= site_url('back_end/invoice') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-yellow">
                <div class="inner">
                  <h3><?= $paid ?></h3>
                  <p>Order Paid</p>
                </div>
                <div class="icon">
                  <i class="fa fa-credit-card"></i>
                  <!-- <i class="ion ion-bag"></i> -->
                </div>
                <a href="<?= site_url('back_end/invoice') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>


            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-navy">
                <div class="inner">
                  <h3><?= $shipped ?></h3>
                  <p>Order Shipped</p>
                </div>
                <div class="icon">
                  <i class="fa fa-plane"></i>
                  <!-- <i class="ion ion-bag"></i> -->
                </div>
                <a href="<?= site_url('back_end/invoice') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-green">
                <div class="inner">
                  <h3><?= $confirmed ?></h3>
                  <p>Order Confirmed</p>
                </div>
                <div class="icon">
                  <i class="fa fa-thumbs-up"></i>
                  <!-- <i class="ion ion-bag"></i> -->
                </div>
                <a href="<?= site_url('back_end/invoice') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-red">
                <div class="inner">
                  <h3><?= $cancelled ?></h3>
                  <p>Order Cancelled</p>
                </div>
                <div class="icon">
                  <i class="fa fa-thumbs-down"></i>
                  <!-- <i class="ion ion-bag"></i> -->
                </div>
                <a href="<?= site_url('back_end/invoice') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

            <div class="col-lg-2 col-xs-6">
              <!-- small box -->
              <div class="small-box bg-aqua">
                <div class="inner">
                  <h3><?= $all ?></h3>
                  <p>Order All</p>
                </div>
                <div class="icon">
                  <i class="fa fa-cart-plus"></i>
                  <!-- <i class="ion ion-bag"></i> -->
                </div>
                <a href="<?= site_url('back_end/invoice') ?>" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
              </div>
            </div>

          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <script src="<?= base_url('assets/back_end') ?>/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- date-range-picker -->
  <script src="<?= base_url('assets/back_end') ?>/bower_components/moment/min/moment.min.js"></script>
  <script>    
   $(document).ready(function() {
    $('#home').addClass('active treeview');
  });
</script>
