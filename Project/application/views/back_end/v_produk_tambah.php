<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<script type="text/javascript">
  function PreviewImage() {
    var oFReader = new FileReader();
    oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

    oFReader.onload = function (oFREvent) {
      document.getElementById("uploadPreview").src = oFREvent.target.result;
    };
  };
</script>
<!-- Select2 -->
<link rel="stylesheet" href="<?= base_url('assets/back_end/') ?>bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Produk
    </h1>
    <ol class="breadcrumb">
      <li><a href=""><i class="fa fa-dashboard"></i> Produk</a></li>
      <li class="active">Tambah Produk Produk</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">

    <!-- Default box -->
    <div class="box">
      <div class="box-header with-border">
        <h3 class="box-title">Tambah Produk</h3>

        <div class="box-tools pull-right">
          <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
          title="Collapse">
          <i class="fa fa-minus"></i></button>
          <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
            <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <div class="form-group">
            <p><?= $this->session->flashdata('error'); ?></p>
          </div>
          <?php $arb = array('enctype' => "multipart/form-data", );?>
          <?= form_open('back_end/produk/proses_tambah',$arb); ?>
          <div class="form-group">
            <label># Kategory Produk</label>
            <select class="form-control select2"  name="id_kategory" style="width: 100%;">
              <option value="">Choose option</option>
              <?php foreach ($kategory_produk->result() as $key) {
                ?>
                <option value="<?= $key->id ?>"><?= $key->nama ?></option>
                <?php
              } ?>
            </select>
          </div>
          <div class="form-group">
            <label># Nama Produk </label>
            <input type='text' name='nama' class='form-control' maxlength="100" placeholder="Nama Produk" value="<?= $this->session->flashdata('nama') ?>">
          </div>

          <div class="form-group">
            <div class="col-sm-6">
              <div class="form-group">
                <label># Harga Produk</label>
                <input type='text' name='harga' class='form-control' maxlength="11" placeholder="Harga Produk" onkeypress='return check_int(event)' value="<?= $this->session->flashdata('harga') ?>">
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label># Berat Produk</label>
                <input type='text' name='berat' class='form-control' maxlength="11" placeholder="Berat Produk" onkeypress='return check_int(event)' value="<?= $this->session->flashdata('berat') ?>">
              </div>
            </div>
            <div class="col-sm-3">
              <div class="form-group">
                <label># Kategory Berat</label>
                <select class="form-control"  name="kategory_berat">
                  <?php if ($this->session->flashdata('kategory_berat') == "Gr" ) {
                    echo 
                    '<option value="Gr" selected>Grams</option>
                     <option value="Kg">Kilos Grams</option>
                     <option value="Pcs">Pieces</option>';
                  }elseif ($this->session->flashdata('kategory_berat') == "Kg" ) {
                    echo 
                    '<option value="Gr">Grams</option>
                     <option value="Kg" selected>Kilos Grams</option>
                     <option value="Pcs">Pieces</option>';
                  }elseif ($this->session->flashdata('kategory_berat') == "Pcs" ) {
                    echo 
                    '<option value="Gr">Grams</option>
                     <option value="Kg">Kilos Grams</option>
                     <option value="Pcs" selected>Pieces</option>';
                  }
                  else{
                    echo 
                    '<option value="Gr">Grams</option>
                     <option value="Kg">Kilos Grams</option>
                     <option value="Pcs">Pieces</option>';
                  } ?>
                </select>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label># Deskripsi </label>
            <textarea name="deskripsi" class="form-control" style="height: 100px"><?= $this->session->flashdata('deskripsi') ?></textarea>
          </div>
          <div class="form-group">
            <label for="exampleInputEmail1">Foto</label><br>
            <?= $this->session->flashdata('upload_error') ?>
            <div class="btn btn-default btn-file">
              <input id="uploadImage" type="file" name="gambar" onchange="PreviewImage();" /><i class="fa fa-paperclip"></i> Upload Gambar
            </div>
            <p class="help-block">Max. 2MB</p>
            <img id="uploadPreview" style="width:200px; height:200px;" />
          </div>
          <div class="form-group">
            <button class="btn btn-success form-control" type="submit"> Simpan</button>
            <hr>
            <a href="<?= site_url('back_end/produk') ?>" class="btn btn-default form-control "> Batalkan</a>
          </div>
          <?= form_close(); ?>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          Footer
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script src="<?= base_url('assets/back_end/') ?>bower_components/jquery/dist/jquery.min.js"></script>
  <script src="<?= base_url('assets/back_end/') ?>bower_components/select2/dist/js/select2.full.min.js"></script>
  <script type="text/javascript">
    $('.select2').select2();
  </script>
  <script type="text/javascript">

    function check_int(evt) {
      var charCode = ( evt.which ) ? evt.which : event.keyCode;
      return ( charCode >= 48 && charCode <= 57 || charCode == 8 );
    }
  </script>
  <script>
    $(document).ready(function() {
      $('#produk').addClass('active treeview');
    //Date picker
  });

</script>