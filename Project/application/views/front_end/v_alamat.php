<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url("assets/"); ?>datatables/css/dataTables.bootstrap.css">
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="hidden-sm hidden-xs">My Address</h1>
                <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">My Address</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb">

                    <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                    </li>
                    <li>Address</li>
                </ul>

            </div>
        </div>
    </div>
</div>

<div id="content">
    <div class="container">


        <div class="row">

                    <!-- *** LEFT COLUMN ***
                       _________________________________________________________ -->

                       <div class="col-md-9" id="customer-orders">
                        <p class="lead">Shipping Destination Address</p>
                        <p>If you have any questions, please <a href="<?php echo site_url('contact') ?>">contact us</a>, our customer service center is always ready to serve.<br>Happy Shopping!</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <a href="<?= site_url('address/tambah'); ?>" class="btn btn-sm btn-warning"><i class="fa fa-paperclip"></i> Create Address</a>
                                    <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="table-responsive">
                                <table id="mytable" class="table table-striped table-bordered table-hover" width="100%">
                                    <thead>
                                        <tr>
                                            <th width="1px">No</th>
                                            <th>Recipient's Name</th>
                                            <th>Address Type</th>
                                            <th>Name Type</th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->

                        </div>
                        <!-- /.box -->

                    </div>
                    <!-- /.col-md-9 -->

                    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
                       _________________________________________________________ -->

                       <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
                         _________________________________________________________ -->
                         <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">User Profil</h3>
                            </div>

                            <div class="panel-body">

                               <ul class="nav nav-pills nav-stacked">
                                <li class="account">
                                    <a href="<?= site_url('account') ?>"><i class="fa fa-user"></i> My Account</a>
                                </li>
                                <li class="history">
                                    <a href="<?= site_url('history') ?>"><i class="fa fa-list"></i> Shopping history</a>
                                </li>
                                    <!-- <li class="">
                                        <a href="customer-wishlist.html"><i class="fa fa-heart"></i> My wishlist</a>
                                    </li> -->
                                    <li class="alamat">
                                        <a href="<?= site_url('address') ?>"><i class="fa fa-map-marker"></i> My Address</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                </div>


            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <!-- DataTables -->
        <script src="<?= base_url("assets/"); ?>datatables/js/jquery.dataTables.min.js"></script>
        <script src="<?= base_url("assets/"); ?>datatables/js/dataTables.bootstrap.js"></script>
        <script type="text/javascript">
         $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
         {
          return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $('#mytable').DataTable({
      oLanguage: {
        sProcessing: "loading..."
    },
    processing: true,
    serverSide: true,
    ajax: {"url": "<?= base_url() ?>address/json", "type": "POST"},
    columns: [
    {
        "data": "id",
        "orderable": false
    },
    {"data": "nama_penerima"},
    {"data": "tipe_alamat"},
    {"data": "nama_tipe"},
    {"data": "view","orderable": false},
    ],
    order: [[1, 'asc']],
    rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
    }
});

    //fun reload
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax
    }
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $('.alamat').addClass('active');
});
</script>