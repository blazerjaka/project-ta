        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="hidden-sm hidden-xs">Product <?php echo $title; ?></h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Product <?php echo $title; ?></h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">

                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>Product List - <?php echo $title; ?></li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div class="container" style="background: #fff;border-radius: 10px;border: 1px solid #ddd;padding-top: 10px">
            <div class="col-md-12">
                <div class="heading text-center">
                    <h2>Shopping list</h2>
                </div>
                <div class="row products">
                    <?php
                    foreach ($produk->result() as $key) {
                    ?>
                    <div class="col-md-4 col-sm-3" data-animate="fadeInUp">
                        <div class="box-image" style="width: 100%; height: 230px;">
                            <div class="image">
                                <img src="<?php echo base_url('assets/front_end/images/produk/'.$key->gambar); ?>" alt="" class="img-responsive">
                            </div>
                            <div class="bg"></div>
                            <div class="name">
                                <h3><a href="portfolio-detail.html"><?php echo $key->nama; ?></a></h3>
                            </div>
                            <div class="text">
                                <!-- <span class="coret">Rp. 70.000,00</span> -->
                                <p><b>Rp <?php echo rupiah($key->harga); ?>,00 </b><br><?= $key->berat ?>/<?= $key->kategory_berat ?> </p>
                                <?php
                                $attributes = array('class' => 'form-item');
                                 ?>
                                <?= form_open('', $attributes); ?>
                                    <input type="hidden" name="id" value="<?php echo $key->id; ?>">
                                    <a href="<?php echo site_url('produk/detail/'.$key->slug) ?>" class="btn btn-template-transparent-black"><i class="glyphicon glyphicon-eye-open"></i> Detail</a>
                                    <?= ($key->berat > 0 ? '<button type="submit" class="btn btn-template-transparent-primary"><i class="glyphicon glyphicon-shopping-cart"></i> Order</button>' : '')  ?>
                                <?= form_close(); ?>
                            </div>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                </div>
            </div>
            <div class="col-sm-12">
                <hr><br>
                <div class="pages">
                    <?php echo $halaman; ?> <!--Memanggil variable pagination-->
                </div>
            </div>
        </div>
        <br><br>


<script type="text/javascript">
    $(document).ready(function() {
        $('.produk').addClass('active');
    });


    $(document).ready(function(){
        $(".form-item").submit(function(e){
            var form_data = $(this).serialize();
            var button_content = $(this).find('button[type=submit]');
            button_content.html('Adding...'); //Loading button text

            $.ajax({ //make ajax request to cart_process.php
                url: "<?php echo base_url().'welcome/add_cart'; ?>",
                type: "POST",
                dataType:"json", //expect json value from server
                data: form_data
            }).done(function(data){ //on Ajax success
                $("#cart-info").html(data.items); //total items in cart-info element
                button_content.html('<i class="glyphicon glyphicon-shopping-cart"></i> Proceed'); //reset button text to original text
                alert("Your order has entered the shopping cart"); //alert user
                location.reload();
            })
            e.preventDefault();

        });
    });
</script>