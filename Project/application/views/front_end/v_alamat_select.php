<div id="heading-breadcrumbs">
    <div class="container">
       <div class="row">
          <div class="col-md-7">
                <h1 class="hidden-sm hidden-xs">My Address</h1>
                <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">My Address</h1>
         </div>
         <div class="col-md-5">
             <ul class="breadcrumb">
                <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                </li>
                <li><a href="<?php echo site_url('address') ?>"> Address</a>
                </li>
                <li>View Address</li>
            </ul>
        </div>
    </div>
</div>
</div>

<div id="content" class="clearfix">

	<div class="container">

		<div class="row">

            <div class="col-md-9 clearfix" id="customer-account">
                <div class="box clearfix" style="padding: 10px">
                   <div class="heading">
                      <h3 class="text-uppercase">My Address</h3>
                  </div>
                  <div class="row">
                    <div class="col-sm-12">
                        <div class="form-group">
                            <label for="firstname">Recipient's Name</label>
                            <input type="text" class="form-control" value="<?= $alamat->nama_penerima ?>" disabled="">
                        </div>
                    </div>
                    <div class="col-sm-12">
                        <div class="form-group">
                           <label for="firstname">Address Type</label>
                           <?php
                           if ($alamat->tipe_alamat == '1') {
                                                    # code...
                            ?>
                            <input type="text" class="form-control" value="Main Address" disabled="">
                            <?php
                        }else{
                            ?>
                            <input type="text" class="form-control" value="Other" disabled="">
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="firstname">Name Type</label>
                        <input type="text" class="form-control" value="<?= $alamat->nama_tipe ?>" disabled="">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="firstname">Country/ State/ City </label>
                        <input type="text" class="form-control" value="<?= $alamat->negara.'/ '.$alamat->provinsi.'/ '.$alamat->kota ?>" disabled="">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="firstname">Postal Code</label>
                        <input type="text" class="form-control" value="<?= $alamat->kode_pos ?>" disabled="">
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="firstname">Address</label>
                        <textarea class="form-control" disabled=""><?= $alamat->alamat ?></textarea>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="form-group">
                        <label for="firstname">No Handphone</label>
                        <input type="text" class="form-control" value="<?= $alamat->no_hp ?>" disabled="">
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <hr>
            <div class="row">
                <div class="col-sm-3 text-center">
                    <button onclick="history.go(-1)" class="btn btn-lg btn-default" title="Kembali" style="width: 100%"><i class="glyphicon glyphicon-chevron-left"></i> Back</button>
                </div>
                <div class="col-sm-9 text-center">
                    <a href="<?= site_url('address/edit/'.$alamat->id) ?>" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-pencil-square-o"></i> Edit Address</a>

                </div>

            </div>


        </div>

    </div>
    <!-- /.col-md-9 -->

    <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
                        _________________________________________________________ -->

                        <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
                            _________________________________________________________ -->
                            <div class="panel panel-default sidebar-menu">

                               <div class="panel-heading">
                                <h3 class="panel-title">User Profil</h3>
                            </div>

                            <div class="panel-body">

                                <ul class="nav nav-pills nav-stacked">
                                    <li class="account">
                                        <a href="<?= site_url('account') ?>"><i class="fa fa-user"></i> My Account</a>
                                    </li>
                                    <li class="history">
                                        <a href="<?= site_url('history') ?>"><i class="fa fa-list"></i> Shopping History</a>
                                    </li>
                                    <!-- <li class="">
                                        <a href="customer-wishlist.html"><i class="fa fa-heart"></i> My wishlist</a>
                                    </li> -->
                                    <li class="alamat">
                                        <a href="<?= site_url('address') ?>"><i class="fa fa-map-marker"></i> My Address</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('.alamat').addClass('active');
        });
    </script>