<?php
$url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$tag = array('Master Botanicals',$title.'|  Master Botanicals' ,'Borneo', 'Pontianak', 'Indonesia', 'Sell CRUSHED LEAF','Sell GOLD VEIN','Sell GREEN VEIN','Sell MORINGA VEIN','Sell RED VEIN','Sell WHITE VEIN','Sell YELLOW VEIN' ); 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="robots" content="all,follow">
    <meta name="googlebot" content="index,follow,snippet,archive">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo $title; ?> Label-a</title>
    <meta name="keywords" content="Master Botanicals, <?= $title ?> | Master Botanicals ,Borneo, Pontianak, Indonesia, CRUSHED LEAF,GOLD VEIN,GREEN VEIN,MORINGA VEIN,RED VEIN,WHITE VEIN,YELLOW VEIN"/>
    <link rel="canonical" href="<?= $url ?>" />
    <meta property="og:locale" content="id" />
    <meta property="og:type" content="Master Botanicals" />
    <meta property="og:title" content="<?= $title ?> | Master Botanicalsg" />
    <meta property="og:description" content="
    Master Botanicals is a ecommerce for botanical herbal, and you can choose which one based on your taste. This herbal domicilated in Indonesia, Province West Kalimantan, Pontianak Cty. For further information you may also look forward to our official accounts, Such as facebook, google plus, and instagram. 
    " />
    <meta property="og:url" content="<?= $url ?>" />
    <meta property="og:site_name" content="Master Botanicals" />
    <meta property="article:publisher" content="masterbotanicals.com" />
    <meta property="article:author" content="Admin Master Botanicals" />
    <?php foreach ($tag as $key): ?>
        <meta property="article:tag" content="<?= $key?>" />  
    <?php endforeach ?>
    <meta property="article:tag" content="Sell <?= $title ?>" />  
    <meta content='Indonesia' name='geo.placename'/>
    <meta content='MasterBotanicals.com' name='Author'/>
    <meta content='9BC10956954' name='blogcatalog'/>
    <meta content='Indonesian' name='language'/>
    <meta content='general' name='rating'/>
    <meta content='global' name='distribution'/>
    <meta content='blogger' name='generator'/>
    <meta content='aeiwi, alexa, alltheWeb, altavista, aol netfind, anzwers, canada, directhit, euroseek, excite, overture, go, google, hotbot. infomak, kanoodle, lycos, mastersite, national directory, northern light, searchit, simplesearch, Websmostlinked, webtop, what-u-seek, aol, yahoo, webcrawler, infoseek, excite, magellan, looksmart, bing, cnet, googlebot' name='search engines'/>
    <meta content="index follow" name="robots"/>

    <link href='<?= base_url('') ?>assets/front_end/css/fonts/fontawesome.css' rel='stylesheet' type='text/css'>

    <!-- Bootstrap and Font Awesome css -->
    <link rel="stylesheet" href="<?= base_url('') ?>assets/front_end/css/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/bootstrap/css/bootstrap.min.css">

    <!-- Css animations  -->
    <link href="<?php echo base_url() ?>assets/front_end/css/animate.css" rel="stylesheet">

    <!-- Theme stylesheet, if possible do not edit this stylesheet -->
    <link href="<?php echo base_url() ?>assets/front_end/css/style-makeup.css" rel="stylesheet" id="theme-stylesheet">


    <!-- Favicon and apple touch icons-->
    <!-- <link rel="shortcut icon" href="<?php echo base_url() ?>assets/front_end/images/icon/favicon.ico" type="image/x-icon" /> -->

    <!-- owl carousel css -->

    <link href="<?php echo base_url() ?>assets/front_end/css/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/front_end/css/owl.theme.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>assets/front_end/css/style-preloader.css" rel="stylesheet" type="text/css" media="all" />
    <!-- js -->
    <script type="text/javascript" src="<?php echo base_url() ?>assets/front_end/js/jquery-2.1.4.min.js"></script>

    <!-- Recapcha -->
    <?=  $this->recaptcha->getScriptTag(); ?>
</head>

<body style="background: #fafafa">
    <!-- Preloader -->
    <!-- Start of LiveChat (www.livechatinc.com) code -->
<script type="text/javascript">
  window.__lc = window.__lc || {};
  window.__lc.license = 11040582;
  (function() {
    var lc = document.createElement('script'); lc.type = 'text/javascript'; lc.async = true;
    lc.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'cdn.livechatinc.com/tracking.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(lc, s);
  })();
</script>
<noscript>
<a href="https://www.livechatinc.com/chat-with/11040582/" rel="nofollow">Chat with us</a>,
powered by <a href="https://www.livechatinc.com/?welcome" rel="noopener nofollow" target="_blank">LiveChat</a>
</noscript>
<!-- End of LiveChat code -->

    <script type="text/javascript">
        $(window).load(function() { $("#loading").fadeOut("slow"); })
    </script>

    <div id="loading">
        <!-- <img alt="logo" src="<?php echo base_url() ?>assets/front_end/images/logo/loader.png" align="center" style="position: relative; top: 200px;" width="200"><br><br> -->
        <br><br><br><br>
        <img alt="logo" src="<?php echo base_url() ?>assets/front_end/images/logo/loader2.gif" align="center" style="position: relative; top: 200px;" width="120">
    </div>

    <div id="all">

            <header>
                 <!-- *** TOP ***
                 _________________________________________________________ -->
                 <div id="top">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-4 contact">
                                <p class="hidden-md hidden-lg"><a href="tel:+6282253657711" data-animate-hover="pulse"><i class="fa fa-phone"></i></a>  <a href="mailto:support@masterbotanicals.com" data-animate-hover="pulse"><i class="fa fa-envelope"></i></a>
                                </p>
                            </div>
                            <div class="col-xs-8">
                                <div class="login" style="float: right;">
                                    <?php if ($this->session->userdata('user_logged_in') == 'Sudah_Loggin') {
                                        ?>
                                        <div class="dropdown active">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i> <?php echo $this->session->userdata('nama_user');  ?> <b class="caret"></b></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="<?php echo site_url('account'); ?>"><i class="fa fa-user"></i> <span class=" text-uppercase">Account</span></a>
                                                </li>
                                                <li><a href="<?php echo site_url('history'); ?>"><i class="fa fa-list"></i><span class=" text-uppercase">History</span></a>
                                                </li>
                                                <li><a href="<?php echo site_url('address'); ?>"><i class="fa fa-map-marker"></i> <span class=" text-uppercase">Address</span></a>
                                                </li>
                                                <li><a href="<?php echo site_url('welcome/logout'); ?>"><i class="fa fa-sign-in"></i> <span class=" text-uppercase">Logout</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <?php
                                    }else{
                                        ?>
                                        <a href="#" data-toggle="modal" data-target="#login-modal"><i class=" fa fa-sign-in"></i> <span class="text-uppercase">Login</span></a>
                                        <?php
                                    }
                                    ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <!-- *** TOP END *** -->


                <!-- *** NAVBAR ***
                _________________________________________________________ -->

                <div class="navbar-affixed-top" data-spy="affix" data-offset-top="200">

                    <div class="navbar navbar-default yamm" role="navigation" id="navbar">

                        <div class="container">
                            <div class="navbar-header">

                                <a class="navbar-brand home" href="<?php echo site_url(''); ?>">
                                    <!-- <img src="<?php echo base_url() ?>assets/front_end/images/logo/logo.png" alt="logo" class="hidden-xs hidden-sm" style="margin-top: -7px; width:245px">
                                    <img src="<?php echo base_url() ?>assets/front_end/images/logo/logo.png" alt="logo" class="visible-xs visible-sm" style="margin-top: -9px; width:230px"><span class="sr-only"></span> -->
                                </a>
                                <div class="navbar-buttons">
                                    <button type="button" class="navbar-toggle btn-template-main-nav" data-toggle="collapse" data-target="#navigation">
                                        <span class="sr-only">Toggle navigation</span>
                                        <i class="fa fa-align-justify"></i>
                                    </button>
                                </div>
                            </div>
                            <!--/.navbar-header -->

                            <div class="navbar-collapse collapse" id="navigation">
                                <ul class="nav navbar-nav navbar-right">
                                    <li class="home">
                                        <a href="<?php echo site_url(''); ?>"><i class="glyphicon glyphicon-home"></i> Home </a>
                                    </li>
                                    <li class="dropdown produk">
                                        <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown">Product <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                                <?php
                                                $kategory_produk =  $this->db->order_by('nama', 'asc');
                                                $kategory_produk = $this->DButama->GetDB('kategory_produk');
                                                ?>
                                                <?php
                                                $jumlah_kategory = $kategory_produk->num_rows();
                                                $no = 1;
                                                $hasilbagi = round(($jumlah_kategory/2), 0, PHP_ROUND_HALF_UP);
                                                 foreach ($kategory_produk->result() as $key):
                                                    if ($no == 1) {
                                                        echo '<div class="col-md-6">';
                                                    }
                                                ?>
                                                    <li><a href="<?php echo site_url('produk/index/'.$key->slug) ?>"></span> <?=  $key->nama ?></a>
                                                    </li>
                                                    <?php
                                                    if ($no == $hasilbagi) {
                                                        echo '</div><div class="col-md-6">';
                                                    }
                                                    $no++;
                                                    ?>
                                                <?php endforeach ?>
                                                <li><a href="<?php echo site_url('produk/viewall') ?>"></span> View All Category</a>
                                                </li>
                                                </div>

                                        </ul>
                                    </li>
                                    <li class="tentang">
                                        <a href="<?php echo site_url('welcome/about'); ?>">About Us </a>
                                    </li>
                                    <!-- <li class="kontak">
                                        <a href="<?php echo site_url('contact'); ?>">Contact Us </a>
                                    </li> -->
                                    <!-- <li class="blog">
                                        <a href="<?php //echo site_url('blog'); ?>">Blog </a>
                                    </li> -->
                                    <!-- keranjang -->
                                    <?php
                                    $jumlah_cart = $this->cart->total_items();
                                    ?>
                                    <li class="cart hidden-xs hidden-sm">
                                        <a class="btn btn-small" href="<?php echo site_url('welcome/cart'); ?>" style="border-bottom-width: 0px;">
                                            <i class="fa fa-shopping-cart"></i><span><i id="cart-info"></i><?php echo $jumlah_cart; ?></span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <?php
                            $jumlah_cart = $this->cart->total_items();
                            ?>
                            <div align="center" class="cart visible-xs visible-sm">
                                <a class="btn btn-small" href="<?php echo site_url('welcome/cart'); ?>" style="border-bottom-width: 0px;">
                                    <i class="fa fa-shopping-cart"></i>  <span><i id="cart-info"></i>Your Shopping Cart | <?php echo $jumlah_cart; ?></span>
                                </a>
                            </div>
                            <!--/.nav-collapse -->
                        </div>
                    </div>
                            <!-- /#navbar -->
                </div>
                        <!-- *** NAVBAR END *** -->
            </header>
        <!-- *** LOGIN MODAL ***
            _________________________________________________________ -->
            <?php if ($this->session->userdata('user_logged_in') != 'Sudah_Loggin') {
                ?>
                <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="Login" aria-hidden="true">
                    <div class="modal-dialog modal-sm" style="width: 340px;">

                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                <h4 class="modal-title" id="Login">Login Form</h4>
                            </div>
                            <div class="modal-body">
                                <form action="<?= site_url('welcome/recaptcha') ?>" method="POST">
                                    <input type="hidden" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
                                    <div class="form-group">
                                        <?= $this->recaptcha->getWidget() ?>
                                    </div>
                                    <div class="google">
                                        <button type="submit" class="btn btn-template-google" style="width: 100%; color: #565656;"><i class="fa fa-google-plus"></i> Login with Google</button>
                                    </div>
                                </form>
                            </div>
                            <!-- <div class="modal-footer">
                                <p>Powered by <a href="<?php echo site_url('') ?>">Master Botanicals</a></p>
                            </div> -->
                        </div>
                    </div>
                </div>
                <?php } ?>

                <!-- *** LOGIN MODAL END *** -->
