 <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="hidden-sm hidden-xs">Checkout - Address</h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Checkout - Address</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>Checkout - Address</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">

                <div class="row">

                    <div class="col-md-9 clearfix" id="checkout">

                        <div class="box">
                            <?= form_open('checkout/proses_checkout1'); ?>

                                <ul class="nav nav-pills nav-justified">
                                    <li class="active"><a href="#"><i class="fa fa-map-marker"></i><br>Address</a>
                                    </li>
                                    <li class="disabled"><a href="#"><i class="fa fa-truck"></i><br>Shipping Method</a>
                                    </li>
                                    <!-- <li class="disabled"><a href="#"><i class="fa fa-money"></i><br>Payment method</a>
                                    </li> -->
                                    <li class="disabled"><a href="#"><i class="fa fa-eye"></i><br>View Order</a>
                                    </li>
                                </ul>

                                <div class="content" style="padding: 10px">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <p>Choose the destination address of your order delivery.</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.row -->
                                    <?php if ($alamat->num_rows() >= 1): ?>
                                        <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                            <select class="form-control"  name="id_alamat" class="select">
                                            <?php
                                            foreach ($alamat->result() as $key) {
                                                # code...
                                                ?>
                                                <option value="<?= $key->id ?>"><?= $key->nama_tipe ?></option>
                                                <?php
                                                }
                                                 ?>
                                            </select>
                                            </div>
                                        </div>
                                        </div>
                                    <?php endif ?>
                                     <div class="row">
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <a href="<?= site_url('address/tambah'); ?>" class="btn btn-sm btn-warning"><i class="fa fa-paperclip"></i> Create Address</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="box-footer">
                                    <div class="col-sm-4">
                                        <div align="center">
                                            <a href="<?= site_url('welcome/cart') ?>" class="btn btn-default" style="width:220px"><i class="fa fa-chevron-left"></i>Back to Cart</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-4">
                                        <div align="center">
                                            <button type="submit" class="btn btn-template-main" style="width:220px">Shipping Method<i class="fa fa-chevron-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            <?= form_close(); ?>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

                     <div class="col-md-3">
                        <div class="box" id="order-summary">
                            <div class="box-header">
                                <h3>Cart Totals</h3>
                            </div>
                            <p class="text-muted" style="text-align: justify; padding: 10px;">Shipping and additional fees are calculated based on the value you have entered.</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr class="total">
                                            <td>Total</td>
                                            <th>Rp <?php echo rupiah($this->cart->total()); ?>,00</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.cart').addClass('active');
    });
</script>