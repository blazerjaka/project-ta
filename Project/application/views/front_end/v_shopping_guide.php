        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="hidden-sm hidden-xs">Shopping Guide</h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Shopping guide</h1>
                    </div>
                    <div class="col-md-6">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li><a href="<?php echo site_url('welcome/about') ?>">About Us</a></li>
                            <li>Shopping Guide</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <section class="bar background-white">
            <div class="heading text-center">
                <h2>Master Botanicals</h2>
            </div>
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12" data-animate="fadeInLeft">
                            <div class="panel-group accordion" id="accordionThree">
                                <div class="panel panel-default1 hidden-sm hidden-xs" align="center">
                                    <div class="col-md-4">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3a">
                                                    <h3>Buy</h3>
                                                    <img src="<?php echo base_url() ?>assets/front_end/images/support/a-1.png" alt="">
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3b">
                                                   <h3>Payment</h3>
                                                   <img src="<?php echo base_url() ?>assets/front_end/images/support/bayar.png" alt="">
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3c">
                                                    <h3>Receipt of Goods</h3>
                                                    <img src="<?php echo base_url() ?>assets/front_end/images/support/c-2.png" alt="">
                                                </a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>

                                <div class="panel" style="border:none;">
                                    <div class="panel-heading hidden-md hidden-lg">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3a">
                                                <h3>Buy</h3>
                                                <img src="<?php echo base_url() ?>assets/front_end/images/support/a-1.png" alt="">
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3a" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Buy a Product</h4><br>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="tabs">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a href="#tab4-1" data-toggle="tab"><i class="icon-star"></i><b>Search Product</b></a>
                                                            </li>
                                                            <li class=""><a href="#tab4-2" data-toggle="tab"><b>Add to Cart</b></a>
                                                            </li>
                                                            <li><a href="#tab4-3" data-toggle="tab"><b>Procces Your Order</b></a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="tab4-1">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/cari.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">To find the product you are looking for, Click the <b>Categories Product</b> in the Master Botanicals menu page.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab4-2">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/info.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">After you find the product you want, click on the <b>Product Details</b> to see the complete information about the product you choose, for example:</p>
                                                                        <ul class="urut">
                                                                            <li><b>Product Information</b>: contains all important information about the product you are viewing.</li>
                                                                            <li><b>Reviews</b>: contains reviews from buyers who have previously purchased the product.</li>
                                                                        </ul>
                                                                        <p align="justify">Already confident with the product you selected? Directly click <b>"Order Now".</b></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab4-3">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <h4>Your Shopping Cart</h4>
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/shopping.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">For the next step click icon <b>Cart</b> on the navigation menu, then you will be redirected to the shopping cart page.<br>Click next step to continue checkout process</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <hr>
                                                                    <div class="col-md-12">
                                                                        <h4>Enter Your Address</h4>
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/address.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">Then you will be directed to the shipping address page, <b>enter or choose a valid shipping address or that you have created before.</b><br>If you have not created an address, please enter your address data on your account's address page or you can <a href="<?php echo site_url('address') ?>" title="Address" target="_blank">Click Here</a>.<br>Then click next step to proceed to the shipping method.</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <hr>
                                                                    <div class="col-md-12">
                                                                        <h4>Choose Shipping Method</h4>
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/shipping.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">And the next you will be directed to the shipping method page, <b>choose one of the shipping methods you want.</b><br>Then click next step to proceed to payment method.</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <hr>
                                                                    <div class="col-md-12">
                                                                        <h4>Choose Payment Method</h4>
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/payment.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">Then you will be directed to the payment method page, <b>choose one of the payment methods you want.</b><br>Then click next step to proceed to Finalization you order.</p>
                                                                    </div>
                                                                </div>
                                                                <div class="row">
                                                                    <hr>
                                                                    <div class="col-md-12">
                                                                        <h4>Finalization Your Order</h4>
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/final.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">And you will be directed to the view order page, you can <b>see your order list, complete with shipping method, payment method and total order price you have made before.</b> If you are sure with your order product please click finalization and make payment to Master Botanicals.</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.tab-content -->
                                                    </div>
                                                    <!-- /.tabs -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel" style="border:none;">
                                    <div class="panel-heading hidden-md hidden-lg">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3b">
                                               <h3>Payment</h3>
                                               <img src="<?php echo base_url() ?>assets/front_end/images/support/bayar.png" alt="">
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3b" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Payment Guide</h4><br>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="tabs">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a href="#tab4-11" data-toggle="tab"><i class="icon-star"></i><b>View Order History</b></a>
                                                            </li>
                                                            <li class=""><a href="#tab4-12" data-toggle="tab"><b>Info Account Number</b></a>
                                                            </li>
                                                            <li><a href="#tab4-13" data-toggle="tab"><b>Send Proof of Payment</b></a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="tab4-11">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/history.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">Then you will be directed to the shopping history page, there is a shopping list you have made before. <br>There are some data such as
                                                                        <ul class="urut">
                                                                            <b>
                                                                            <li>No Invoice Order</li>
                                                                            <li>Order Date</li>
                                                                            <li>Total Price</li>
                                                                            <li>Order Status</li>
                                                                            </b>
                                                                        </ul>To see details of your order click details on any of the order</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab4-12">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/accountnumber.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">On the account number page you will find data information about Master Botanicals Account Number</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="tab-pane" id="tab4-13">
                                                                <div class="row">
                                                                    <div class="col-md-12">
                                                                        <img src="<?php echo base_url() ?>assets/front_end/images/support/proofpayment.png" alt="" width="100%">
                                                                        <hr>
                                                                        <p align="justify">To enter the shopping product detail page, make sure you have clicked details on one of the order.<br>There is a <b>Form to Send Proof of Payment</b>. Enter proof of payment with <b>Image Format (jpg/jpeg/png)</b>. Make sure the data to be sent is correct. After you send the proof of payment, please wait until we provide further confirmation. If you have any question please <a href="<?php echo site_url('contact') ?>" title="Contact Us">Contact Us!</a></p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <!-- /.tab-content -->
                                                    </div>
                                                    <!-- /.tabs -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel" style="border:none;">
                                    <div class="panel-heading hidden-md hidden-lg">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3c">
                                                <h3>Receipt of Goods</h3>
                                                <img src="<?php echo base_url() ?>assets/front_end/images/support/c-2.png" alt="">
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3c" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Receipt of Goods</h4><br>
                                                </div>
                                                <div class="col-md-12">
                                                    <div class="tabs">
                                                        <ul class="nav nav-tabs">
                                                            <li class="active"><a href="#tab4-21" data-toggle="tab"><i class="icon-star"></i><b>Menu1</b></a>
                                                            </li>
                                                            <li class=""><a href="#tab4-22" data-toggle="tab"><b>Menu2</b></a>
                                                            </li>
                                                            <li><a href="#tab4-23" data-toggle="tab"><b>Menu3</b></a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div class="tab-pane active" id="tab4-21">
                                                                This is tab one.
                                                            </div>
                                                            <div class="tab-pane" id="tab4-22">
                                                                This is tab two.
                                                            </div>
                                                            <div class="tab-pane" id="tab4-23">
                                                                This is tab three.
                                                            </div>
                                                        </div>
                                                        <!-- /.tab-content -->
                                                    </div>
                                                    <!-- /.tabs -->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>

                </div>
                <!-- /.container -->
            </div>
        </section>

<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tentang').addClass('active');
    });
</script>