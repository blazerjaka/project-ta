<div id="heading-breadcrumbs">
  <div class="container">
   <div class="row">
    <div class="col-md-7">
     <h1 class="hidden-sm hidden-xs">My Account</h1>
     <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">My Account</h1>
   </div>
   <div class="col-md-5">
     <ul class="breadcrumb">

      <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
      </li>
      <li>Account</li>
    </ul>
  </div>
</div>
</div>
</div>

<div id="content" class="clearfix">

	<div class="container">

		<div class="row">
      <div class="col-md-9 clearfix" id="customer-account">
        <p><?= $this->session->flashdata('error'); ?></p>
        <?= form_open('account/proses_edit_nama'); ?>
        <div class="box" style="padding: 10px">
          <div class="heading">
            <h3 class="text-uppercase">My Account</h3>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label>Name</label>
                <input class="form-control" name="fullname" value="<?= $users->fullname ?>">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label>Email</label>
                <input class="form-control" value="<?= $users->email ?>" disabled>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 text-center">
              <button type="submit" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-save"></i> Save your Name</button>
            </div>
          </div>
        </div>
        <?= form_close(); ?>
        <?php if ($users->password == null): ?>
          <div class="box" style="padding: 10px">
            <div class="heading">
              <h3 class="text-uppercase">Add To Password</h3>
            </div>
            <form method="post" role="form" id="form-addpassword" action="<?php echo site_url('account/add_password'); ?>">
              <input type="text" id="csrfHash" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="password_1">Password</label>
                    <input type="password" class="form-control" id="password3" placeholder="" name="password" required=" "><span class="help-block" id="error"></span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="password-login">Confirm Password</label>
                    <input type="password" class="form-control"  placeholder="" required=" " name="cpassword"><span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
              <hr>
              <div class="text-center">
                <button type="submit" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-save"></i> Save your Password</button>
              </div>
            </form>
          </div>
        <?php else: ?>
          <p class="lead">Change your Password</p>
          <p>If you have any questions, please <a href="<?php echo site_url('contact') ?>">contact us</a>, our customer service center is always ready to serve.<br>Happy Shopping!</p>

          <div class="box" style="padding: 10px">
            <div class="heading">
              <h3 class="text-uppercase">Change Password</h3>
            </div>
            <form method="post" role="form" id="form-changepassword" action="<?php echo site_url('account/proses_ganti_password'); ?>">
              <input type="hidden" id="csrfHash" name="<?=$this->security->get_csrf_token_name();?>" value="<?=$this->security->get_csrf_hash();?>" style="display: none">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="password_old">Old Password</label>
                    <input type="password" class="form-control" id="old_password" placeholder="" name="old_password" required=" "><span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="password_1">New Password</label>
                    <input type="password" class="form-control" id="password3" placeholder="" name="password" required=" "><span class="help-block" id="error"></span>
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <label for="password-login">Confirm New Password</label>
                    <input type="password" class="form-control"  placeholder="" required=" " name="cpassword"><span class="help-block" id="error"></span>
                  </div>
                </div>
              </div>
              <!-- /.row -->
              <hr>
              <div class="text-center">
                <button onclick="refreshTokens()"  class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-save"></i> Save your new Password</button>
              </div>
            </form>

          </div>
        <?php endif ?>
        <!-- /.box -->
      </div>
      <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
                          _________________________________________________________ -->
                          <div class="panel panel-default sidebar-menu">

                           <div class="panel-heading">
                            <h3 class="panel-title">User Profil</h3>
                          </div>

                          <div class="panel-body">

                            <ul class="nav nav-pills nav-stacked">
                             <li class="account">
                              <a href="<?= site_url('account') ?>"><i class="fa fa-user"></i> My Account</a>
                            </li>
                            <li class="history">
                              <a href="<?= site_url('history') ?>"><i class="fa fa-list"></i> Shopping History</a>
                            </li>
                        			<!-- <li class="">
                        				<a href="customer-wishlist.html"><i class="fa fa-heart"></i> My wishlist</a>
                        			</li> -->
                        			<li class="alamat">
                        				<a href="<?= site_url('address') ?>"><i class="fa fa-map-marker"></i> My Address</a>
                        			</li>
                        			<li>
                        				<a href="<?= site_url('welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
                        			</li>
                        		</ul>
                        	</div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                      </div>

                      <!-- *** RIGHT COLUMN END *** -->

                    </div>
                    <!-- /.row -->

                  </div>
                  <!-- /.container -->
                </div>
                <!-- /#content -->
                <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
                <script src="<?php echo base_url(); ?>assets/front_end/js/jquery.validate.min.js"></script>
                <script src="<?php echo base_url() ?>assets/front_end/js/owl.carousel.min.js"></script>
                <script type="text/javascript">

                  function refreshTokens() {
                    var url = "<?= base_url()."welcome/get_tokens" ?>";
                    $.get(url, function(theResponse) {
                      /* you should do some validation of theResponse here too */
                      $('#csrfHash').val(theResponse);;
                    });
                  }

                  $("#form-changepassword").validate({
                    rules:
                    {
                      old_password: {
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        remote: {
                          url: "<?php echo base_url()."account/cek_password"; ?>",
                          type: "post",
                          data: {
                            old_password : function() {
                              return $( "#old_password" ).val();
                            },
                            <?= $this->security->get_csrf_token_name(); ?> : function () {
                              refreshTokens();
                              return $( "#csrfHash" ).val();
                            }
                          }
                        }
                      },
                      password: {
                        required: true,
                        minlength: 8,
                        maxlength: 15,
                        remote : function () {
                          refreshTokens();
                        }
                      },
                      cpassword: {
                        required: true,
                        equalTo: '#password3',
                        remote : function () {
                          refreshTokens();
                        }
                      },
                    },
                    messages:
                    {
                      old_password : {
                        required : "Password is required",
                        minlength: "Password at least have 8 characters",
                        remote : "Old Password not database"
                      },
                      password:{
                        required: "Password is required",
                        minlength: "Password at least have 8 characters"
                      }
                      ,
                      cpassword:{
                        required: "Retype your password",
                        equalTo: "Password did not match !"
                      }
                    },
                    errorPlacement : function(error, element) {
                      $(element).closest('.form-group').find('.help-block').html(error.html());
                    },
                    highlight : function(element) {
                      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                      $(element).closest('.form-group').removeClass('has-error');
                      $(element).closest('.form-group').find('.help-block').html('');
                    },


                  });
                </script>

                <script type="text/javascript">
                  $("#form-addpassword").validate({
                    rules:
                    {
                      password: {
                        required: true,
                        minlength: 8,
                        maxlength: 20
                      },
                      cpassword: {
                        required: true,
                        equalTo: '#password3'
                      },
                    },
                    messages:
                    {
                      password:{
                        required: "Password is required",
                        minlength: "Password at least have 8 characters"
                      }
                      ,
                      cpassword:{
                        required: "Retype your password",
                        equalTo: "Password did not match !"
                      }
                    },
                    errorPlacement : function(error, element) {
                      $(element).closest('.form-group').find('.help-block').html(error.html());
                    },
                    highlight : function(element) {
                      $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                    },
                    unhighlight: function(element, errorClass, validClass) {
                      $(element).closest('.form-group').removeClass('has-error');
                      $(element).closest('.form-group').find('.help-block').html('');
                    },


                  });
                </script>
                <script>
                  window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
                </script>
                <script type="text/javascript">
                  $(document).ready(function() {
                    $('.account').addClass('active');
                  });
                </script>