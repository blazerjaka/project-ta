        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="hidden-sm hidden-xs">About Us</h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">About Us</h1>
                    </div>
                    <div class="col-md-6">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>About Us</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <section>
            <div class="heading text-center">
                <h2>Label-a</h2>
            </div>
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                        <p align="center">
                            <b></b> 
                        </p>
                        <iframe src="https://maps.google.com/maps?q=Gg.%20Kurnia%20sari&t=&z=19&ie=UTF8&iwloc=&output=embed" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

                        </div>
                    </div>

                </div>
                <!-- /.container -->
            </div>
        </section>

        <section class="bar background-white no-mb">
            <div class="heading text-center">
                <h2>FAQ</h2>
            </div>
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-9" data-animate="fadeInLeft">
                            <div class="panel-group accordion" id="accordionThree">
                                <div class="panel panel-default1">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3a">
                                                About Product
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3a" class="panel-collapse collapse in">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <h4>Product of Label-a</h4>
                                                </div>
                                                <div class="col-md-2">
                                                </div>
                                                <div class="col-md-10"  align="justify">
                                                    <p>
                                                        The products available on the web <b>Label-a</b> are original products from <b>Label-a</b>, produced and packed directly by the <b>Label-a</b> team in West Kalimantan Indonesia.<br><br> We strive to maintain a high commitment to quality, quantity, and credibility product so that we can be a dependable partner, reliable, professional, and give benevit to all our customers with competitive prices, especially in supporting the needs in field of kratom.
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default1">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3b">
                                                About Order
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3b" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12" align="justify">
                                                    <div class="col-md-6">
                                                        <p>
                                                            <h4>Status Order:</h4>
                                                            For the status of your order when the transaction in <b>Label-a</b> has 5 main stages of Proceed, Paid, Shipped, Confimed, and Cancelled.
                                                            <ul><br>
                                                                <li><img src="<?php echo base_url() ?>assets/front_end/images/support/proceed.png" alt="Proceed">
                                                                    <br>When your order status on Proceed, that means your order is being waiting for payment to be sent to the <b>Label-a</b>. If you have sent the payment to <b>Label-a</b>, you are expected to immediately send a proof of payment in your order view tab</li>
                                                                    <br>
                                                                <li><img src="<?php echo base_url() ?>assets/front_end/images/support/paid.png" alt="Paid">
                                                                    <br>When your order status on Paid, that means the payment of your order has been confirmed by <b>Label-a</b> and your order will be packed immediately and sent to your address.</li><br>
                                                                <li><img src="<?php echo base_url() ?>assets/front_end/images/support/shipped.png" alt="Shipped">
                                                                    <br>When your order status on Shipped, that means your order has been confirmed to be sent to your address.</li>
                                                                    <br>
                                                                <li><img src="<?php echo base_url() ?>assets/front_end/images/support/confirm.png" alt="Confirm">
                                                                    <br>When your order status on Confirm, that means <b>Label-a</b> has confirmed that your order has arrived at delivery destination.</li>
                                                                    <br>
                                                                <li><img src="<?php echo base_url() ?>assets/front_end/images/support/canceled.png" alt="Canceled">
                                                                    <br>When your order status on Canceled, that means your order has been canceled either from <b>Label-a</b> or from yourself.</li>
                                                            </ul>
                                                        </p>
                                                    </div>
                                                    <hr class="hidden-md hidden-lg">
                                                    <div class="col-md-6">
                                                        <p>
                                                            <!-- <h4>Payment Method:</h4>
                                                            For transaction method of payment at <b>Label-a</b>, we provide 4 choice of payment method, that are Paypal, MoneyGram, Wastern Union, and Bank Wire. You are required to choose one of the options we have provided.<br>For information about payment account number, you can see on the <b>Via Payment</b> tab on your history page.<br>
                                                            <img src="<?php echo base_url() ?>assets/front_end/images/support/bank.png" alt="Payment" class="payimg"></li> -->

                                                            </ul>
                                                        </p>
                                                        <hr>
                                                        <p>
                                                            <!-- <h4>Shipping Method:</h4>
                                                            For transaction method of shipping at <b>Label-a</b>, we provide 4 choice of shipping method, that are Pos Indonesia, DHL, FedEx, and UPS. You are required to choose one of the options we have provided.
                                                            <br>
                                                            <img src="<?php echo base_url() ?>assets/front_end/images/support/ship.jpg" alt="Shipping" class="payimg"></li> -->
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default1">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse3c">
                                                About Account
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse3c" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="col-md-6">
                                                        <h4>Login</h4>
                                                        <p align="justify">
                                                            Register Account or Login is the first stage for users to be able to conduct transactions on the site <b>Label-a</b>. You are required to register or login your account first. For registration or account login to web <b>Label-a</b>, we only accept registration or web login <b>Via Google+</b>. After login you can use our service for ordering goods (transaction).
                                                        </p>
                                                        <hr>
                                                        <h4>Password</h4>
                                                        <p align="justify">
                                                            You can change the password in <b>Label-a</b> account. Make sure that your password is unknown to anyone except yourself to avoid any abuse of the account. You can change your passwords regularly to keep them safe.
                                                        </p>
                                                        <hr class="hidden-md hidden-lg">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <h4>Address</h4>
                                                        <p align="justify" class="urut">
                                                            You can add, change or delete the shipping address in your <b>Label-a</b> account. If you want to make the shipping at another address,  you can change or add the address whereas if the previous shipping address has been out of use, you can delete it.

                                                            <h5>Adding a New Shipping Address</h5>
                                                            <ul class="urut">
                                                                <li>Login your account</li>
                                                                <li>Go to the address page</li>
                                                                <li>Select the Create address tab</li>
                                                                <li>Input the address data</li>
                                                                <li>Save</li>
                                                            </ul>
                                                            <h5>Delete a Shipping Address</h5>
                                                            <ul class="urut">
                                                                <li>Login your account</li>
                                                                <li>Go to the address page</li>
                                                                <li>Click Delete tab and your address has been delete.</li>
                                                            </ul>
                                                            <h5>Edit Shipping address</h5>
                                                            <ul class="urut">
                                                                <li>Login your account</li>
                                                                <li>Go to the address page</li>
                                                                <li>Select the Edit address tab</li>
                                                                <li>Change the address data</li>
                                                                <li>Save</li>
                                                            </ul>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-default1">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordionThree" href="#collapse4c">
                                                <!-- About Fiture Request -->
                                            </a>
                                        </h4>
                                    </div>
                                    <div id="collapse4c" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p>Empty</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="hidden-md hidden-lg">
                        <div class="col-md-3" data-animate="fadeInRight" align="center">
                            <!-- <a href="<?php echo site_url('welcome/shopping_guide') ?>" title="Shopping Guide"> -->
                            <h3 align="left">Shopping Guide</h3>
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/panduan.png" alt="Shopping Guide">
                            </a>
                            <!-- <div class="video">
                                <div class="embed-responsive embed-responsive-4by3">
                                    <iframe class="embed-responsive-item" src="//www.youtube.com/embed/upZJpGrppJA"></iframe>
                                </div>

                            </div> -->
                        </div>
                    </div>

                </div>
                <!-- /.container -->
            </div>
        </section>
        <hr>
        <section class="bar background-white no-mb" data-animate="fadeInUp">
            <div class="heading text-center">
                <h2>Privacy Policy</h2>
            </div>
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10 text-justify">
                            <p>Your privacy is important to us, and it is Label-a.com's policy to respect your privacy regarding any information we may collect when operating our website. We've developed it so you can understand how we collect, use, communicate, disclose, and utilize your personal information.</p>
                            <ul>
                                <li>Before or at the time of collecting personal information, we will identify the purposes for which information is being collected.</li>
                                <li>We will collect and use personal information solely with the objective of fulfilling those purposes specified by us and for other compatible purposes, unless we obtain the consent of the individual concerned or as required by law.</li>
                                <li>We will only retain personal information as long as necessary for the fulfillment of those purposes.</li>
                                <li>We will protect personal information by reasonable security safeguards against loss or theft, as well as unauthorized access, disclosure, copying, use or modification.</li>
                                <li>We will collect personal information by lawful and fair means and, where appropriate, with the knowledge or consent of the individual concerned.</li>
                            </ul>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                            <div class="heading text-center">
                                <h2>Terms & Conditions</h2>
                            </div>
                            <p>
                            <h5>COPYRIGHT</h5>
                            All content on Label-a.com such as graphics, text and logos  is the property of <a href="<?php echo site_url() ?>">Label-a.com</a><br><br><hr><br>
                            <h5>RETURN POLICY</h5>
                            Feel free to email us at: <a href="mailto:label.a.kosmetik@gmail.com"> label.a.kosmetik@gmail.com</a> or call us at <a href="https://api.whatsapp.com/send?phone=6282191352200&amp;">+62 821-9135-2200</a> <br> Just on Weekdays (Monday-Sunday) 24 Hours. Thanks for your attention</p>
                            <!-- <p class="text-muted des text-center">SediaIn beroperasi dari Senin sampai Sabtu mulai pukul 08.00 sampai dengan 15.00 WIB <br>Selamat Berbelanja!</p>
                            <div class="col-md-3">
                                <div class="wkt">
                                    <img title=" " alt="Profil" src="<?php echo base_url() ?>assets/front_end/images/biodata/1.jpg">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="ttg">
                                    <h2>Customers Service</h2>
                                    <p >Customers Service SediaIn selalu online 24 jam.</p>
                                </div>
                            </div>
                            <div class="col-md-12">
                            &nbsp;<hr>
                            </div>

                            <div class="col-md-3">
                                <div class="wkt">
                                    <img title=" " alt="Visi" src="<?php echo base_url() ?>assets/front_end/images/biodata/2.jpg" width="60%" style="border-radius: 50%">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="ttg">
                                    <h2>Waktu Pemesanan</h2>
                                    <p >SediaIn menggunakan sistem Preorder yaitu pesan hari ini besok sampai kerumah anda. Jangka waktu pemesanan mulai setiap hari Senin sampai Sabtu pukul 08.00 sampai dengan 20.00 WIB. Jika pemesanan melewati batas waktu yang telah ditentukan atau pada hari libur nasional maka pesanan akan diproses pada jam kerja selanjutnya.</p>
                                </div>
                            </div>
                            <div class="col-md-12">
                            &nbsp;<hr>
                            </div>

                            <div class="col-md-3">
                                <div class="wkt">
                                    <img title=" " alt="Misi" src="<?php echo base_url() ?>assets/front_end/images/biodata/3.jpg" width="60%" style="border-radius: 50%">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="ttg">
                                    <h2>Waktu Pengiriman</h2>
                                    <p >Waktu pengiriman barang hanya setiap hari Senin sampai Sabtu mulai pukul 08.00 sampai dengan 15.00 WIB,  kecuali pada hari libur nasional maka pengiriman akan dialihkan pada jam kerja selanjutnya.</p>
                                </div>
                            </div> -->
                        </div>
                    </div>

                </div>
                <!-- /.container -->
            </div>
        </section>



<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.tentang').addClass('active');
    });
</script>