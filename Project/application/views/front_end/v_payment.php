<div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="hidden-sm hidden-xs">Checkout - Payment method</h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Checkout - View Order</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">

                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>Checkout - Payment method</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">

                <div class="row">

                    <div class="col-md-9 clearfix" id="checkout">

                        <div class="box">
                            <?= form_open('checkout/proses_checkout3'); ?>
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="<?= site_url('checkout') ?>"><i class="fa fa-map-marker"></i><br>Address</a>
                                    </li>
                                    <li><a href="<?= site_url('checkout/delivery') ?>"><i class="fa fa-truck"></i><br>Shipping Method</a>
                                    </li>
                                    <li class="active"><a href="#"><i class="fa fa-money"></i><br>Payment method</a>
                                    </li>
                                    <li class="disabled"><a href="shop-checkout4.html"><i class="fa fa-eye"></i><br>View Order</a>
                                    </li>
                                </ul>
                                

                                <div class="content" style="padding: 10px">
                                    
                                <div class="row">
                                        <!-- <div class="col-sm-3">
                                            <div class="box payment-method">
                                                <h6>Cash on Delivery / COD</h6>
                                                <p>Menyediakan Metode Pembayaran COD agar Transaksi Anda Lebih Aman.</p>
                                                <div class="box-footer text-center">
                                                    <input type="radio" name="payment" value="COD">
                                                </div>
                                            </div>
                                        </div> -->
                                        
                                        <div class="col-sm-3">
                                        <center><h4>BRI</h4></center>
                                            <div class="box payment-method">
                                            <img src="<?php echo base_url() ?>assets/front_end/images/logo/bri1.jpg" alt="bri" style="width: 100%;height: 60px;">
                                                <div class="box-footer text-center">
                                                    <input type="radio" name="payment" value="BRI">
                                                </div>
                                                <br>
                                                <span style="font-size:18px"><center>127801005092509
                                                <br>A.N - Dedi</center>
                                                </span>
                                            </div>
                                        </div> 
                                        <div class="col-sm-3">
                                        <center><h4>BCA</h4></center>
                                            <div class="box payment-method">
                                            <img src="<?php echo base_url() ?>assets/front_end/images/logo/bca1.jpg" alt="bca" style="width: 100%;height: 60px;">
                                                <div class="box-footer text-center">
                                                    <input type="radio" name="payment" value="BCA">
                                                </div>
                                                <br>
                                                <span style="font-size:18px"><center>7310252527
                                                <br>A.N - Dedi</center>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                        <center><h4>MANDIRI</h4></center>
                                            <div class="box payment-method">
                                            <img src="<?php echo base_url() ?>assets/front_end/images/logo/mandiri.jpg" alt="mandiri" style="width: 100%;height: 60px;">
                                                <div class="box-footer text-center">
                                                    <input type="radio" name="payment" value="MANDIRI">
                                                </div>
                                                <br>
                                                <span style="font-size:18px"><center>0700000899992
                                                <br>A.N - Dedi</center>
                                                </span>
                                            </div>
                                        </div>
                                        <div class="col-sm-3">
                                        <center><h4>BNI</h4></center>
                                            <div class="box payment-method">
                                            <img src="<?php echo base_url() ?>assets/front_end/images/logo/bni.jpg" alt="bni" style="width: 100%;height: 60px;">
                                                <div class="box-footer text-center">
                                                    <input type="radio" name="payment" value="BNI">
                                                </div>
                                                <br>
                                                <span style="font-size:18px"><center>0238272088
                                                <br>A.N - Dedi</center>
                                                </span>
                                            </div>
                                        </div> 
                                    </div>
                                    <!-- /.row -->
                                </div>
                                <!-- /.content -->

                                <div class="box-footer">
                                    <div class="col-sm-4">
                                        <div align="center">
                                            <a href="<?= site_url('checkout/delivery') ?>" class="btn btn-default" style="width:220px"><i class="fa fa-chevron-left"></i>Back to Shipping</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-4">
                                        <div align="center">
                                            <button type="submit" class="btn btn-template-main" style="width:220px">Proceed Order<i class="fa fa-chevron-right"></i>
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            <?= form_close(); ?>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

                    <div class="col-md-3">
                        <div class="box" id="order-summary">
                            <div class="box-header">
                                <h3>Cart Totals</h3>
                            </div>
                            <p class="text-muted" style="text-align: justify; padding: 10px;">Shipping and additional fees are calculated based on the value you have entered.</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr class="total">
                                            <td>Total</td>
                                            <th>Rp <?php echo rupiah($this->cart->total()); ?>,00</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.cart').addClass('active');
    });
</script>