<script type="text/javascript">
    function isNumberKey (evt) {
        var charCode = (evt.which) ? evt.which :
        event.keyCode
        if (charCode > 31 && (charCode <48 || charCode > 57))

            return false;
        return true;
    };
</script>
<link rel="stylesheet" href="<?= base_url('') ?>assets/select2/dist/css/select2.min.css">
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="hidden-sm hidden-xs">My Address</h1>
                <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">My Address</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                    </li>
                    <li><a href="<?php echo site_url('address') ?>"> Address</a>
                    </li>
                    <li>Edit Address</li>
                </ul>
            </div>
        </div>
    </div>
</div>

<div id="content" class="clearfix">

	<div class="container">

		<div class="row">

                    <!-- *** LEFT COLUMN ***
                        _________________________________________________________ -->

                        <div class="col-md-9 clearfix" id="customer-account">
                            <p class="lead">Shipping Destination Address</p>
                            <p>If you have any questions, please <a href="<?php echo site_url('contact') ?>">contact us</a>, our customer service center is always ready to serve.<br>Happy Shopping!</p>
                            <div class="box clearfix" style="padding: 10px">
                               <div class="heading">
                                  <h3 class="text-uppercase">Edit Address</h3>
                              </div>

                              <p>Enter your destination address correctly, to help our courier send your order to the destination correctly.</p>
                              <hr>
                              <p><?= $this->session->flashdata('error'); ?></p>
                              <hr>
                              <?= form_open('address/proses_edit'); ?>
                              <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="firstname">Recipient's Name</label>
                                        <input type=hidden name="id" value="<?= $alamat->id ?>">
                                        <input type="text" class="form-control" name="nama_penerima" value="<?= $alamat->nama_penerima ?>">
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="form-group">
                                       <label for="firstname">Address Type</label>
                                       <select class="form-control"  name="tipe_alamat" class="select">
                                        <?php
                                        if ($alamat->tipe_alamat == '1') {
                                                    # code...
                                            ?>
                                            <option value="1">Main Address</option>
                                            <option value="2">Other</option>
                                            <?php
                                        }else{
                                            ?>
                                            <option value="2">Other</option>
                                            <option value="1">Main Address</option>
                                            <?php
                                        }
                                        ?>
                                                <!-- <option value="">Choose option</option>
                                                <option value="1">Main Address</option>
                                                <option value="2">Other</option> -->

                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-3">
                                        <div class="form-group">
                                            <label for="firstname">Name Type</label>
                                            <input type="text" class="form-control" name="nama_tipe" value="<?= $alamat->nama_tipe ?>">
                                            <span style="font-size: 10px">Example: My Home</span>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.row -->

                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="lastname">Provinsi</label>
                                            <select class="form-control" id="country"  name="negara" class="select">
                                                <option value="">Choose option</option>
                                                <?php foreach ($negara->result() as $key ) {
                                                    if ( $key->country_name == $alamat->negara) {
                                                        echo "<option value='$key->country_name' selected>$key->country_name</option>";
                                                    }else{
                                                        echo "<option value='$key->country_name'>$key->country_name</option>";
                                                    }
                                                } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                           <label for="company">State</label>
                                           <input type="text" class="form-control" name="provinsi" placeholder="State" maxlength="30" value="<?= $alamat->provinsi ?>">
                                       </div>
                                   </div>
                               </div>
                               <!-- /.row -->

                               <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label for="street">City</label>
                                        <input type="text" class="form-control" name="kota" placeholder="City" maxlength="30" value="<?= $alamat->kota ?>">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="form-group">
                                       <label for="street">Postal Code</label>
                                       <input type="text" class="form-control" name="kode_pos" placeholder="Postal Code"  onkeypress="return isNumberKey(event)" maxlength="5" value="<?= $alamat->kode_pos ?>">
                                   </div>
                               </div>
                           </div>
                           <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="zip">Phone</label>
                                    <input type="text" class="form-control" name="no_hp" onkeypress="return isNumberKey(event)" maxlength="13" value="<?= $alamat->no_hp ?>">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label for="alamat">Address</label>
                                    <textarea class="form-control" name="alamat"><?= $alamat->alamat ?></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                         <div class="col-sm-3 text-center">
                            <button type="reset" class="btn btn-lg btn-warning" style="width: 100%"><i class="fa fa-refresh"></i> Reset</button>
                        </div>
                        <div class="col-sm-9 text-center">
                            <button type="submit" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-save"></i> Save your Address</button>

                        </div>
                    </div>
                    <?= form_close(); ?>
                </div>
            </div>
            <!-- /.col-md-9 -->

            <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
                        _________________________________________________________ -->

                        <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
                            _________________________________________________________ -->
                            <div class="panel panel-default sidebar-menu">

                               <div class="panel-heading">
                                <h3 class="panel-title">User Profil</h3>
                            </div>

                            <div class="panel-body">

                                <ul class="nav nav-pills nav-stacked">
                                    <li class="account">
                                        <a href="<?= site_url('account') ?>"><i class="fa fa-user"></i> My Account</a>
                                    </li>
                                    <li class="history">
                                        <a href="<?= site_url('history') ?>"><i class="fa fa-list"></i> Shopping History</a>
                                    </li>
                                    <li class="alamat">
                                        <a href="<?= site_url('address') ?>"><i class="fa fa-map-marker"></i> My Address</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <!-- Select2 -->
        <script src="<?= base_url('') ?>assets/select2/dist/js/select2.full.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('.alamat').addClass('active');
        });
          $('#country').select2();
      </script>