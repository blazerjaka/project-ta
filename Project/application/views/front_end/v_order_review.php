        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="hidden-sm hidden-xs">Checkout - View Order</h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Checkout - View Order</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">

                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>Checkout - View Order</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">

                <div class="row">

                    <div class="col-md-9 clearfix" id="checkout">

                        <div class="box">
                            <?= form_open('checkout/proses_checkout4'); ?>
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="<?= site_url('checkout') ?>"><i class="fa fa-map-marker"></i><br>Address</a>
                                    </li>
                                    <li><a href="<?= site_url('checkout/delivery') ?>"><i class="fa fa-truck"></i><br>Shipping Method</a>
                                    </li>
                                    
                                    <li class="active"><a href="#"><i class="fa fa-eye"></i><br>View Order</a>
                                    </li>
                                </ul>

                                <div class="content" style="padding: 10px">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <th colspan="2">Product</th>
                                                    <th>Quatity</th>
                                                    <th>Price</th>
                                                    <th>Shipping</th>
                                                    <th>Payment</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php
                                                $i=0;
                                                foreach ($this->cart->contents() as $items) :
                                                $i++;
                                                ?>

                                                    <tr>
                                                        <td>
                                                            <a href="#">
                                                                <img src="<?php echo base_url(); ?>assets/front_end/images/produk/<?= $items['image'] ?>">
                                                            </a>
                                                        </td>
                                                        <td><a href=""><?= $items['name'] ?></a></td>
                                                        <td><?= $items['qty'] ?></td>
                                                        <td>Rp <?php echo rupiah($items['price']); ?>,00</td>
                                                        <td></td>
                                                        <td></td>
                                                        <td>Rp <?php echo rupiah($items['subtotal']); ?>,00</td>
                                                    </tr>

                                                    <?php endforeach; ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="6">Total</th>
                                                    <th>Rp <?php echo rupiah($this->cart->total()); ?>,00</th>
                                                </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.content -->

                                <div class="box-footer"">
                                    <div class="col-sm-4">
                                        <div align="center">
                                            <a href="<?= site_url('checkout/payment') ?>" class="btn btn-default" style="width:100%"><i class="fa fa-chevron-left"></i>Back to Payment</a>
                                        </div>
                                    </div>
                                    <div class="col-sm-4">
                                    </div>
                                    <div class="col-sm-4">
                                        <div align="center">
                                            <button type="submit" class="btn btn-template-main" style="width:100%">Finalization<i class="fa fa-chevron-right"></i>
                                        </button>
                                        </div>
                                    </div>
                                </div>
                            <?= form_close(); ?>
                        </div>
                        <!-- /.box -->


                    </div>
                    <!-- /.col-md-9 -->

                    <div class="col-md-3">

                        <div class="box" id="order-summary">
                            <div class="box-header">
                                <h3>Cart Totals</h3>
                            </div>
                            <p class="text-muted" style="text-align: justify; padding: 10px;">Shipping and additional fees are calculated based on the value you have entered.</p>

                            <div class="table-responsive">
                                <table class="table">
                                    <tbody>
                                        <tr class="total">
                                            <td>Total</td>
                                            <th>Rp <?php echo rupiah($this->cart->total()); ?>,00</th>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>

                    </div>
                    <!-- /.col-md-3 -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.cart').addClass('active');
    });
</script>