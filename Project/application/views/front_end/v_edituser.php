<script type="text/javascript">
    function isNumberKey (evt) {
        var charCode = (evt.which) ? evt.which :
        event.keyCode
        if (charCode > 31 && (charCode <48 || charCode > 57))

        return false;
        return true;
    };
</script>

<div class="newproducts-w3agile">
    <div class="container">
			<h3>Edit Profil</h3>
			<div id="errorDiv"></div>
			<div class="login-form-grids">
				<form method="post" role="form" id="register-form" action="<?php echo site_url('buat_akun/proses'); ?>">
				<div class="form-group">
					<label>Nama Lengkap</label>
					<input type="text" placeholder="Nama" name="name"  required=" " >
					<span class="help-block" id="error"></span>
					</div>
				<div class="form-group">
					<label>Email</label>
					<input type="email" name="email" id="email" placeholder="Email Address" required=" " >
					<span class="help-block" id="error"></span>
					</div>
				<div class="form-group">
					<label>No.HP</label>
					<input type="text" placeholder="" name="no_hp" onkeypress="return isNumberKey(event)" maxlength="15" value="+62" >
					<span class="help-block" id="error"></span>
					</div>
				<div class="form-group">
					<label>Alamat</label><br>
					<textarea style="width:100%;"></textarea>
					<span class="help-block" id="error"></span></div>
				<div class="form-group">
					<label>Kota</label><br>
					<select>
						<option>Kota Pontianak</option>
						<option>Kota Singkawang</option>
					</select>
					<span class="help-block" id="error"></span>
					</div>
				<div class="form-group">
					<label>Provinsi</label><br>
					<select>
						<option>Kalimantan Barat</option>
						<option></option>
					</select>
					<span class="help-block" id="error"></span>
				</div>
				
				
				<h6>Informasi Login</h6>
				<div class="form-group">
                   	<div class="form-group">
					<input type="password" id="password" placeholder="Password" name="password" required=" " ><span class="help-block" id="error"></span>
					</div>
					<div class="form-group">
					<input type="password"  placeholder="Password Confirmation" required=" " name="cpassword" ><span class="help-block" id="error"></span>
					</div>
					<div class="register-check-box">
						<div class="check">
							<label class="checkbox"><input type="checkbox" name="checkbox"><i> </i>I accept the terms and conditions</label>
						</div>
					</div>
					<input type="submit" value="Simpan">
				</form>
			</div>
	</div>
	<br><br><br>
</div>