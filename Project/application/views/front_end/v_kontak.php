        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <h1 class="hidden-sm hidden-xs">Contact Us</h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Contact Us</h1>
                    </div>
                    <div class="col-md-5">
                        <ul class="breadcrumb">

                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>Contact</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>
        <section>
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="box clearfix" style="padding: 10px">
                                <div class="heading">
                                    <h3>Contact Form</h3>
                                </div>

                                <p class="text-muted" align="justify">If you have any questions, please enter your message, our customer service center is always ready to serve.</p><hr>
                                <form action="<?php echo site_url('kontak/sendmail') ?>" method="post" role="form">
                                    <div class="row">
                                        <?php if ($this->session->userdata('user_logged_in') == 'Sudah_Loggin') { ?>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="firstname">Your Name</label>
                                                <input type="text" name="nama_user" value="<?php echo $this->session->userdata('nama_user');  ?>" class="form-control" id="firstname">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" value="<?php echo $this->session->userdata('email_user');  ?>" class="form-control" id="email">
                                            </div>
                                        </div>
                                        <?php } else {?>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="firstname">Your Name</label>
                                                <input type="text" name="nama_user" class="form-control" id="firstname">
                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <div class="form-group">
                                                <label for="email">Email</label>
                                                <input type="text" name="email" class="form-control" id="email">
                                            </div>
                                        </div>
                                        <?php
                                          }
                                        ?>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="subject">Subject</label>
                                                <input type="text" name="judul" class="form-control" id="subject">
                                            </div>
                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group">
                                                <label for="message">Message</label>
                                                <textarea id="message" name="pesan" class="form-control" style="height: 150px"></textarea>
                                            </div>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                        <hr>
                                            <button type="submit" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-envelope-o"></i> Send Message</button>

                                        </div>
                                    </div>
                                    <!-- /.row -->
                                </form>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="panel panel-default sidebar-menu">

                            <div class="heading">
                                <h3>Call Center</h3>
                            </div>
                            <p class="text-muted" align="justify">For more information about Label-A Products please contact our Call Center. We are always ready to answer your call and your messages.</p><hr>
                            <p><a href="https://api.whatsapp.com/send?phone=6282191352200&amp;"><i class="fa fa-phone-square"></i> +62 821-9135-2200</a></p>
                            <p><a href="mailto:label.a.kosmetik@gmail.com"><i class="fa fa-envelope"></i> label.a.kosmetik@gmail.com</a></p>
                            <!-- <p><a href="https://www.instagram.com/zack_kratom/" target="_blank"><i class="fa fa-instagram"></i> Master Botanicals - Zack</a></p>
                            <p><a href="https://www.facebook.com/profile.php?id=100013385370262" target="_blank"><i class="fa fa-facebook-square"></i> Master Botanicals - Zack</a></p> -->

                            <!-- <p><i class="glyphicon glyphicon-earphone"></i> <a href="tel:+6282253657711">+6282253657711</a></p> -->
                            <!-- <br><hr>
                            <div class="heading">
                                <h3>Email</h3>
                            </div>
                            <p class="text-muted" align="justify">For more information about Master Botaicals Products please contact our Contact Email. We are always ready to answer your message.</p>
                            <p><a href="mailto:masterbotanicals@gmail.com"><i class="glyphicon glyphicon-envelope"></i> masterbotanicals@gmail.com</a></p> -->
                            <!-- <p><i class="glyphicon glyphicon-envelope"></i> <a href="mailto:support@masterbotanicals.com">support@masterbotanicals.com</a></p> -->


                        </div>
                        </div>

                    </div>
                    </div>
                    </div>


                </section>

<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.kontak').addClass('active');
    });
</script>