<!-- DataTables -->
<link rel="stylesheet" href="<?= base_url("assets/"); ?>datatables/css/dataTables.bootstrap.css">
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="hidden-sm hidden-xs">Shopping History</h1>
                <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Shopping History</h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb">

                    <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                    </li>
                    <li>History</li>
                </ul>

            </div>
        </div>
    </div>
</div>

<div id="content">
    <div class="container">


        <div class="row">
           <div class="col-md-9" id="customer-orders">
                <div class="tabs">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab4-1" data-toggle="tab"><i class="icon-star"></i><b>Shopping History</b></a>
                        </li>
                        <!-- <li class=""><a href="#tab4-2" data-toggle="tab"><b>Via Payment</b></a> -->
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab4-1">
                            <p align="justify"><b>Hi, for information about delivery account purposes please open on the Via Payment menu. And we confirm that after you make payment please send your proof of payment directly with click view to your order, so we process your order immediately. thanks.</b> And if you have any questions, please <a href="<?php echo site_url('contact') ?>">Contact Us!!</a><br>Happy Shopping!</p>
                            <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <button class="btn btn-default" onclick="reload_table()"><i class="glyphicon glyphicon-refresh"></i> Reload</button>
                                    </div>
                                </div>
                            </div>
                            <div class="box">
                                <div class="table-responsive">
                                    <table id="mytable" class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th width="1px">No</th>
                                                <th>Order</th>
                                                <th>Date</th>
                                                <th>Total</th>
                                                <th>Status</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                        </div>
                        <div class="tab-pane" id="tab4-2">
                            <h3>For Payment Via PayPal</h3>
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/paypal.jpg" alt="" width="220px" class="hidden-sm hidden-xs">
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/paypal.jpg" alt="" width="40%" class="hidden-md hidden-lg">
                            <br><hr>
                            <p align="justify">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <hr><br>
                            <h3>For Payment Via MoneyGram | Wastern | Bank Wire</h3>
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/dhl.png" alt="" width="220px" height="60px" class="hidden-sm hidden-xs">
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/dhl.png" alt="" width="35%" class="hidden-md hidden-lg">
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/fedex.png" alt="" width="220px" height="60px" class="hidden-sm hidden-xs">
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/fedex.png" alt="" width="26%" class="hidden-md hidden-lg">
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/money.jpg" alt="" width="220px" height="60px" class="hidden-sm hidden-xs">
                            <img src="<?php echo base_url() ?>assets/front_end/images/support/money.jpg" alt="" width="35%" class="hidden-md hidden-lg">
                            <hr>
                            <p align="justify">Here's the information for payment purposes by using one of these three methods :<br></p>
                            <div class="box">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" width="100%">
                                        <thead>
                                            <tr>
                                                <th>Bank Name</th><td>PT. BANK DANAMON INDONESIA, Tbk</td>
                                            </tr>
                                            <tr>
                                                <th>Account Name</th><td>MUHAMMAD ZAKARIA</td>
                                            </tr>
                                            <tr>
                                                <th>Acount #</th><td>003611955034</td>
                                            </tr>
                                            <tr>
                                                <th>SWIFT CODE</th><td>BDINIDJA</td>
                                            </tr>
                                            <tr>
                                                <th>Bank Address</th><td>BDI PONTIANAK SUNGAI RAYA 0002845</td>
                                            </tr>
                                            <tr>
                                                <th>Address</th><td>Jl. Adisucipto Km. 13,5 Gg. Kapuas No. 7A</td>
                                            </tr>
                                            <tr>
                                                <th>City</th><td>Pontianak</td>
                                            </tr>
                                            <tr>
                                                <th>Province</th><td>Kalimantan Barat</td>
                                            </tr>
                                            <tr>
                                                <th>Zipcode</th><td>78391</td>
                                            </tr>
                                            <tr>
                                                <th>Country</th><td>Indonesia</td>
                                            </tr>
                                            <tr>
                                                <th>Phone</th><td>+62 858 2179 2016</td>
                                            </tr>
                                        </thead>

                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                                <!-- /.table-responsive -->
                            </div>
                            <br>
                        </div>
                    </div>
                    <!-- /.tab-content -->
                </div>
                <!-- /.tabs -->

        </div>
        <!-- /.col-md-9 -->

        <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
                       _________________________________________________________ -->

                       <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
                         _________________________________________________________ -->
                         <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">User Profil</h3>
                            </div>

                            <div class="panel-body">

                               <ul class="nav nav-pills nav-stacked">
                                <li class="account">
                                    <a href="<?= site_url('account') ?>"><i class="fa fa-user"></i> My Account</a>
                                </li>
                                <li class="history">
                                    <a href="<?= site_url('history') ?>"><i class="fa fa-list"></i> Shopping History</a>
                                </li>
                                <li class="alamat">
                                    <a href="<?= site_url('address') ?>"><i class="fa fa-map-marker"></i> My Address</a>
                                </li>
                                <li>
                                    <a href="<?= site_url('welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <!-- /.col-md-3 -->

                    <!-- *** CUSTOMER MENU END *** -->
                </div>

                <!-- *** RIGHT COLUMN END *** -->

            </div>


        </div>
        <!-- /.container -->
    </div>
    <!-- /#content -->

    <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
    <script>
        window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
    </script>
    <!-- DataTables -->
    <script src="<?= base_url("assets/"); ?>datatables/js/jquery.dataTables.min.js"></script>
    <script src="<?= base_url("assets/"); ?>datatables/js/dataTables.bootstrap.js"></script>
    <script type="text/javascript">
       $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
       {
          return {
            "iStart": oSettings._iDisplayStart,
            "iEnd": oSettings.fnDisplayEnd(),
            "iLength": oSettings._iDisplayLength,
            "iTotal": oSettings.fnRecordsTotal(),
            "iFilteredTotal": oSettings.fnRecordsDisplay(),
            "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
            "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
        };
    };

    var table = $('#mytable').DataTable({
      oLanguage: {
        sProcessing: "loading..."
    },
    processing: true,
    serverSide: true,
    ajax: {"url": "<?= base_url() ?>history/json", "type": "POST"},
    columns: [
    {
        "data": "id",
        "orderable": false
    },
    {"data": "invoice"},
    {"data": "tanggal"},
    {"data": "total"},
    {"data": "status"},
    {"data": "view","orderable": false},
    ],
    order: [[2, 'desc']],
    rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
    }
});

    //fun reload
    function reload_table()
    {
        table.ajax.reload(null,false); //reload datatable ajax
    }
</script>
<script type="text/javascript">
    $(document).ready(function() {
        $('.history').addClass('active');
    });
</script>