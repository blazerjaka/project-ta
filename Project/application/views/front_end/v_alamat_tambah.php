<script type="text/javascript">
    function isNumberKey (evt) {
        var charCode = (evt.which) ? evt.which :
        event.keyCode
        if (charCode > 31 && (charCode <48 || charCode > 57))

            return false;
        return true;
    };
</script>

<link rel="stylesheet" href="<?= base_url('') ?>assets/select2/dist/css/select2.min.css">
<div id="heading-breadcrumbs">
    <div class="container">
     <div class="row">
      <div class="col-md-7">
            <h1 class="hidden-sm hidden-xs">My Address</h1>
            <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">My Address</h1>
   </div>
   <div class="col-md-5">
        <ul class="breadcrumb">
            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
            </li>
            <li><a href="<?php echo site_url('address') ?>"> Address</a>
            </li>
            <li>Create Address</li>
        </ul>
            </div>
        </div>
    </div>
</div>

<div id="content" class="clearfix">

	<div class="container">

		<div class="row">

            <div class="col-md-9 clearfix" id="customer-account">
                <p class="lead">Shipping Destination Address</p>
                <p>If you have any questions, please <a href="<?php echo site_url('contact') ?>">contact us</a>, our customer service center is always ready to serve.<br>Happy Shopping!</p>
                <div class="box clearfix" style="padding: 10px">
                    <div class="heading">
                      <h3 class="text-uppercase">Create Address</h3>
                  </div>
                  <p>Enter your destination address correctly, to help our courier send your order to the destination correctly.</p>
                  <hr>
                  <p><?= $this->session->flashdata('error'); ?></p>
                  <hr>
                  <?= form_open('address/proses_tambah'); ?>
                  <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="firstname">Recipient's Name</label>
                            <input type="text" class="form-control" value="<?php echo $this->session->userdata('nama_user');  ?>" name="nama_penerima" maxlength="50" value="<?= $this->session->flashdata('nama_penerima') ?>">
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="form-group">
                         <label for="firstname">Address Type</label>
                         <select class="form-control"  name="tipe_alamat" class="select">
                          <option value="">Choose option</option>
                          <?php
                          $alamat_utama = $this->db->where(array('email_users' => $this->session->userdata('email'), 'tipe_alamat' => '1'))->limit(1)->get('alamat');
                          if ($alamat_utama->num_rows() == 0) {
                            echo '<option value="1">Main Address</option>';
                        }
                        ?>
                        <option value="2">Other</option>
                    </select>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="form-group">
                    <label>Name Type</label>
                    <input type="text" class="form-control" name="nama_tipe" value="My Home" maxlength="50" value="<?= $this->session->flashdata('nama_tipe') ?>">
                    <span style="font-size: 10px">Example: My Home</span>
                </div>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label for="lastname">Country</label>
                    <select class="form-control" id="country"  name="negara" class="select">
                        <option value="">Choose option</option>
                        <?php foreach ($negara->result() as $key ) {
                            if ($this->session->flashdata('negara') == $key->country_name) {
                                echo "<option value='$key->country_name' selected>$key->country_name</option>";
                            }else{
                                echo "<option value='$key->country_name'>$key->country_name</option>";
                            }
                        } ?>
                    </select>
                </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group">
                    <label>State </label>
                    <input type="text" class="form-control" name="provinsi" placeholder="State" maxlength="30" value="<?= $this->session->flashdata('provinsi') ?>">
                </div>
            </div>
        </div>
        <!-- /.row -->

        <div class="row">
            <div class="col-sm-6">
                <div class="form-group">
                    <label>City</label>
                    <input type="text" class="form-control" name="kota" placeholder="City" maxlength="30" value="<?= $this->session->flashdata('kota') ?>">
                </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group">
                <label>Postal Code</label>
                <input type="text" class="form-control" name="kode_pos" placeholder="Postal Code"  onkeypress="return isNumberKey(event)" maxlength="5" value="<?= $this->session->flashdata('kode_pos') ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="alamat">Phone</label>
                <input type="text" class="form-control" name="no_hp" onkeypress="return isNumberKey(event)" maxlength="13" value="<?= $this->session->flashdata('no_hp') ?>">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label for="alamat">Address</label>
                <textarea name="alamat" class="form-control" style="height: 100px"></textarea>
            </div>
        </div>
    </div>
    <hr>
    <div class="row">
       <div class="col-sm-12 text-center">
        <button type="submit" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-save"></i> Save your Address</button>

    </div>

</div>
<?= form_close(); ?>

</div>

</div>
<!-- /.col-md-9 -->

<!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
                        _________________________________________________________ -->

                        <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
                            _________________________________________________________ -->
                            <div class="panel panel-default sidebar-menu">

                             <div class="panel-heading">
                              <h3 class="panel-title">User Profil</h3>
                          </div>

                          <div class="panel-body">

                              <ul class="nav nav-pills nav-stacked">
                                <li class="account">
                                    <a href="<?= site_url('account') ?>"><i class="fa fa-user"></i> My Account</a>
                                </li>
                                <li class="history">
                                    <a href="<?= site_url('history') ?>"><i class="fa fa-list"></i> Shopping History</a>
                                </li>
                                    <!-- <li class="">
                                        <a href="customer-wishlist.html"><i class="fa fa-heart"></i> My wishlist</a>
                                    </li> -->
                                    <li class="alamat">
                                        <a href="<?= site_url('address') ?>"><i class="fa fa-map-marker"></i> My Address</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->

        <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <!-- Select2 -->
        <script src="<?= base_url('') ?>assets/select2/dist/js/select2.full.min.js"></script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('.alamat').addClass('active');
        });
          $('#country').select2();
      </script>