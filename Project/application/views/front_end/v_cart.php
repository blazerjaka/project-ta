<script type="text/javascript">
    function isNumberKey (evt) {
        var charCode = (evt.which) ? evt.which :
        event.keyCode
        if (charCode > 31 && (charCode <48 || charCode > 57))

            return false;
        return true;
    };
</script>
<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h1 class="hidden-sm hidden-xs">My Shopping  Cart</h1>
                <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">My Shopping  Cart</h1>
            </div>
            <div class="col-md-6">
                <ul class="breadcrumb">
                    <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                    </li>
                    <li>Cart</li>
                </ul>

            </div>
        </div>
    </div>
</div>

<div id="content">
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <p class="text-muted lead">You have <?= $this->cart->total_items(); ?> items in your cart.</p>
            </div>


            <div class="col-md-9 clearfix" id="basket">

                <div class="box">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th colspan="2">Product Name</th>
                                    <th>Quantity</th>
                                    <!-- <th>Weight</th> -->
                                    <th>Price</th>
                                    <th colspan="2">Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $i=0;
                                foreach ($this->cart->contents() as $items) :
                                    $i++;
                                    ?>

                                    <tr>
                                        <td>
                                            <a href="<?= site_url('produk/detail/'.$items['slug']) ?>">
                                                <img src="<?php echo base_url(); ?>assets/front_end/images/produk/<?= $items['image'] ?>">
                                            </a>
                                        </td>
                                        <td><a href="<?= site_url('produk/detail/'.$items['slug']) ?>"><?= $items['name'] ?></a></td>
                                        <td>
                                            <?= form_open('welcome/update_cart'); ?>
                                            <input type="text" name="qty" value="<?= $items['qty'] ?>" onkeypress="return isNumberKey(event)" maxlength="2" class="form-control">
                                            <input type="hidden" name="rowid" value="<?= $items['rowid'] ?>">
                                        </td>
                                        <!-- <td><?php echo ($items['weight']*$items['qty']).$items['kategory_berat']; ?>
                                        </td> -->
                                        <td>Rp <?php echo rupiah($items['price']); ?></td>
                                        <td>Rp <?php echo rupiah($items['subtotal']); ?></td>
                                        <td>
                                            <button class="btn btn-default" name="update" title="Refresh"><i class="fa fa-refresh"></i> </button>
                                            <?= form_close(); ?>
                                            <a href="<?php echo site_url('welcome/delete_cart/'.$items['rowid']); ?>" class="btn btn-warning" title="Hapus"><i class="fa fa-trash-o"></i></a>
                                        </td>
                                    </tr>

                                <?php endforeach; ?>

                            </tbody>
                            <tfoot>
                                <tr>
                                    <th colspan="5">Total</th>
                                    <th colspan="2">Rp <?php echo rupiah($this->cart->total()); ?>,00</th>
                                </tr>
                            </tfoot>
                        </table>

                    </div>
                    <!-- /.table-responsive -->

                    <div class="box-footer">
                        <div class="col-md-4">
                            <div align="center">
                                <a href="<?php echo site_url(); ?>" class="btn btn-default" style="width:100%"><i class="fa fa-chevron-left"></i> Return to Shop</a>
                            </div>
                        </div>
                        <div class="col-md-4"></div>
                        <div class="col-md-4">
                            <div align="center">
                                <a href="<?= site_url("checkout") ?>" class="btn btn-template-main cek" style="width:100%" name="pesan">Proceed Checkout <i class="fa fa-chevron-right"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col-md-9 -->

            <div class="col-md-3">
                <div class="box" id="order-summary">
                    <div class="box-header">
                        <h3>Cart Totals</h3>
                    </div>
                    <p class="text-muted" style="text-align: justify; padding: 10px;">Shipping and additional fees are calculated based on the value you have entered.</p>

                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr class="total">
                                    <td>Total</td>
                                    <th>Rp <?php echo rupiah($this->cart->total()); ?>,00</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>

                </div>


                       <!--  <div class="box">
                            <div class="box-header">
                                <h4>Coupon Code</h4>
                            </div>
                            <p class="text-muted" style="text-align: justify; padding: 10px;">If you have a shopping coupon code, please enter the code to enjoy the shopping discount</p>
                            <form>
                                <div class="input-group">

                                    <input type="text" class="form-control">

                                    <span class="input-group-btn">

                                        <button class="btn btn-template-main" type="button"><i class="fa fa-gift"></i></button> -->

                                    </span>
                                </div>
                                <!-- /input-group -->
                            </form>
                        </div>

                    </div>
                    <!-- /.col-md-3 -->

                </div>

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
        <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <script type="text/javascript">
            $(document).ready(function() {
                $('.cart').addClass('active');
            });
        </script>