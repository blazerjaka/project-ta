<script type="text/javascript">
    function PreviewImage() {
        var oFReader = new FileReader();
        oFReader.readAsDataURL(document.getElementById("uploadImage").files[0]);

        oFReader.onload = function (oFREvent) {
            document.getElementById("uploadPreview").src = oFREvent.target.result;
        };
    };
</script>

<div id="heading-breadcrumbs">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h1 class="hidden-sm hidden-xs">Invoice #<?= $this->uri->segment(3) ?></h1>
                <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Invoice #<?= $this->uri->segment(3) ?></h1>
            </div>
            <div class="col-md-5">
                <ul class="breadcrumb">

                    <li><a href="index.html">Home</a>
                    </li>
                    <li><a href="customer-orders.html">My History</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div id="content">
    <div class="container">

        <div class="row">

                    <!-- *** LEFT COLUMN ***
                       _________________________________________________________ -->

                       <div class="col-md-9 clearfix" id="customer-order">

                        <p class="lead">Invoice <b><?= $this->uri->segment(3) ?></b> booked on <strong><?= $invoice->tanggal ?></strong>
                            and now <strong><?= $invoice->status ?></strong>.</p>
                            <p><b>Hai, we confirmed that after you make payment please send your proof of payment directly by click view on your order, thank you.</b><br>Happy Shopping!</p>

                            <div class="box">
                                <div class="table-responsive">
                                    <table class="table ">
                                        <thead>
                                            <tr>
                                                <th colspan="2">Product Name</th>
                                                <th>Quatity</th>
                                                <th>Price</th>
                                                <th>Shipping</th>
                                                <th>Payment</th>
                                                <th>Discout</th>
                                                <th>Total</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $hasil = 0;
                                            foreach ($orders->result() as $key):
                                                ?>
                                                <tr>
                                                    <td>
                                                        <a href="#">
                                                            <img src="<?php echo base_url(); ?>assets/front_end/images/produk/<?= $key->gambar_produk ?>">
                                                        </a>
                                                    </td>
                                                    <td><a href="#"><?= $key->nama_produk ?></a>
                                                    </td>
                                                    <td> <?= $key->qty ?></td>
                                                    <td>Rp <?php echo rupiah($key->harga_produk); ?>,00</td>
                                                    <td><?= $invoice->delivery ?></td>
                                                    <td><?= $invoice->payment ?></td>
                                                    <td>Rp 0.00</td>
                                                    <td>Rp <?php echo rupiah($key->harga_produk*$key->qty); ?>,00</td>
                                                </tr>
                                                <?php
                                                $hasil = ($key->harga_produk*$key->qty)+$hasil;
                                                endforeach ?>
                                            </tbody>
                                            <tfoot>
                                                <tr>
                                                    <th colspan="7" class="text-right"><b>Total Order</b></th>
                                                    <th><b>Rp <?php echo rupiah($hasil); ?>,00</b></th>
                                                </tr>
                                            </tfoot>
                                        </table>

                                    </div>
                                    <!-- /.table-responsive -->
                                    <hr>
                                    <?php if ($invoice->status != 'Proceed') {
                                        }else{ ?>
                                    <div class="box clearfix" style="padding: 10px">
                                        <div class="heading">
                                            <h3>Form Confirmation of Payment</h3>
                                        </div>

                                        <p class="text-muted" align="justify">If you have any questions, please enter your message, our customer service center is always ready to serve.</p><hr>

                                        <?php $arb = array('enctype' => "multipart/form-data", );?>
                                        <?= form_open('history/addgbrinv',$arb); ?>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="firstname">Your Name</label>
                                                    <input type="text" name="nama_user" value="<?php echo $this->session->userdata('nama_user');  ?>" class="form-control" id="firstname" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">Email</label>
                                                    <input type="text" name="email" value="<?php echo $this->session->userdata('email');  ?>" class="form-control" id="email" disabled>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                    <label for="email">Your Invoice</label>
                                                    <input type="text" name="inv" value="<?= $this->uri->segment(3) ?>" class="form-control" id="email" disabled>
                                                    <input type="hidden" name="invoice" value="<?= $this->uri->segment(3) ?>">
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="form-group">
                                                <!-- <label for="firstname">Proof of Payment</label>
                                                    <input type="text" name="bukti" class="form-control" id="firstname"> -->
                                                    <div class="form-group">
                                                      <label for="exampleInputEmail1">Proof of Payment</label><br>
                                                  <!-- <div class="form-group" id="photo-preview">
                                                    <div class="col-md-9">
                                                        (No photo)
                                                        <span class="help-block"></span>
                                                    </div>
                                                </div> -->

                                                <div class="btn btn-default" style="width:100%">
                                                    <input id="uploadImage" type="file" name="gambar" class="" onchange="PreviewImage();"/><!-- <i class="fa fa-paperclip pull-right"></i> --><br>
                                                </div>
                                                <p style="font-size:10px;color:#bdbdbd">Max. 2MB</p>
                                                <div class="col-sm-6">
                                                  <img id="uploadPreview" style="width:100%; height:100%;" alt="Not a Picture" />
                                              </div>
                                          </div>
                                      </div>
                                  </div>
                              </div>
                              <br><br>
                              <div class="row">
                                <div class="col-sm-8 text-center">
                                    <button type="submit" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-location-arrow"></i> Send Data</button>
                                </div>
                                <div class="col-sm-4 text-center">
                                    <button type="submit" name="submit_cencel" value="submit_cencel" class="btn btn-lg btn-warning" style="width: 100%"><i class="fa fa-trash-o"></i> Cancle your Order</button>
                                </div>
                            </div>
                            <!-- /.row -->
                    </div>
                    <?= form_close(); } ?>
                    <div class="row addresses" style="text-align: left;padding: 10px">
                        <div class="col-sm-2"></div>
                        <div class="col-sm-8">
                            <h3 class="text-uppercase">Your Shipping Data</h3>
                            <?php foreach ($alamat->result() as $key => $row_alamat): ?>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <tbody>
                                            <tr>
                                                <td width="200px">Invoce</td><td><?= $invoice->invoice ?></td>
                                            </tr>
                                            <tr>
                                                <td>Recipient's name</td><td><?= $row_alamat->nama_penerima ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td><td><?= $row_alamat->email_users ?></td>
                                            </tr>
                                            <tr>
                                                <td>Country</td><td><?= $row_alamat->negara ?></td>
                                            </tr>
                                            <tr>
                                                <td>State</td><td><?= $row_alamat->provinsi ?></td>
                                            </tr>
                                            <tr>
                                                <td>City</td><td><?= $row_alamat->kota ?></td>
                                            </tr>
                                            <tr>
                                                <td>Postal Code</td><td><?= $row_alamat->kode_pos ?></td>
                                            </tr>
                                            <tr>
                                                <td>No HP</td><td><?= $row_alamat->no_hp ?></td>
                                            </tr>
                                            <tr>
                                                <td>Address </td><td><?php if ($row_alamat->tipe_alamat == '1'): ?>
                                                    Utama
                                                <?php endif ?>
                                                <?php if ($row_alamat->tipe_alamat == '2'): ?>
                                                    Other
                                                <?php endif ?> : <?= $row_alamat->nama_tipe ?><br><hr><?= $row_alamat->alamat ?></td>
                                            </tr>
                                            <tr>
                                                <td>Payment Method</td><td><?= $invoice->payment ?></td>
                                            </tr>
                                            <tr>
                                                <td>Shipping Method</td><td><?= $invoice->delivery ?></td>
                                            </tr>
                                            <tr>
                                                <td>Total</td><td>Rp <?php echo rupiah($hasil); ?>,00</td>
                                            </tr>
                                            <tr>
                                                <td>No Tracking</td><td><b><?= $invoice->no_tracking ?></b></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            <?php endforeach ?>
                        </div>
                    </div>
                    <!-- /.addresses -->
                    <div style="padding-left: 10px">
                        <a href="<?php echo site_url('history') ?>" class="btn btn-default" title="Kembali"><i class="glyphicon glyphicon-chevron-left"></i> Back</a>
                    </div>
                </div>
                <!-- /.box -->
            </div>
            <!-- /.col-md-9 -->

            <!-- *** LEFT COLUMN END *** -->

                    <!-- *** RIGHT COLUMN ***
                       _________________________________________________________ -->

                       <div class="col-md-3">
                        <!-- *** CUSTOMER MENU ***
                           _________________________________________________________ -->
                           <div class="panel panel-default sidebar-menu">

                            <div class="panel-heading">
                                <h3 class="panel-title">Customer section</h3>
                            </div>

                            <div class="panel-body">

                               <ul class="nav nav-pills nav-stacked">
                                <li class="account">
                                    <a href="<?= site_url('account') ?>"><i class="fa fa-user"></i> My account</a>
                                </li>
                                <li class="history">
                                    <a href="<?= site_url('history') ?>"><i class="fa fa-list"></i> My History</a>
                                </li>
                                    <!-- <li class="">
                                        <a href="customer-wishlist.html"><i class="fa fa-heart"></i> My wishlist</a>
                                    </li> -->
                                    <li class="alamat">
                                        <a href="<?= site_url('address') ?>"><i class="fa fa-map-marker"></i> My Address</a>
                                    </li>
                                    <li>
                                        <a href="<?= site_url('welcome/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <!-- /.col-md-3 -->

                        <!-- *** CUSTOMER MENU END *** -->
                    </div>

                    <!-- *** RIGHT COLUMN END *** -->

                </div>
                <!-- /.row -->


            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


        <script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
        <script>
            window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
        </script>
        <script type="text/javascript">
          $(document).ready(function() {
            $('.history').addClass('active');
        });
    </script>
