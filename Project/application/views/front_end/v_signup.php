        <div id="heading-breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h1 class="hidden-sm hidden-xs">Sign Up</h1>
                        <h1 class="hidden-md hidden-lg" style="font-size: 18pt;">Sign Up</h1>
                    </div>
                    <div class="col-md-6">
                        <ul class="breadcrumb">
                            <li><a href="<?php echo site_url('') ?>"><i class="glyphicon glyphicon-home"></i> Home</a>
                            </li>
                            <li>Sign Up</li>
                        </ul>

                    </div>
                </div>
            </div>
        </div>

        <div id="content">
            <div class="container">

                <div class="row">
                    <div class="col-md-8">
                        <div class="box" style="padding: 30px;">
                            <p class="des">Don't have account olshop?</p>
                            <p style="text-align: justify;">Let's sign up now, enjoy the various menu packages of choice.
                            <br>Happy shopping!</p>
                            <hr>

                            <form method="post" role="form" id="register-form2" action="<?php echo site_url('signup/proses'); ?>">
                                <div class="form-group">
                                    <label for="name-login">Your Name</label>
                                    <input type="text" class="form-control" placeholder="" name="name"  required=" ">
                                    <span class="help-block" id="error"></span>
                                </div>
                                <div class="form-group">
                                    <label for="email-login">Email</label>
                                    <input type="email" class="form-control" name="email" id="email2" placeholder="" required=" "><span class="help-block" id="error"></span>
                                </div>
                                <div class="form-group">
                                    <label for="password-login">Password</label>
                                    <input type="password" class="form-control" id="password2" placeholder="" name="password" required=" "><span class="help-block" id="error"></span>
                                </div>
                                <div class="form-group">
                                    <label for="password-login">Confirmation Password</label>
                                    <input type="password" class="form-control"  placeholder="" required=" " name="cpassword"><span class="help-block" id="error"></span>
                                </div>
                                <hr>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-lg btn-template-primary" style="width: 100%"><i class="fa fa-user-md"></i> Register Now!</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="box" style="padding: 30px;">
                        Iklan
                        </div>
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->
