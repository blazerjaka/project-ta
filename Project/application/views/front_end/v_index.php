<link href="<?php echo base_url() ?>assets/front_end/css/light-carousel1.css" rel="stylesheet" type="text/css">

<div class="sample1">
    <div class="carousel">
       
        <div class="controls">
            <div class="prev"></div>
            <div class="next"></div>
        </div>
    </div>
</div>
<script src="<?php echo base_url() ?>assets/front_end/js/jquery.light-carousel1.js"></script>
<script>
    $('.sample1').lightCarousel();
</script>

<section class="">
    <div class="container" style="background: #fff;border-radius: 0 0 0 10px;border: 1px solid #ddd">
        <div class="col-md-12">
            <hr>
            <div class="heading text-center">
                <h2>Category Product</h2>
            </div>

            <div class="row products">
                <?php foreach ($kategory_produk->result() as $key): ?>
                    <div class="col-md-4">
                        <div class="team-leader">
                            <div class="team-leader-shadow"><a href="<?php echo site_url('produk/index/'.$key->slug) ?>"></a></div>
                            <img src="<?php echo base_url() ?>assets/front_end/img/<?= $key->gambar?>" alt="<?= $key->nama ?>">
                            <div class="team-leader-span"><?= $key->nama ?> Product</div>
                            <ul>
                                <li><a href="<?php echo site_url('produk/index/'.$key->slug) ?>">View Product</a></li>
                            </ul>
                        </div>
                    </div>
                <?php endforeach ?>
            </div>
            <hr>
            <!-- /.products -->
            <div class="row products">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <a href="#"><p style="font-size: 20pt;" align="center">View All Category</p></a>
                    <div class="team-leader">
                        <div class="team-leader-shadow"><a href="<?php echo site_url('produk/viewall') ?>"></a></div>
                        <img src="<?php echo base_url() ?>assets/front_end/images/produk/all.jpg" alt="white"">
                        <div class="team-leader-span hidden-xs hidden-sm">View All Category on <b>Label A<b></div>
                        <div class="team-leader-span visible-xs visible-sm">All Category Product</div>
                        <ul>
                            <li><a href="<?php echo site_url('produk/viewall') ?>">View All Category</a></li>
                        </ul>
                    </div>
                    <br><br>
                </div>
            </div>
        </div>
    </div>
    <!-- /.container -->
</section>
<!-- /#content -->

<section class="bar background-pentagon no-mb">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="heading text-center">
                    <h2>What's our customers say ?</h2>
                </div>

                <p class="des text-center">We already have many customers.<br>See what our customers say about us.</p>


                <!-- *** TESTIMONIALS CAROUSEL ***_________________________________________________________ -->
                <ul class="owl-carousel testimonials same-height-row">
                    <li class="item bordir">
                        <div class="testimonial same-height-always">
                            <div class="text">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua.</p>
                            </div>
                            <div class="bottom">
                                <div class="icon"><i class="fa fa-quote-left"></i>
                                </div>
                                <div class="name-picture">
                                    <h5>Ilham Hermawan</h5>
                                    <p><a href="mailto:ihamherwawantkj@gmail.com" title="email">ihamherwawantkj@gmail.com</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="item">
                        <div class="testimonial same-height-always">
                            <div class="text">
                                <p>Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat.</p>
                            </div>
                            <div class="bottom">
                                <div class="icon"><i class="fa fa-quote-left"></i>
                                </div>
                                <div class="name-picture">
                                    <h5>Yuliandri</h5>
                                    <p><a href="mailto:yuliandri@gmail.com" title="email">yuliandri@gmail.com</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="item">
                        <div class="testimonial same-height-always">
                            <div class="text">
                                <p> Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur.</p>
                            </div>

                            <div class="bottom">
                                <div class="icon"><i class="fa fa-quote-left"></i>
                                </div>
                                <div class="name-picture">
                                    <h5>Leni Ardianti</h5>
                                    <p><a href="mailto:leni12@gmail.com" title="email">leni12@gmail.com</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="item">
                        <div class="testimonial same-height-always">
                            <div class="text">
                                <p>Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>

                            <div class="bottom">
                                <div class="icon"><i class="fa fa-quote-left"></i>
                                </div>
                                <div class="name-picture">
                                    <h5>Meri Dianti</h5>
                                    <p><a href="mailto:merimeri12@gmail.com" title="email">merimeri12@gmail.com</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class="item">
                        <div class="testimonial same-height-always">
                            <div class="text">
                                <p> Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>

                            <div class="bottom">
                                <div class="icon"><i class="fa fa-quote-left"></i>
                                </div>
                                <div class="name-picture">
                                    <h5>Fitrianty</h5>
                                    <p><a href="mailto:fitri25@gmail.com" title="email">fitri25@gmail.com</a></p>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
                <!-- /.owl-carousel -->
                <!-- *** TESTIMONIALS CAROUSEL END *** -->
            </div>

        </div>
    </div>
</section>
<!-- /.bar -->
<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.1.min.js"></script>
<script>
    window.jQuery || document.write('<script src="<?php echo base_url() ?>assets/front_end/js/jquery-1.11.0.min.js"><\/script>')
</script>
<script type="text/javascript">
    $(document).ready(function(){
        $(".form-item").submit(function(e){
            var form_data = $(this).serialize();
            var button_content = $(this).find('button[type=submit]');
            button_content.html('Adding...'); //Loading button text

            $.ajax({ //make ajax request to cart_process.php
                url: "<?php echo base_url().'welcome/add_cart'; ?>",
                type: "POST",
                dataType:"json", //expect json value from server
                data: form_data
            }).done(function(data){ //on Ajax success
                $("#cart-info").html(data.items); //total items in cart-info element
                button_content.html('<i class="glyphicon glyphicon-shopping-cart"></i> Proceed'); //reset button text to original text
                alert("Your order has entered the shopping cart"); //alert user
                location.reload();
            })
            e.preventDefault();
        });
    });
    $(document).ready(function() {
        $('.home').addClass('active');
    });
</script>